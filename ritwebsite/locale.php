<?php
  /**
   * Utility storing all static language values
   *
   * PHP version 5.4.3
   *
   * @category Utility
   * @package  Locale
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?>

<?php
    $languages = [
        "_en"=>[
            // Home Page
            "News" => "News",
            "Events"=> "Events",
            "Study@RIT" => "Study @ RIT",
            "Notices" => "Notice Board",
            "NoticeStaff" => "Notices For Staffs",
            "NoticeStud" => "Notices For Students",
            "ViewAll" => "View All",
            "UnderGraduateCourses"=>"Undergraduate Courses",
            "PostGraduateCourses"=>"Postgraduate Courses",
            "ResearchCourses"=>"Research Courses",
            "KnowMore" => "Know More",
            "Announcements" => "Announcements",
            "PrincipalMessage" => "Message from the Principal",
            "RITHighlights" => "RIT Highlights",
            "FooterText"=>"Rajiv Gandhi Institute Of Technology, named after the late Prime Minister Sri Rajiv Gandhi, run by the Government of Kerala, started functioning in 1991.",
            "Copyright"=>"Copyright @2021 | Designed With ♥ by RIT",

            // View All
            "AllAnnouncements" => "All Announcements" ,
            "AllNews" => "All News" ,
            "AllEvents" => "All Events" ,
            "AllNotices" => "All Notices",
            "PostedOn" => "Posted On:",
            "ExpiredOn" => "Expired On:",
            "ExpiresOn" => "Expires On:",
            "EventStartOn" => "Event Starts On:",
            "EventEndOn" => "Event Ends On:",
            "Staffs" => "For Staffs",
            "Students" => "For Students",

            // Departments
            "Administration"=>"Department of Administration",

            "AboutUs"=>"About Us",
            "Vision"=>"Vision",
            "Mission"=>"Mission",
            "HOD"=>"HOD's Desk",
            "Gallery"=>"Gallery",
            "Teachers"=>"Teachers",
            "TechnicalStaff" => "Technical Staff",
            "AdminStaff" => "Administrative Staff",

            // Academics
            "CoursesOffered" => "Courses Offered..",

            // Login
            "Login"=>"Login",
            "Username"=>"User name",
            "Password"=>"Password",

            // Error code
            "e1" => "Please enter username and password",
            "e2" => "No such user exist",
            "e3" => "No such username password combination exist",

            //contact us
            "ContactUs"=>"Contact Us",
            "welcome"=>"Welcome to Rajiv Gandhi Institute of Technology",
            "Address"=>"Address",
            "Velloor"=>"Velloor P.O",
            "pampady"=>"Pampady" ,
            "kottayam"=>"Kottayam-686501, Kerala",
            "collegeemail"=>"College Email Id",
            "phone"=>"Phone No",
            "department"=>"Department E-Mail Id",
            "PTA"=>"P.T.A",
            "science"=>"Science Wing",
            "others"=>"Others",
            "consultancy"=>"Consultancy Services",
            "send"=>"Send Your Messages",
            "name"=>"Name",
            "email"=>"E-Mail",
            "message"=>"Message",

            // Facility
            "Auditorium"=>"Auditorium",
            "AuditoriumText"=>"A mini-auditorium is available near the workshop building for academic or nonacademic activities of the college.",

            //footer
            "QuickLink"=>"Quick Links",
            "TotalVisits"=>"Total Visits",

            //quotations
            "Sl.No"=>"Sl.No",
            "Quotations"=>"Quotations",
            "Tenders"=>"Tenders",
            "Committees"=>"College Commitees",
            "Action"=>"Action",
            "show"=>"Show",

            //pensioners
            "Department"=>"Department",
            "Designation"=>"Designation",
            "retiredate"=>"Retire Date",
            "Image"=>"Image",
            "contactdetails"=>"Contact Details",
            "citation"=>"Citation",

            
        ],
        "_mal"=>[
            // Home Page
            "News" => "വാർത്തകൾ",
            "Events"=> "ഇവന്റുകൾ",
            "Study@RIT" => "പഠനം @ RIT",
            "Notices"=> "അറിയിപ്പ് ബോർഡ്",
            "NoticeStaff" => "സ്റ്റാഫുകൾക്കുള്ള അറിയിപ്പുകൾ",
            "NoticeStud" => "വിദ്യാർത്ഥികൾക്കുള്ള അറിയിപ്പുകൾ",
            "ViewAll" => "എല്ലാം കാണുക",
            "UnderGraduateCourses"=>"ബിരുദ കോഴ്സുകൾ",
            "PostGraduateCourses"=>"ബിരുദാനന്തര കോഴ്സുകൾ",
            "ResearchCourses"=>"ഗവേഷണ കോഴ്സുകൾ",
            "KnowMore" => "കൂടുതലറിയുക",
            "PrincipalMessage"=>"പ്രിൻസിപ്പലിൽ നിന്നുള്ള സന്ദേശം",
            "RITHighlights"=>"RIT ഹൈലൈറ്റുകൾ",
            "Announcements" => "പ്രഖ്യാപനങ്ങൾ",
            "FooterText"=>"അന്തരിച്ച പ്രധാനമന്ത്രി ശ്രീ രാജീവ് ഗാന്ധിയുടെ പേരിലുള്ള രാജീവ് ഗാന്ധി ഇൻസ്റ്റിറ്റ്യൂട്ട് ഓഫ് ടെക്നോളജി 1991 ൽ പ്രവർത്തനം ആരംഭിച്ചു.",
            "Copyright"=>"പകർപ്പവകാശം @ 2021 സ്നേഹത്തോടെ RIT രൂപകൽപ്പന ചെയ്തത്",

            // View All
            "AllAnnouncements" => "എല്ലാ പ്രഖ്യാപനങ്ങളും" ,
            "AllNews" => "എല്ലാ വാർത്തകളും" ,
            "AllEvents" => "എല്ലാ ഇവന്റുകളും" ,
            "AllNotices" => "എല്ലാ അറിയിപ്പുകളും",
            "PostedOn" => "പോസ്റ്റ് ചെയ്തത്:",
            "ExpiredOn" => "കാലഹരണപ്പെടുന്ന തീയതി:",
            "EventStartOn" => "ഇവന്റ് ആരംഭ തീയതി:",
            "EventEndOn" => "ഇവന്റ് അവസാന തീയതി തീയതി:",
            "Staffs" => "സ്റ്റാഫുകൾക്കായി",
            "Students" => "വിദ്യാർത്ഥികൾക്കായി",

            // Departments
            "Administration"=>"അഡ്മിനിസ്ട്രേഷൻ വകുപ്പ്",

            "AboutUs"=>"ഞങ്ങളേക്കുറിച്ച്",
            "Vision"=>"ദർശനം",
            "Mission"=>"ദൗത്യം",
            "HOD"=>"HOD ഡെസ്ക്",
            "Gallery"=>"ഗാലറി",
            "Teachers"=>"അധ്യാപകർ",
            "TechnicalStaff" => "സാങ്കേതിക ഉദ്യോഗസ്ഥർ",
            "AdminStaff" => "അഡ്മിനിസ്ട്രേറ്റീവ് സ്റ്റാഫ്",

            // Academics
            "CoursesOffered" => "ലഭ്യമായ കോഴ്സുകൾ..",

            //Login
            "Login"=>"ലോഗിൻ",
            "Username"=>"ഉപയോക്തൃ നാമം",
            "Password"=>"പാസ്‌വേഡ്",

            // Error code
            "e1" => "ദയവായി ഉപയോക്തൃനാമവും പാസ്‌വേഡും നൽകുക",
            "e2" => "അത്തരമൊരു ഉപയോക്താവ് നിലവിലില്ല",
            "e3" => "അത്തരം ഉപയോക്തൃനാമ പാസ്‌വേഡ് സംയോജനമൊന്നും നിലവിലില്ല",

            //contact us
            "ContactUs"=>"ഞങ്ങളെ സമീപിക്കുക",
            "welcome"=>"രാജീവ് ഗാന്ധി ഇൻസ്റ്റിറ്റ്യൂട്ട് ഓഫ് ടെക്നോളജിയിലേക്ക് സ്വാഗതം",
            "Address"=>"വിലാസം",
            "Velloor"=>"വെല്ലൂർ പി.ഒ.",
            "pampady"=>"പാമ്പാടി",
            "kottayam"=>"കോട്ടയം - 686501, കേരളം",
            "collegeemail"=>"കോളേജ് ഇമെയിൽ ഐഡി",
            "phone"=>"ഫോൺ നമ്പർ",
            "department"=>"വകുപ്പ് ഇമെയിൽ ഐഡി",
            "PTA"=>"പി.ടി.എ.",
            "science"=>"സയൻസ് വിഭാഗം",
            "consultancy"=>"കൺസൾട്ടൻസി സേവനങ്ങൾ",
            "others"=>"മറ്റുള്ളവ",
            "send"=>"നിങ്ങളുടെ സന്ദേശം അയയ്‌ക്കുക",
            "name"=>"പേര്",
            "email"=>"ഇ-മെയിൽ",
            "message"=>"സന്ദേശം",

            // Facility
            "Auditorium"=>"ഓഡിറ്റോറിയം",
            "AuditoriumText"=>"കോളേജിന്റെ അക്കാദമിക് അല്ലെങ്കിൽ അക്കാദമിക് പ്രവർത്തനങ്ങൾക്കായി വർക്ക് ഷോപ്പ് കെട്ടിടത്തിന് സമീപം ഒരു മിനി ഓഡിറ്റോറിയം ലഭ്യമാണ്.",

            //footer
            "QuickLink"=>"ദ്രുത ലിങ്കുകൾ",
            "TotalVisits"=>"മൊത്തം സന്ദർശനങ്ങൾ",

            //quotations
            "Sl.No"=>"ക്രെമ.നമ്പർ",
            "Quotations"=>"ഉദ്ധരണികൾ",
            "Tenders"=>"ടെൻഡറുകൾ",
            "Committees"=>"കമ്മിറ്റികൾ",
            "Action"=>"പ്രവർത്തനം",
            "show"=>"കാണിക്കുക",

            //pensioners
            "Department"=>"വകുപ്പ്",
            "Designation"=>"പദവി",
            "retiredate"=>"വിരമിക്കുന്ന തീയതി",
            "Image"=>"ചിത്രം",
            "contactdetails"=>"ബന്ധപ്പെടാനുള്ള വിശദാംശങ്ങൾ",
            "citation"=>"അവലംബം",

        ]
    ];
    
    ?>