<?php
  /**
   * Webpage showing all announcements or news
   *
   * This webpage uses the POST value of table
   * to find the table to retrieve data from
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  ViewAll
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Activites
   */
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
    require '../findUrl.php';
    require '../header.php';
    require '../connection.php';
    require '../retrieveData.php';
    require '../locale.php';
    require '../header.php';


    $sql = '';
    $subHeading = '';
    if (isset($_POST['table'])) {
        $table = $_POST['table'];
        $type = $_POST['dept'];
        $heading = $_POST['vName'];
        $noticeType = $_POST["noticeType"];
        $_SESSION["viewAllTableName"] = $table;
        $_SESSION["viewAllTableType"] = $type;
        $_SESSION["viewAllTableHeading"] = $heading;
        $_SESSION["viewAllNoticeType"] = $noticeType;
    } else {
        if (isset($_SESSION["viewAllTableName"])) {
            $table = $_SESSION["viewAllTableName"];
            $type = $_SESSION["viewAllTableType"];
            $heading = $_SESSION["viewAllTableHeading"];
            $noticeType = $_SESSION["viewAllNoticeType"];
        } else {
            header("Location: ".$_SERVER['HTTP_REFERER']);
        }
    }

    if ($table == 'ritwebsite_department_news') {
        
        $sql = "SELECT * FROM ritwebsite_department_news WHERE department='".$type."'";
        $departmentName = retrieveData('SELECT name_en,name_mal FROM ritwebsite_sub_category WHERE name_en="'.$type.'"', $con);
        if ($lang == "_en") {
            $subHeading = "Department of ".$departmentName[0]["name_en"];
        } else {
            $subHeading = $departmentName[0]["name_mal"]." വകുപ്പ്";
        }
    } elseif ($table === "ritwebsite_notices") {
          
          $sql = "SELECT * FROM ritwebsite_notices WHERE type='".$noticeType."'";
          $subHeading = $languages[$lang][$noticeType];
    } else {
        $sql = "SELECT * FROM ".$table." ORDER BY id DESC";
    }

    // Retrieveing data
    $tableRecords = retrieveData($sql, $con);
    

    // Setting View
    if ($heading == 'AllNews') {
        $content1Class = "col-sm-12 col-md-8";
        $content2Class = "col-sm-12 col-md-4";
    } else {
        $content1Class = "col-12";
        $content2Class = "col-0";
    }
    ?>
  <title>Rajiv Gandhi Institute of Technology | View All</title>

  <link href="style.css" rel="stylesheet">

</head>

<body onload="setTheme()">

  <?php
    $activePageName = "";
    require '../nav.php';
    ?>

  <!-- Heading -->
  <div class="container">
    <div class="row">
      <div>
        <h1>
          <a href="javascript:history.back()" class="back-btn">
            <i class="fas fa-arrow-circle-left"></i>
          </a>
          <?php echo $languages[$lang][$heading]; ?>
        </h1>
        <h3> <?php echo $subHeading; ?> </h3>
      </div>
    </div>
  </div>
  <hr>

  <!-- All Records -->
  <div class="container">
    <?php
    for ($i=0; $i < count($tableRecords); $i++) {
        $firstContent = 0;
        $content = '';
        $eventDates = '';
        foreach ($tableRecords[$i] as $key => $value) {
            switch ($key) {
            case 'id':
                break;

            case 'image':
                $image = $value;
                break;

            case 'type':
                $type = $value;
                break;

            case 'createdAt':
                $content .= '<p class="view-date"> 
                  <strong>'.$languages[$lang]["PostedOn"].'</strong>
                  <time datetime="'.$value.'">'.$value.'</time>
                </p>';
                break;
            case 'expireAt':
            case 'expiry':
                $expiryTitle = $languages[$lang]["ExpiredOn"];
                if (strtotime($value) > time() ) {
                    $expiryTitle = $languages[$lang]["ExpiresOn"];
                }
                $content .= '<p class="view-date">
                  <strong>'.$expiryTitle.'</strong> 
                  <time datetime="'.$value.'">'.$value.'</time>
                </p>';
                break;
            
            case 'event_start':
                $eventDates .= '<p class="view-date">
                    <strong>'.$languages[$lang]["EventStartOn"].'</strong> 
                    <time datetime="'.$value.'">'.$value.'</time>
                  </p>';
                break;
            case 'event_end':
                $eventDates .= '<p class="view-date">
                  <strong>'.$languages[$lang]["EventEndOn"].'</strong> 
                  <time datetime="'.$value.'">'.$value.'</time>
                 </p>';
                break;
            
            default:
                if (strpos($key, $lang) !== false) {
                    if ($firstContent === 0) {
                        $content .= '<h4 class="view-heading"> '.$value.' </h4>';
                        $firstContent++;
                    } else {
                        $content .= '<p class="view-content"> '.$value.' </p>';
                    }
                } else {
                    if (strpos($value, "http") !== false) {
                        $allLink = $value;
                        $allLinkPos = strpos($value, $pathToUploads);
                        if ($allLinkPos !== false) {
                            $allLinkPart = substr($value, $allLinkPos);
                            $allLink = findHost().$allLinkPart;
                        }
                    }
                }
                break;
            }
        }
        
        if ($image === '') {
            $contentImage = "../images/news-holder.png";
        } else {
            $contentImage =  'data:' .$type.' ;base64,'.$image;
        }
        ?>

    <div class="row justify-content-center m-4 view-card shadow rounded ">
      <a href="<?php echo $allLink; ?>" target="_blank" rel="noopener noreferrer">
      <!-- Image -->
      <div class="<?php echo $content2Class; ?> img content-img order-sm-1 order-md-2"
        style="background-image: url('<?php echo $contentImage; ?>');">
      </div>

      <!-- Content -->
      <div class="<?php echo $content1Class; ?> p-4 content-card order-sm-2 order-md-1">
        <?php
          echo $content;
        echo $eventDates; ?>
      </div>
      </a>
    </div>

        <?php
        $allLink = "";
    }
    ?>
  </div>


  <?php require '../footer.php'; ?>

</body>

</html>