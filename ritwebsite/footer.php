<?php
  /**
   * Footer used in all webpages
   *
   * PHP version 5.4.3
   *
   * @category Footer
   * @package  RitWebsite
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?>
<footer class="footer">
    <?php 
    $quick = retrieveData('SELECT * FROM ritwebsite_quick_link ORDER BY priority', $con); 
    $views=retrieveData('SELECT total_views FROM ritwebsite_viewer_count WHERE id=1', $con);
    ?>
    <div class="container">
        <!-- Heading -->
        <div class="row" id="linksButton">
            <h3 class="ps-0" style="text-align:left;color:#fff">
            <?php echo $languages[$lang]["QuickLink"] ?>
                <i id="linkIcon" class="fas fa-angle-up"></i>
            </h3>
        </div>
        <hr id="firstHR">

        <!-- links -->
        <div class="row" id="links">
            <?php 
            for ($i=0;$i<count($quick);$i++) {
                $qname = $quick[$i]["name".$lang];
                $qlink = $quick[$i]["main_link"];
                
                // Adding root if file is uploaded
                $qlinkPos = strpos($qlink, "/Admin");
                if ($qlinkPos !== false) {
                    $linkPart = substr($qlink, $qlinkPos);
                    $qlink = findHost().$linkPart;
                }

                // Adding root if link is partial link
                if (strpos($qlink, "http") === false) {
                    $qlink = findHost().$qlink;
                }
                    
                ?>

            <div class="col-sm-12 col-md-3 p-2 linkContainer" 
             style="text-align:left">
                <a class="qlinks" href="<?php echo $qlink; ?>" target="_blank">
                  <?php echo $qname;?></a>
            </div>

                <?php 
            } ?>
        </div>
        <hr>

        <!-- Logo -->
        <div class="row">
            <div class="col-6 footLogo">
                <img src="<?php echo findHost()."/images/footerLogo.png" ?>" 
                 alt="Logo" class="img-fluid">
            </div>
            <div class="col-6 footView">
                <h5 style="color:#fff"> <?php echo $languages[$lang]["TotalVisits"] ?></h5>
                <?php   
                echo $views[0]["total_views"]; ?>
            </div>
        </div>
    </div>

    <hr>

    <div class="container">
        <div class="row justify-content-between">
            <div class="col-9">
            <p style="text-align:left"><?php echo $languages[$lang]["Copyright"] ?>
            </p>
            </div>
            <div class="col-3" style="text-align:right">
                <a href="javascript:window.scrollTo(0,0);" class="btn btn-primary">
                  <i id="linkIcon" class="fas fa-angle-up"></i>
                </a>
            </div>
        </div>
    </div>

    <script>
        document.getElementById('linksButton').onclick = showHideLinks;

        function showHideLinks() {
            var icon = document.getElementById('linkIcon');
            var links = document.getElementById('links');
            var firstHR = document.getElementById('firstHR');
            let closed = localStorage.getItem("closed") == "true";
            if (closed) {
                firstHR.style.display = "block";
                icon.style.transform = "rotate(360deg)"
                links.style.transform = "scale(1,1)";
                links.style.maxHeight = "500vh";
                localStorage.setItem("closed", "false");
            } else {
                firstHR.style.display = "none";
                icon.style.transform = "rotate(180deg)"
                links.style.transform = "scale(1,0)";
                links.style.maxHeight = "0";
                localStorage.setItem("closed", "true");
            }
        }
    </script>
</footer>