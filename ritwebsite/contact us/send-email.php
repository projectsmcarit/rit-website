<?php
  /**
   * Logic to send mail
   *
   * This feature needs setup of email system
   * All other aspects of code is done.
   *
   * PHP version 5.4.3
   *
   * @category Logic
   * @package  ContactUs
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/contact%20us
   */


// Replace this with correct  email address
$to = 'hoseakalayil@gmail.com';

if ($_POST) {
    $name = trim(stripslashes($_POST['name']));
    $email = trim(stripslashes($_POST['email']));
    $subject = trim(stripslashes(''));
    $contact_message = trim(stripslashes($_POST['message']));
   
    if ($subject == '') {
        $subject = "Contact Form Submission";
    }

    // Set Message
    $message = "Email from: " . $name . "<br />";
    $message .= "Email address: " . $email . "<br />";
    $message .= "Message: <br />";
    $message .= nl2br($contact_message);
    $message .= "<br /> ----- <br /> This email was sent from the contact form @ RIT website";

    // Set From: header
    $from =  $name . " <" . $email . ">";

    // Email Headers
    $headers = "From: " . $from . "\r\n";
    $headers .= "Reply-To: ". $email . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    ini_set("sendmail_from", $to); // for windows server
    $mail = mail($to, $subject, $message, $headers);

    if ($mail) {
        echo "OK";
    } else {
        print_r(error_get_last());
        echo "Something went wrong. Please try again.";
    }
}
