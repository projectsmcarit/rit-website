<?php
  /**
   * Webpage to show contact info
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  ContactUs
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/contact%20us
   */
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
    require '../findUrl.php';
    require '../connection.php';
    require '../retrieveData.php';
    require '../locale.php';
    require '../header.php'; 
    ?>

  <!-- Style -->
  <link rel="stylesheet" href="style.css">

  <title>Rajiv Gandhi Institite Of Techonology | Contact Us</title>
</head>

<body body onload="setTheme()">
  <?php
    $activePageName = "Contact";
    require '../nav.php';
    ?>


  <div class="content" style="background-color:var(--newsevent)">

    <div class="container">

      <div class="row align-items-center">

        <div class="col me-auto bgContainer p-4">
          <div class="mb-5">
            <h3 class="text-white mb-4">
              <?php echo $languages[$lang]["ContactUs"] ?>
            </h3>
            <p class="text-white"><?php echo $languages[$lang]["welcome"] ?></p>
            <hr class="hrStyle">
          </div>
          <div class="row">
            <div class="col-md-6">
              <h3 class="text-white h5 mb-3">
                <?php echo $languages[$lang]["Address"] ?>
              </h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white mb-2">
                  <span class="me-3">
                    <i class="fas fa-map"></i>
                  </span>
                  <?php 
                    echo $languages[$lang]["Velloor"]."<br>"
                    .$languages[$lang]["pampady"]."<br>"
                    .$languages[$lang]["kottayam"]
                    ?>
                </li>
              </ul>
            </div>

            <div class="col-md-6">
              <h3 class="text-white h5 mb-3">
                <?php echo $languages[$lang]["collegeemail"] ?>
              </h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white">
                  <span class="me-3">
                    <i class="far fa-envelope"></i>
                  </span>
                  College: info@rit.ac.in
                </li>
                <li class="d-flex text-white">
                  <span class="me-3">
                    <i class="far fa-envelope"></i>
                  </span>
                  Principal : principal@rit.ac.in
                </li>
              </ul>
            </div>

            <div class="col-md-6">
              <h3 class="text-white h5 mb-3">
                <?php echo $languages[$lang]["phone"] ?>
              </h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white mb-2">
                  <span class="me-3">
                    <i class="fas fa-phone-alt"></i>
                  </span>
                  Principal : 0481 2506153
                </li>
                <li class="d-flex text-white mb-2">
                  <span class="me-3">
                    <i class="fas fa-fax"></i>
                  </span>
                  Fax: 0481 2506153
                </li>
                <li class="d-flex text-white mb-2">
                  <span class="me-3">
                    <i class="fas fa-phone-alt"></i>
                  </span>
                  Office: 0481 2507763
                </li>
                <li class="d-flex text-white mb-2">
                  <span class="me-3">
                    <i class="fas fa-phone-alt"></i>
                  </span>
                  CCE: 0481 2500453
                </li>
              </ul>
            </div>


            <div class="col-md-6">
              <h3 class="text-white h5 mb-3">
                <?php echo $languages[$lang]["department"] ?>
              </h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white">
                  <span class="me-3">
                    <i class="far fa-envelope"></i>
                  </span>
                  Civil : hod.ce@rit.ac.in
                </li>
                <li class="d-flex text-white">
                  <span class="me-3">
                    <i class="far fa-envelope"></i>
                  </span>
                  Computer Science: hod.cse@rit.ac.in
                </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Electrical
                  : hod.eee@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span>
                  Electronincs : hod.ece@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Mechanical
                  : hod.me@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> MCA 
                 : hod.mca@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Physical
                  Education : hod.physicaleducation@rit.ac.in </li>
              </ul>
            </div>

            <div class="col-md-6">
              <h3 class="text-white h5 mb-3"><?php echo $languages[$lang]["science"] ?></h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Physics 
                  : hod.physics@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Chemistry
                  : hod.chemistry@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Maths 
                  : hod.maths@rit.ac.in </li>
              </ul>
            </div>

            <div class="col-md-6">
              <h3 class="text-white h5 mb-3"><?php echo $languages[$lang]["others"] ?></h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Hostel 
                  : warden@rit.ac.in </li>
                <li class="d-flex text-white"><span class="me-3"><i class="far fa-envelope"></i></span> Library 
                  : librarian@rit.ac.in </li>
              </ul>
            </div>


            <div class="col-md-6">
              <h3 class="text-white h5 mb-3"><?php echo $languages[$lang]["consultancy"] ?></h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white mb-2"><span class="me-3"><i class="fas fa-phone-alt"></i></span> 0481
                  2500453</li>
              </ul>
            </div>

            <div class="col-md-6">
              <h3 class="text-white h5 mb-3"><?php echo $languages[$lang]["PTA"] ?></h3>
              <ul class="list-unstyled mb-5">
                <li class="d-flex text-white mb-2"><span class="me-3"><i class="fas fa-phone-alt"></i></span>
                  0481-2508453</li>
              </ul>
            </div>
            <hr class="hrStyle">
          </div>
        </div>

        <!-- <div class="col">
          <div class="box container">
            <h3 class="heading"><?php echo $languages[$lang]["send"] ?></h3>
            <form class="mb-5" method="post" id="contactForm" name="contactForm"
              action="<?php echo $serverHome ?>/contact%20us/send-email.php">
              <div class="row">

                <div class="col-md-12 form-group">
                  <label for="name" class="col-form-label"><?php echo $languages[$lang]["name"] ?></label>
                  <input type="text" class="form-control" name="name" id="name" required>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="email" class="col-form-label"><?php echo $languages[$lang]["email"] ?></label>
                  <input type="text" class="form-control" name="email" id="email" required>
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-md-12 form-group">
                  <label for="message" class="col-form-label"><?php echo $languages[$lang]["message"] ?></label>
                  <textarea class="form-control" name="message" id="message" cols="30" rows="7" required></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-block btn-primary rounded-0 py-2 px-4">
                  <span class="submitting"></span>
                </div>
              </div>
            </form>

            <div id="form-message-warning mt-4"></div>
            <div id="form-message-success">
              Your message was sent, thank you!
            </div>
          </div>
        </div> -->

      </div>

    </div>
  </div>
  <?php require "../footer.php" ?>
</body>

</html>