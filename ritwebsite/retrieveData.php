<?php
     /**
      * Utility used to retrieve data from table
      *
      * PHP version 5.4.3
      *
      * This file dependes on connection.php
      * Make sure to import that file first
      *
      * @category Utility
      * @package  RetrieveData
      * @author   Original Author <hoseakalayil@gmail.com>
      * @license  https://opensource.org/licenses No License
      * @version  SVN: $1.0$
      * @link     http://rit.ac.in/
      */

/**
 * Function used retrieve data from tables
 *
 * @param String $query The SQL query
 * @param mysqli $con   The SQL connection
 * 
 * @return Array
 */
function retrieveData($query,$con) 
{
    $queryResult = mysqli_query($con, $query);
    $result = [];
    if ($queryResult) {
        while ($row = mysqli_fetch_array($queryResult, MYSQLI_ASSOC)) {
            array_push($result, $row);
        }
    }
    
    if (gettype($queryResult) === "mysqli_result") {
        mysqli_free_result($queryResult);
    }
    return $result;
}
?>