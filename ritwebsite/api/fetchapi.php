<?php
    /**
     * Utility to retrieve teacher data from RITSOFT
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables
     */

/**
 * Fetch Teacher details from RITSOFT
 * 
 * The department and its corresponding dept parameter is given below
 * The correct dept parameter is required while calling the function
 * 
 * Applied Science               -> APPLIED SCIENCE
 * Architecture                  -> ARCHITECTURE
 * Civil                         -> CIVIL
 * Computer Science              -> COMPUTER SCIENCE
 * Electrical And Electronics    -> ELECTRICAL AND ELECTRONICS
 * Electronics And Communication -> ELECTRONICS AND COMMUNICATION
 * MCA                           -> COMPUTER APPLICATIONS
 * Mechanical                    -> MECHANICAL
 *
 * @param string $dept    To determine department to fetch from
 * @param string $apiKey  APIKEY set in api_auth table
 * @param string $apiName username set in api_auth table
 * @param string $apiPass password set in api_auth table
 * 
 * @return array
 */
function fetchDepartmentDetails($dept, $apiKey, $apiName, $apiPass)
{
    $finalResult = array();
    try {
        $ch = curl_init();
        $url = "http://rit.ac.in/ritsoft/ritsoftv2/api/faculty_detailsapi.php";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "dept=${dept}&key=${apiKey}&username=${apiName}&password=${apiPass}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($server_output, true);
        if ($result['result'] == 'Found') {
            $finalResult = array($result['image'],$result['data']);
        } else {
            $finalResult = [['ERROR']];
        }
        return $finalResult;
    } catch (Exception $e) {
        return [['ERROR']];
    }
}