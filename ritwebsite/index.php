<?php
  /**
   * Webpage showing Home page
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Home
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
     require 'findUrl.php';
     require 'header.php';
     require 'connection.php';
     require 'retrieveData.php';
     require 'locale.php';
     $cachefile = "cache/cached_home".$lang.".html"; // cached_home_en.html
     require 'cache/retrieveCache.php';
     
     $quotes = retrieveData('SELECT * FROM ritwebsite_home_page_quotes ORDER BY RAND() LIMIT 1', $con);
     $imageSlider = retrieveData('SELECT * FROM ritwebsite_home_page_image_slider ORDER BY priority', $con);
     $newsEvents = retrieveData('SELECT * FROM ritwebsite_home_page_news WHERE expiry > NOW() ORDER BY priority', $con);
     $events = retrieveData('SELECT * FROM ritwebsite_home_page_events WHERE event_end > NOW() ORDER BY priority', $con);
     $studyTypes = retrieveData('SELECT * FROM ritwebsite_academic_detail ORDER BY priority', $con);
     $principalContent = retrieveData('SELECT * FROM ritwebsite_home_page_principal_message', $con);
     $highlights = retrieveData('SELECT * FROM ritwebsite_home_page_highlights', $con);
     $notice = retrieveData('SELECT * FROM ritwebsite_notices WHERE expireAt > NOW() ORDER BY priority LIMIT 4', $con);
    
     
     $imageSliderLength = count($imageSlider);
     $newsEventsLength = count($newsEvents);
     $highlightsLength = count($highlights);
     $noticeLength = count($notice);
     
    // Quote
    $quote = $quotes[0]["quote".$lang];

    // Update count
    mysqli_query($con, "UPDATE ritwebsite_viewer_count SET total_views = total_views + 1");
    ?>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css"
    integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="style.css">
  <title> Rajiv Gandhi Institite of Techonology | Home</title>

</head>

<body onload="setThemeAndSplash()">
  <div id="splashPage" class="splash px-2" onclick="viewQuote(false)" style="z-index:2000">
    <div class="quoteContainer w-100 m-0 p-2" onmouseover="viewQuote(true)">
      <h1 id="quote">
        <?php echo $quote; ?>
      </h1>
      <h2> -Rajiv Gandhi </h2>
      <h5 id="continueText"> </h5>
    </div>
  </div>

  <?php
    $activePageName = "Home";
    require 'nav.php';
    ?>

  <!-- Image slider -->
  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">

      <?php
        for ($i=0; $i<$imageSliderLength; $i++) {
            if ($i == 0) {
                $j = $i+1;
                $indicator = 'data-bs-slide-to="'.$i. '" class="active" aria-current="true" aria-label="Slide ' .$j. '"';
            } else {
                $j = $i+1;
                $indicator = 'data-bs-slide-to="' .$i. '" aria-label="Slide ' .$j. '"';
            } ?>
      <button type="button" data-bs-target="#carouselExampleCaptions" <?php echo $indicator; ?>></button>
      <?php
        } ?>
    </div>

    <div class="carousel-inner">
      <?php
        for ($i=0;$i<$imageSliderLength; $i++) {
            $status = "carousel-item";
            if ($i ==0) {
                $status = "carousel-item active";
            }
            $type = $imageSlider[$i]["type"];
            $image = 'data:' .$type.' ;base64,'.$imageSlider[$i]["image"];
            $title = $imageSlider[$i]["title".$lang];
            $caption = $imageSlider[$i]["caption".$lang]; ?>

      <div class="overlay <?php echo $status; ?> overlay">
        <img src="<?php echo $image; ?>" class="d-block w-100 imgSlider" alt="Slide">
        <div class="carousel-caption d-none d-md-block">
          <h5><?php echo $title; ?></h5>
          <p><?php echo $caption; ?></p>
        </div>
      </div>

      <?php
        } ?>

    </div>

    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

  <!-- News and events -->
  <section class="section-style">
    <div class="row">

      <div id="multi-item-example" class="col-sm-12 col-md-8 news-events carousel slide carousel-multi-item">
        <!-- Heading -->
        <div class="row">
          <div class="col">
            <h2 class="title">
              <i class="fas fa-bell"></i>
              <?php echo $languages[$lang]["News"] ?>

              <form id="allNews" action="viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
                style="display:contents">
                <input type="hidden" name="vName" value="AllNews">
                <input type="hidden" name="table" value="ritwebsite_home_page_news">
                <a href="javascript:document.getElementById('allNews').submit()" class="view-more" id="viewMore">
                  <i class="fas fa-angle-double-right"></i>
                </a>
              </form>
            </h2>
          </div>

          <div class="controls-top col-4 align-self-center text-end">
            <button class="circle-btn" type="button" data-bs-target="#multi-item-example" data-bs-slide="prev">
              <i class="fas fa-chevron-left"></i></button>
            <button class="circle-btn" type="button" data-bs-target="#multi-item-example" data-bs-slide="next">
              <i class="fas fa-chevron-right"></i></button>
          </div>
        </div>

        <div class="row">
          <!-- Slider -->
          <div class="carousel-inner" role="listbox">

            <?php
              $newsIndex = 0;
              $totalLength = $newsEventsLength;
            while ($totalLength != 0) {
                $newsActive = '';
                if ($newsIndex == 0) {
                    $newsActive = 'active';
                } ?>

            <div class="carousel-item <?php echo $newsActive; ?>">

              <?php
                $newsRepeat = 3;
                if ($totalLength < 3) {
                    $newsRepeat = $totalLength;
                }

                for ($j=0; $j<$newsRepeat; $j++) {
                    $newsType = $newsEvents[$newsIndex]["type"];
                    if ($newsType == null) {
                        $newsImage = 'images/news-holder.png';
                    } else {
                        $newsBlob = $newsEvents[$newsIndex]["image"];
                        $newsImage = 'data:' .$newsType.' ;base64,'.$newsBlob;
                    }
                    $newsHeading = $newsEvents[$newsIndex]["heading".$lang];
                    $newsBody = $newsEvents[$newsIndex]["body".$lang];
                    $newsLink = $newsEvents[$newsIndex]["link"]; 
                    $newsLinkPos = strpos($newsLink, $pathToUploads);
                    if ($newsLinkPos !== false) {
                        $newsLinkPart = substr($newsLink, $newsLinkPos);
                        $newsLink = findHost().$newsLinkPart;
                    }
                    ?>

              <div class="col-md-4 p-1" style="float:left">
                <div class="card mb-2 news-card">
                  <img class="card-img-top" src="<?php echo $newsImage; ?>" alt="News Image" style="height:150px">
                  <div class="card-body scroll-bar">
                    <a href="<?php echo $newsLink; ?>" target="_blank">
                      <h4 class="card-title"><?php echo $newsHeading; ?></h4>
                    </a>
                    <p class="card-text">
                      <?php echo $newsBody; ?>
                    </p>
                  </div>
                </div>
              </div>

              <?php
                    $newsIndex += 1;
                } ?>

            </div>

            <?php
                $totalLength -=  $newsRepeat;
            } ?>

          </div>
        </div>
      </div>

      <div id="multi-item-example" class="col-sm-12 col-md-4">

        <div class="row">
          <div class="col">
            <h2 class="title">
              <i class="fas fa-calendar-day"></i>
              <?php echo $languages[$lang]["Events"] ?>

              <form id="allEvents" action="viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
                style="display:contents">
                <input type="hidden" name="table" value="ritwebsite_home_page_events">
                <input type="hidden" name="vName" value="AllEvents">
                <a href="javascript:document.getElementById('allEvents').submit()" class="view-more" id="viewMore">
                  <i class="fas fa-angle-double-right"></i>
                </a>
              </form>

            </h2>
          </div>
        </div>

        <div class="row ps-2 scroll-bar" style="height:300px;">
          <?php
            $noEvents = 'No Events are scheduled';
            for ($i=0;$i<count($events);$i++) {
                $noEvents ='';
                $eventHeading = $events[$i]["heading".$lang];
                $eventBody = $events[$i]["body".$lang];
                $eventLink = $events[$i]["link"];
                $eventTime = "";

                $eventDateTime = $events[$i]["event_start"];
                $eventStart = explode(" ", $eventDateTime);
                $eventStartDate = $eventStart[0];
                $eventDay = date('jS', strtotime($eventDateTime));
                $eventMonth= date('F', strtotime($eventDateTime));
                $eventYear= date('Y', strtotime($eventDateTime));
                $eventStartTime = $eventStart[1];

                $eventEnd = explode(" ", $events[$i]["event_end"]);
                $eventEndDate = $eventEnd[0];
                $eventEndTime = $eventEnd[1];

                if ($eventEndDate == $eventStartDate) {
                    //Event on same day...so show time
                    $eventTime = substr($eventStartTime, 0, -3)." to ".substr($eventEndTime, 0, -3);
                    $eventRange = " from ";
                } else {
                    //Event goes for days...show days
                    $evnetEndDateTime = $events[$i]["event_end"];
                    $eventEndDay = date('jS', strtotime($evnetEndDateTime));
                    $eventEndMonth= date('F', strtotime($evnetEndDateTime));
                    $eventEndYear= date('Y', strtotime($evnetEndDateTime));
                    $eventTime = $eventEndDay.",".$eventEndMonth." ".$eventEndYear;
                    $eventRange = " to ";
                } 
                $eventLinkPos = strpos($eventLink, $pathToUploads);
                if ($eventLinkPos !== false) {
                    $eventLinkPart = substr($eventLink, $eventLinkPos);
                    $eventLink = findHost().$eventLinkPart;
                }
                $eventStartString = $eventDay.",".$eventMonth." ".$eventYear.$eventRange.$eventTime;
                ?>


          <div class="col-12 eventCard mb-3">
            <h4>
              <a href="<?php echo $eventLink; ?>" target="_blank" rel="noopener noreferrer"
                style="color: var(--footerColor);">
                <?php echo $eventHeading; ?>
              </a>
            </h4>
            <h6 style="color: #aaaa">
              <?php echo $eventStartString; ?>
            </h6>
            <hr style="color: var(--newsevent);height: 2px">

            <div class="eventDetail">
              <?php echo $eventBody; ?>
            </div>
          </div>

          <?php
            }
            ?>
          <p><?php echo $noEvents; ?></p>
        </div>

      </div>
  </section>

  <!-- Notice Board -->

  <section class="section-style">
    <div class="col ms" style="margin-left: 20px;">
      <div class="col-12">
        <h2 class="title">
          <i class="fas fa-list-alt"></i>
          <?php echo $languages[$lang]["Notices"] ?>
        </h2>
      </div>
      <hr class="border-line">
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-6">
        <main role="main" class="container bootdey.com" style="background-color: var(--newsevent);">
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0" style="color: var(--accent);">
              <?php echo $languages[$lang]["NoticeStaff"] ?>
              <form id="allNotices" action="viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
                style="display:contents">
                <input type="hidden" name="table" value="ritwebsite_notices">
                <input type="hidden" name="vName" value="AllNotices">
                <input type="hidden" name="noticeType" value="Staffs">
                <a href="javascript:document.getElementById('allNotices').submit()" class="view-more" id="viewMore">
                  <i class="fas fa-angle-double-right"></i>
                </a>
              </form>
            </h6>

            <?php
            

            for ($i=0;$i<count($notice);$i++) {
                $noticeTitle = $notice[$i]["heading".$lang];
                $noticeBody = $notice[$i]["body".$lang];
                $noticelink = $notice[$i]["link"];
                $noticelinkPos = strpos($noticelink, $pathToUploads);
                if ($noticelinkPos !== false) {
                    $noticelinkPart = substr($noticelink, $noticelinkPos);
                    $noticelink = findHost().$noticelinkPart;
                }
                $noticeType = $notice[$i]["type"];
                if ($noticeType=='Staffs') { ?>
            <div class="media text-muted pt-3">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark"><a class="noticelinks" href="<?php echo $noticelink; ?>"
                    target="_blank">
                    <?php echo $noticeTitle; ?></a></strong>
                <?php echo $noticeBody; ?>
              </p>
            </div>
            <?php
                        
                }
            }
            
            ?>
          </div>
        </main>
      </div>
      <div class="col-sm-12 col-md-6">
        <main role="main" class="container bootdey.com" style="background-color: var(--newsevent);">
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0" style="color: var(--accent);">
              <?php echo $languages[$lang]["NoticeStud"] ?>
              <form id="allNoticesStud" action="viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
                style="display:contents">
                <input type="hidden" name="table" value="ritwebsite_notices">
                <input type="hidden" name="vName" value="AllNotices">
                <input type="hidden" name="noticeType" value="Students">
                <a href="javascript:document.getElementById('allNoticesStud').submit()" class="view-more" id="viewMore">
                  <i class="fas fa-angle-double-right"></i>
                </a>
              </form>
            </h6>
            <?php
            

            for ($i=0;$i<count($notice);$i++) {
                $noticeTitle = $notice[$i]["heading".$lang];
                $noticeBody = $notice[$i]["body".$lang];
                $noticelink = $notice[$i]["link"];
                $noticeType = $notice[$i]["type"];
                $noticelinkPos = strpos($noticelink, $pathToUploads);
                if ($noticelinkPos !== false) {
                    $noticelinkPart = substr($noticelink, $noticelinkPos);
                    $noticelink = findHost().$noticelinkPart;
                }
                if ($noticeType=='Students') { ?>
            <div class="media text-muted pt-3">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark"><a class="noticelinks" href="<?php echo $noticelink; ?>"
                    target="_blank">
                    <?php echo $noticeTitle; ?></a></strong>
                <?php echo $noticeBody; ?>
              </p>
            </div>
            <?php
                        
                }
            }
            
            ?>
          </div>
        </main>
      </div>
    </div>
  </section>

  <!-- End of Notice Board -->


  <!-- Study @ RIT -->
  <section class="section-style">
    <div class="col ms" style="margin-left: 20px;">
      <div class="col-12">
        <h2 class="title">
          <i class="fas fa-school"></i>
          <?php echo $languages[$lang]["Study@RIT"] ?>
        </h2>
      </div>
      <hr class="border-line">
    </div>

    <div class="container">
      <div class="row justify-content-center">
        <?php
        for ($i=0; $i < count($studyTypes); $i++) {
            $studyType = $studyTypes[$i]["title_en"];
            $courseHeading = $studyTypes[$i]["title".$lang];
            $courseLink ='';
            $courses = retrieveData('SELECT * FROM ritwebsite_course_detail WHERE graduate_type = "'.$studyType.'"', $con);
            for ($j=0; $j < count($courses); $j++) {
                $courseId = $courses[$j]["title_en"];
                $coursePage = findHost()."/Academics/index.php?lang=".$lang."&type=".$studyType."#".$courseId;
                $courseLink .= '
              <a href="'.$coursePage.'" class="col course-name p-1 m-1" target="_blank">'
                .$courses[$j]["title".$lang].
                '</a>';
            } ?>

        <div class="col-12 align-self-center outerBg m-0 mb-4 p-4">
          <div class=" text-center p-4 courseHeading">
            <h1><?php echo $courseHeading; ?></h1>
          </div>
          <span class="courseCards p-4">
            <div class="container">
              <div class="size row text-center">
                <?php echo $courseLink; ?>
              </div>
              <div class="col-12 text-center m-4">
                <a target="_blank" class="viewAllBtn p-2"
                  href="<?php echo findHost()."/Academics/index.php?lang=".$lang."&type=".$studyType; ?>">
                  <?php echo $languages[$lang]["ViewAll"] ?>
                </a>
              </div>
            </div>
          </span>
        </div>

        <?php
        }
        ?>
      </div>
    </div>
  </section>




  <!-- Principles Message -->
  <section class="section-style">

    <div class="col">
      <!--Heading-->
      <div class="row">
        <h2 class="title">
          <i class="fas fa-comment-alt"></i>
          <?php echo $languages[$lang]["PrincipalMessage"] ?>
        </h2>
      </div>
      <hr class="border-line">

      <!-- Image And Message -->
      <div class="container message-box">
        <div class="row justify-content-evenly align-items-center p-1 rounded">
          <?php
            for ($i=0;$i<count($principalContent);$i++) {
                $ritmsg= $principalContent[$i]["message".$lang];
                $princiName= $principalContent[$i]["principal_name".$lang];
                $ritvision = $principalContent[$i]["vision".$lang];
                $ritmission = $principalContent[$i]["mission".$lang];
                $imageType = $principalContent[$i]["type"];
                $pimage = 'data:' .$imageType.' ;base64,'.$principalContent[$i]["image"]; ?>

          <div class="col-1">
            <span class="quoteMark">"</span>
          </div>

          <div class="col-sm-10 col-md-6 p-3 messageText">
            <?php
                echo $ritmsg; ?>
            <h5 class="princiName">~ <?php echo $princiName; ?></h5>
            <h6 class="princiPost"> Principal </h6>
          </div>

          <div class="col-sm-12 col-md-3 p-3 message-image" style="background-image: url('<?php echo $pimage ?>');">
          </div>
          <?php
            }
            ?>
        </div>
      </div>

      <div class="container mt-4">
        <div class="row">

          <!-- Vission -->
          <div id="visionText" class="col-md-6 col-sm-12 text-center vismis rounded">
            <h2> <?php echo $languages[$lang]["Vision"] ?> </h2>
            <hr class="visbar my">
            <p> <?php echo $ritvision; ?> </p>
          </div>

          <!-- Mission -->
          <div id="missionText" class="col-md-6 col-sm-12 text-center vismis rounded">
            <h2> <?php echo $languages[$lang]["Mission"] ?> </h2>
            <hr class="misbar align-self-end">
            <p> <?php echo $ritmission; ?> </p>
          </div>

        </div>
      </div>

    </div>
  </section>

  <!-- Gallery -->
  <section class="section-style" style="margin-bottom:20px">

    <div class="col">
      <!--Heading-->
      <div class="row">
        <h2 class="title">
          <i class="fas fa-star"></i>
          <?php echo $languages[$lang]["RITHighlights"] ?>
        </h2>
      </div>
      <hr class="border-line">

      <div id="higlight-carousel" class="carousel slide carousel-fade z-depth-1-half" data-bs-ride="carousel">

        <?php
        $highlightSlides = '';
        $highlightIndicators = '';
        for ($i=0; $i<$highlightsLength; $i++) {
            $activeIndicator = '';
            $activeHighlight = '';
            if ($i == 0) {
                $activeIndicator = 'class="active"';
                $activeHighlight= "active";
            }
            $image = $highlights[$i]["image"];
            $type = $highlights[$i]["type"];
            $highlightImage = 'data:' .$type.' ;base64,'.$image;
            $highlightTitle = $highlights[$i]["title".$lang];
            $highlightIndicators .= '<li data-bs-target="#higlight-carousel" data-bs-slide-to="'.$i.'" '.$activeIndicator.'></li>';
            $highlightSlides .= '
        <div class="carousel-item '.$activeHighlight.'">
          <div class="view">
            <img class="d-block w-100 imgSlider" src="'.$highlightImage.'"
              alt="First slide">
            <div class="mask rgba-black-strong"></div>
          </div>
          <div class="carousel-caption">
            <h3 class="h3-responsive">'.$highlightTitle.'</h3>
          </div>
        </div>';
        }
        ?>

        <ol class="carousel-indicators">
          <?php echo $highlightIndicators; ?>
        </ol>

        <div class="carousel-inner" role="listbox">
          <?php echo $highlightSlides; ?>
        </div>

        <a class="carousel-control-prev" href="#higlight-carousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#higlight-carousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>


  </section>

  <div class="impLinks p-2 text-center">
      <h2>-Related Links-</h2>
      <a class="m-2 impLinkText" href="http://dtekerala.gov.in/">DTE Kerala</a> |
      <a class="m-2 impLinkText" href="https://ddfs.dtekerala.gov.in/">DDFS</a> |
      <a class="m-2 impLinkText" href="https://www.egrantz.kerala.gov.in/">Egrantz</a> |
      <a class="m-2 impLinkText" href="https://www.spark.gov.in/">Spark</a> |
      <a class="m-2 impLinkText" href="https://tekerala.org/">TE Kerala</a> |
      <a class="m-2 impLinkText" href="https://www.highereducation.kerala.gov.in/">Higher Education</a> |
      <a class="m-2 impLinkText" href="http://rit.ac.in/www.sitttrkerala.ac.in">SITTT Kerala</a>
  </div>

  <?php 
    require 'footer.php';
    require 'cache/createCache.php';
  ?>

</body>

</html>