<?php
  /**
   * Webpage showing gallery details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Gallery
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Gallery
   */
?>

<html>

<head>
    <!-- Header -->
    <?php 
    require '../findUrl.php';
    require '../connection.php';
    require '../retrieveData.php';
    require '../locale.php';
    require "../header.php";

    // cached_gallery_en.html
    $cachefile = "../cache/cached_gallery".$lang.".html";
    require '../cache/retrieveCache.php';

    $gallery = retrieveData('SELECT * FROM ritwebsite_gallery ORDER BY priority', $con);
    $types = retrieveData("SELECT DISTINCT category FROM ritwebsite_gallery", $con);
    ?>

    <title> Rajiv Gandhi Institite Of Techonology | Gallery</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>

<body onload="setTheme()">

    <?php
        $activePageName = "Gallery";
        require '../nav.php';
    ?>

    <section class="portfolio" id="portfolio">
        <div class="container-fluid">
            <div class="row justify-content-center">

                <div class="text-center">
                    <button class="filter-button active" data-filter="all">All</button>
                    <?php
                    for ($i=0; $i < count($types); $i++) { 
                        $imageCategory = $types[$i]["category"];?>
                    <button class="filter-button" data-filter="<?php echo $imageCategory ?>">
                        <?php echo $imageCategory; ?>
                    </button>
                        <?php
                    } 
                    ?>
                </div>

                <br>
                <?php
                $widthSets = [3,4,5,6];
                $allImages = '';
                for ($i=0;$i<count($gallery);$i++) {
                    $wIndex = array_rand($widthSets);
                    $galleryTitle = $gallery[$i]["title".$lang];
                    $galleryCaption = $gallery[$i]["caption".$lang];
                    $galleryImageType = $gallery[$i]["type"];
                    $galleryImage = 'data:' .$galleryImageType.' ;base64,'.$gallery[$i]["image"];
                    $galleryCategory = $gallery[$i]["category"];
                    $allImages .= '
                    <img src="'.$galleryImage.'" class="modal-content" id="img'.$i.'">
                    ';
                    ?>

                <div
                    class="p-2 gallery_product col-sm-<?php echo $widthSets[$wIndex]; ?> col-xs-6 filter <?php echo $galleryCategory; ?>">
                    <a class="fancybox" rel="ligthbox" onclick="showImage(<?php echo $i; ?>)">
                        <img class="roundimg img-fluid imgSettings" alt="Gallery" src="<?php echo $galleryImage; ?>" />
                        <div class='img-info'>
                            <h4><?php echo $galleryTitle; ?></h4>
                            <p><?php echo $galleryCaption; ?></p>
                        </div>
                    </a>
                </div>
                <?php } ?>

            </div>
    </section>

    <!-- The Modal -->
    <div id="imageView" class="modal">

        <!-- The Close Button -->
        <span class="close" onclick="closeModal()">&times;</span>

        <?php echo $allImages; ?>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
    <script src="script.js"></script>
    <script>
        function closeModal() {
            var id = localStorage.getItem("imageId");
            document.getElementById("imageView").style.display = "none";
            document.getElementById("img" + id).style.display = "none";
        }

        function showImage(id) {
            localStorage.setItem("imageId", id);
            document.getElementById("imageView").style.display = "block";
            document.getElementById("img" + id).style.display = "block";
        }
    </script>
    <?php 
        require "../footer.php";
        require "../cache/createCache.php";
    ?>
</body>

</html>