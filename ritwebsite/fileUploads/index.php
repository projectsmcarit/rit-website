<?php
  /**
   * Webpage showing file uploads details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Gallery
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Gallery
   */
  session_start();
?>

<html>

<head>
    <!-- Header -->
    <?php 
    require '../findUrl.php';
    require '../connection.php';
    require '../retrieveData.php';
    require '../locale.php';
    require "../header.php"; 
    if (isset($_GET["fileUploadType"])) {
        $uploadType = $_GET["fileUploadType"];
        $_SESSION["fileUploadType"] = $uploadType;
    } else {
        if (isset($_SESSION["fileUploadType"])) {
            $uploadType = $_SESSION["fileUploadType"];
        } else {
            header("Location: ".findHost());
        }
    }

    // Changing default lang for english since column name was made wrong
    $langKeyForThisPage = $lang;
    if ($lang === "_en") {
        $langKeyForThisPage = "";
    }

    $uploadTypeTitle = retrieveData('SELECT * FROM ritwebsite_office_upload_types WHERE name="'.$uploadType.'" ', $con)[0]["name".$langKeyForThisPage];
    $uploads = retrieveData('SELECT * FROM ritwebsite_office_tables WHERE uploadType="'.$uploadType.'" ', $con);
    ?>
    <title>
        Rajiv Gandhi Institite Of Techonology | <?php echo $uploadType; ?> 
        
    </title>

    <link rel="stylesheet" href="style.css">
</head>

<body onload="setTheme()">

    <?php
        $activePageName = '';
        require '../nav.php';
    ?>

    <div class="container mt-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title m-b-0">
                        <?php echo $uploadTypeTitle; ?>
                    </h5>
                </div>
                
                <div class="table-responsive">
                
                    <table class="table table-striped">
                        <thead class="thead-light">
                            <tr>
                                
                                <th scope="col"><?php echo $languages[$lang]["Sl.No"]; ?></th>
                                <th scope="col"><?php echo $uploadTypeTitle; ?></th>
                                <th scope="col"><?php echo $languages[$lang]["Action"]; ?></th>
                                
                            </tr>
                        </thead>
                        <tbody class="customtable">
                            <tr>
                            <?php
                            $sno=1;
                            for ($i=0;$i<count($uploads);$i++) {
                                $Title = $uploads[$i]["title".$lang];
                                $qtype = $uploads[$i]["uploadType"];
                                $qlink = $uploads[$i]['link']; ?>
                                <td><?php echo $sno++; ?></td>
                                <td><?php echo $Title; ?></td>
                                <td>
                                    <a href='<?php echo $qlink; ?>'>
                                        <?php echo $languages[$lang]["show"] ?>
                                    </a>
                                </td>
                                
                            </tr>
                                    <?php
                            }
                            
                            ?>
                            
                        </tbody>
                    </table>
                        
                
                </div>
                
            </div>
            <br>
            <br>
        </div>
    </div>
</div>
<?php require "../footer.php" ?>
</body>

</html>