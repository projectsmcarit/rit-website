<?php
  /**
   * Code for authentication
   * 
   * The user gets logged in here.
   * All the session variables will be set here
   *
   * PHP version 5.4.3
   *
   * @category Logic
   * @package  Login
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/login
   */
    session_start();
    require 'connection.php';
    require 'retrieveData.php';
    require 'findUrl.php';
    $serverHome = findHost();

    $lang = $_POST['lang'];
    $redirectUrl = $serverHome."/login.php?lang=".$lang;
    $actual_link = $serverHome."/login.php?lang=".$lang."&err=";

// Username password not set
if (!isset($_POST['username'], $_POST['password'])) {
        $redirectUrl = $actual_link."e1";
} else {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $stmnt = $con -> prepare('SELECT id,username,password,userType,department FROM ritwebsite_logintb WHERE username=?');
        $stmnt -> bind_param("s", $username);
        $stmnt -> execute();
        $stmnt -> bind_result($rId, $rUname, $rPass, $rUType, $rDept);
        $stmnt -> fetch();
        $stmnt -> close();
        
        //Username not found
    if ($rId === 0) {
            $redirectUrl = $actual_link."e2";
    } else {
        if ($password == $rPass) {
                $_SESSION['username'] = $rUname;
                $_SESSION['id'] = $rId;
                $_SESSION['userType'] = $rUType;
            if ($rUType != 'admin') {
                if ($rUType != 'representative') {
                        $_SESSION['department'] = $rDept;
                        $redirectUrl = $serverHome."/Admin/tables/index.php?page=Department";
                } else {
                        $redirectUrl = $serverHome."/Admin/tables/index.php?page=PensionersTables";    
                }
            } else {
                    $redirectUrl = $serverHome."/Admin/";
            }
        } else {
            // Wrong username password combination
                $redirectUrl = $actual_link."e3";
        }
    }
}
header("Location: $redirectUrl");
?>