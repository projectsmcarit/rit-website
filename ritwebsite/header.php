<?php
  /**
   * Header in all webpages
   *
   * PHP version 5.4.3
   * 
   * This file is dependent on findUrl.php
   * Make sure to import it before this
   *
   * @category Header
   * @package  Ritwebsite
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in
   */
?>
<?php
$pathToUploads = "/Admin/tables/uploads";
$serverHome = findHost();
if (isset($_GET["lang"])) {
    $lang = $_GET["lang"];
} else {
    $lang = "_en";
}
?>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicons -->
<link rel="apple-touch-icon" sizes="180x180" 
  href="<?php echo $serverHome; ?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" 
  href="<?php echo $serverHome; ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" 
  href="<?php echo $serverHome; ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" 
  href="<?php echo $serverHome; ?>/images/favicon/site.webmanifest">

<!-- BootStrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" 
    crossorigin="anonymous">
</script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" 
    crossorigin="anonymous">

<!-- Font Awesome -->
<link rel="stylesheet" 
    href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css"
    integrity="sha384-wESLQ85D6gbsF459vf1CiZ2+rr+CsxRY0RpiF1tLlQpDnAgg6rwdsUF1+Ics2bni" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/c076c3c3aa.js" crossorigin="anonymous"></script>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">

<script src="<?php echo $serverHome ?>/theme/colors.js"></script>
<script>
    localStorage.setItem("closed","false");

    function viewQuote(isShow){
        //Splash
        var splashScreen = document.getElementById("splashPage");
        if(isShow){
            splashScreen.style.top = "0";
            splashScreen.style.opacity = "1";
        }else{
            splashScreen.style.top = "-100vh";
            splashScreen.style.opacity = "0";
        }
    }
    
    function setThemeAndSplash() {
        //Splash
        var hintText = document.getElementById("continueText");
        var splashScreen = document.getElementById("splashPage");
        splashScreen.style.top = "-100vh";
        splashScreen.style.opacity = "0";
        hintText.innerHTML = "Click the space below to continue";

        //Theme
        var themeVar = document.querySelector(":root");
        var index = Math.floor(Math.random() * colorThemes.length);
        localStorage.setItem("colorIndex", index);
        for (var color in colorThemes[index]) {
            themeVar.style.setProperty(color, colorThemes[index][color]);
        }
        window.scrollTo(0,0);
    }

    function setTheme() {
        var themeVar = document.querySelector(":root");
        var index = localStorage.getItem("colorIndex");
        if (index == null) index = 0;
        for (var color in colorThemes[index]) {
            themeVar.style.setProperty(color, colorThemes[index][color]);
        }
    }

    // Add slideDown animation to Bootstrap dropdown when expanding.
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
</script>
<link rel="stylesheet" href="<?php echo $serverHome ?>/theme.css">

<!-- Setting font -->
<?php
// Malayalam style
if ($lang == "_mal") {?>
<style>
    body {
        font-family: "Baloo Chettan 2", cursive !important;
    }
</style>
<?php } else { ?>
<!-- English style -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Pacifico&display=swap');
    body {
        font-family: "Poppins", sans-serif !important;
        font-size:16px;
    }
    h2{
        font-family: 'Pacifico' , cursive;
    }
</style>
<?php } ?>