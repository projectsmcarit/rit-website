<?php
  /**
   * Webpage showing activity details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Activites
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Activites
   */
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
    require '../findUrl.php';
    require '../header.php';
    require '../connection.php';
    require '../retrieveData.php';
    require '../locale.php';
    require '../header.php';

    // cached_activity_en.html
    $cachefile = "../cache/cached_activity".$lang.".html";
    require '../cache/retrieveCache.php';

    $activities = retrieveData('SELECT * FROM ritwebsite_activity_details ORDER BY priority', $con);
    ?>
  <title>Rajiv Gandhi Institute of Technology | Activities</title>

  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"
    integrity="sha512-1cK78a1o+ht2JcaW6g8OXYwqpev9+6GqOkz9xmBN9iUUhIndKtxwILGWYOSibOKjLsEdjyjZvYDq/cZwNeak0w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.8/swiper-bundle.min.css"
    integrity="sha512-eeBbzvdY28BPYqEsAv4GU/Mv48zr7l0cI6yhWyqhgRoNG3sr+Q2Fr6751bA04Cw8SGUawtVZlugHm5H1GOU/TQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link href="style.css" rel="stylesheet">

</head>

<body onload="setTheme()">

  <?php
    $activePageName = "Activities";
    require '../nav.php';
    ?>

  <main id="main">

    <?php
    for ($i=0;$i<count($activities);$i++) {
        $activityTitle = $activities[$i]["title".$lang];
        $activityImageType = $activities[$i]["type"];
        $activityImage = 'data:' .$activityImageType.
        ' ;base64,'.$activities[$i]["image"];
      
        $activityBody1 = $activities[$i]["body1".$lang];
        $activityBody2 = $activities[$i]["body2".$lang]; ?>

    <!-- Library  -->

    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <h1><?php echo $activityTitle; ?></h1>

        <div class="section rounded 
        ch-grid margin-top margin-bottom facility-card p-4">
          <div class="row">

            <div class="col-lg-6 pt-4 pt-lg-0 content">
              <?php echo $activityBody1; ?>
            </div>

            <div class="col-lg-6" data-aos="zoom-in" data-aos-delay="100">
              <div class="about-img">
                <img src="<?php echo $activityImage; ?>" 
                alt="facility" class="img-fluid">
              </div>
            </div>

            <div class="col my-2">
              <?php echo $activityBody2; ?>
            </div>
          </div>

        </div>
    </section>
        <?php
    } ?>


  </main>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"
    integrity="sha512-A7AYk1fGKX6S2SsHywmPkrnzTZHrgiVT7GcQkLGDe2ev0aWb8zejytzS8wjo7PGEXKqJOrjQ4oORtnimIRZBtw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="assets/glightbox.min.js"></script>
  <script src="assets/swiper-bundle.min.js"></script>
  <script src="assets/js/main.js"></script>

  <?php 
    require '../footer.php'; 
    require '../cache/createCache.php';
  ?>

</body>

</html>