<?php
  /**
   * Utility to create cache
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Nav
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */

    // Cache the contents to a cache file
    $cached = fopen($cachefile, 'w');
    fwrite($cached, ob_get_contents());
    fclose($cached);
    ob_end_flush(); // Send the output to the browser
?>