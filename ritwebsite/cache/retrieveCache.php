<?php
  /**
   * Utility to retrieve cache
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Nav
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
    $cachetime = 86400;

    // Serve from the cache if it is younger than $cachetime
    if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
        echo "<!-- Cached copy, generated ".date('H:i', filemtime($cachefile))." -->\n";
        readfile($cachefile);
        exit;
    }
    ob_start(); // Start the output buffer
?>