<?php
  /**
   * Webpage showing facility details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Facilities
   * @author   Original Author <ashlymohan174@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Facilities
   */
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  
     require '../findUrl.php';
     require '../header.php';
     require '../connection.php';
     require '../retrieveData.php';
     require '../locale.php';

     // cached_facility_en.html
     $cachefile = "../cache/cached_facility".$lang.".html";
     require '../cache/retrieveCache.php';
    $auditorium = retrieveData("SELECT * FROM ritwebsite_facility_auditorium ORDER BY priority", $con);
    $facilities = retrieveData("SELECT * FROM ritwebsite_facility_detail ORDER BY priority", $con);
   
    ?>
  <title>Rajiv Gandhi Institute of Technology | Facilities</title>

  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"
    integrity="sha512-1cK78a1o+ht2JcaW6g8OXYwqpev9+6GqOkz9xmBN9iUUhIndKtxwILGWYOSibOKjLsEdjyjZvYDq/cZwNeak0w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.8/swiper-bundle.min.css"
    integrity="sha512-eeBbzvdY28BPYqEsAv4GU/Mv48zr7l0cI6yhWyqhgRoNG3sr+Q2Fr6751bA04Cw8SGUawtVZlugHm5H1GOU/TQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link href="style.css" rel="stylesheet">

</head>

<body onload="setTheme()">

  <?php
    $activePageName = "Facilities";
    require '../nav.php';
    ?>

  <main id="main">
    <?php
    for ($i=0; $i<count($facilities); $i++) {
          $imgOrder = 'order-md-1';
          $bodyOrder = 'order-md-2';
        if ($i%2 == 0) {
            $imgOrder = 'order-md-2';
            $bodyOrder = 'order-md-1';
        }
        $facilityId = $facilities[$i]["title_en"];
        $facilityTitle = $facilities[$i]["title".$lang];
        $facilityImageType = $facilities[$i]["type"];
        $facilityImage = 'data:' .$facilityImageType.' ;base64,'.$facilities[$i]["image"];
        $facilityBody = $facilities[$i]["body".$lang];
        $image = '';
        $content = '';
        $content = '
          <section id="'.$facilityId.'" class="about mt-4">
            <div class="container" data-aos="fade-up">
              <h1>'.$facilityTitle.'</h1>
              <div class="section rounded ch-grid margin-top margin-bottom facility-card p-4">
                <div class="row">
                  <div class="col-lg-6 order-sm-1 '.$imgOrder.'" data-aos="zoom-in" data-aos-delay="100">
                    <div class="about-img">
                      <img src="'.$facilityImage.'" alt="facility" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-lg-6 pt-4 pt-lg-0 order-sm-2 '.$bodyOrder.' content">
                    '.$facilityBody.'
                  </div>
                </div>

              </div>
          </section>';
        echo $content;
    }
    ?>

    <!-- ======= Auditorium Section ======= -->
    <section id="Auditorium" class="about section-bg mt-4">
      <div class="container" data-aos="fade-up">
        <h1><?php echo $languages[$lang]["Auditorium"]  ?></h1>
        <div class="section rounded ch-grid margin-top margin-bottom facility-card p-4">
          <p>
            <?php echo $languages[$lang]["AuditoriumText"] ?>
          </p>
          <div class="container-fluid" data-aos="fade-up" data-aos-delay="100">

            <div class="row g-0">

              <?php
                for ($i=0; $i < count($auditorium); $i++) { 
                    $image = $auditorium[$i]["image"];
                    $type = $auditorium[$i]["type"];
                    $auditoriumImage = 'data:' .$type.' ;base64,'.$image; ?>

              <div class="col-lg-3 col-md-4 p-2 p-2">
                <div class="gallery-item">
                  <a href="#" class="gallery-lightbox" data-gall="gallery-item">
                    <img src="<?php echo $auditoriumImage; ?>" 
                       alt="Auditorium Image" class="img-fluid">
                  </a>
                </div>
              </div>

                    <?php              
                }
                ?>

            </div>

          </div>
    </section>
    <!-- End Gallery Section -->
  </main>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"
    integrity="sha512-A7AYk1fGKX6S2SsHywmPkrnzTZHrgiVT7GcQkLGDe2ev0aWb8zejytzS8wjo7PGEXKqJOrjQ4oORtnimIRZBtw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="assets/glightbox.min.js"></script>
  <script src="assets/swiper-bundle.min.js"></script>
  <script src="assets/js/main.js"></script>

  <?php 
    require '../footer.php';
    require '../cache/createCache.php';
  ?>

</body>

</html>