<?php
    /**
     * Logic to delete a color set
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
session_start();
require '../findUrl.php';
require 'colorFilePath.php';
$path = $globalColorJSFilePath;
$file = fopen($path, 'r');
$exisitingData = fread($file, filesize($path));
$exisitingData = explode("[", $exisitingData);
fclose($file);



$file = fopen($path, 'w');
$header = $exisitingData[0];
$colorSets = $exisitingData[1];

$colorSets = explode("}", $colorSets);
$indexToDelete = $_POST["colorIndex"];
$data = '';
for ($i=0; $i<count($colorSets); $i++) {
    if ($i != $indexToDelete) {
        $data .= $colorSets[$i]."}";
    }
}

$data = substr($data, 0, strripos($data, "}"));
$data = substr($data, strpos($data, "{"));
    
$data = $header."[\n\t".$data;
fwrite($file, $data);
fclose($file);

header("Location: ".$_SERVER["HTTP_REFERER"]);
?>