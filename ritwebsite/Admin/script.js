(function($) {
    "use strict"; // Start of use strict
  
    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide');
      };
    });
  
    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function() {
      if ($(window).width() < 768) {
        $('.sidebar .collapse').collapse('hide');
      };
      
      // Toggle the side navigation when window is resized below 480px
      if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
        $("body").addClass("sidebar-toggled");
        $(".sidebar").addClass("toggled");
        $('.sidebar .collapse').collapse('hide');
      };
    });
  
    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
      if ($(window).width() > 768) {
        var e0 = e.originalEvent,
          delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
      }
    });
    
    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(e) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
      }, 1000, 'easeInOutExpo');
      e.preventDefault();
    });
  
  })(jQuery); // End of use strict

  function setAccessibilityChartStyle(fieldName) {
    var colorCode = fieldName.value
    var entireBG = document.getElementById("entireBG");
    var shadowMessage = document.getElementById("shadowMessage");
    var primaryAccent = document.getElementById("primaryAccent");
    var newsEvents = document.getElementById("newsEvents");
    var overlayColor = document.getElementById("overlayColor");
    var footerColor = document.getElementById("footerColor");
    var navBarGrad = document.getElementById("navbarGrad");

    switch (fieldName.id) {
      case 'Background':
        entireBG.style.background = colorCode;
        break;
      
      case 'Text':
        entireBG.style.color = colorCode;
        newsEvents.style.color = colorCode;
        break;

      case 'Shadow_color':
        shadowMessage.style.boxShadow = "5px 5px 2px 2px " + colorCode;
        break;

      case 'Accent':
        primaryAccent.style.color = colorCode;
        overlayColor.style.color = colorCode;
        break;
        
      case 'News_Events_BG':
        newsEvents.style.background = colorCode;
        localStorage.setItem("NewsEventsColor",colorCode)
        break;

      case 'Overlay_color':
        overlayColor.style.background = colorCode;
        localStorage.setItem("OverlayColor",colorCode);
        break;

      case 'Footer_color':
        footerColor.style.background = colorCode;
      break;
        
      default:
        break;
    }
    var neColor = localStorage.getItem("NewsEventsColor");
    var oColor = localStorage.getItem("OverlayColor");
    navBarGrad.style.background = "linear-gradient(352deg, "+ oColor +" 0%, "+ neColor +" 50%, "+ oColor +" 100%)";
  }
  