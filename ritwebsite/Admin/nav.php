<?php
    /**
     * Utility to show topbar
     *
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
?>

<form action="<?php echo findHost(); ?>/Admin/logoutLogic.php"
 id="timeoutlogoutForm"
 method="post"></form>

<script>
    var bodyTag = document.getElementsByTagName("body")[0];
    bodyTag.onmousemove = function (event) {
        reset_interval();
    }
    bodyTag.onclick = bodyTag.onmousemove;
    bodyTag.onkeypress = bodyTag.onmousemove;
    
    const timeinMS = 1800000;
 
var timer = setInterval(function(){ auto_logout() }, timeinMS);
   
function reset_interval(){
    clearInterval(timer);
    timer = setInterval(function(){ auto_logout() }, timeinMS);  
}

function auto_logout(){
    if(!alert("You have been logged out from the application, Press OK to login again!")){
        document.getElementById("timeoutlogoutForm").submit();

    }
}
  
</script>

<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <?php echo $_SESSION['username']; ?>
                </span>
                <i class="fas fa-user-tie"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#changePasswordModal">
                    <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>
                    Change Password
                </a>
            </div>
        </li>

    </ul>

</nav>
<!-- End of Topbar -->

<?php
$serverHome = findHost();
?>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="Logout Modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <form action="<?php echo $serverHome; ?>/Admin/logoutLogic.php" method="get">
                    <button class="btn btn-primary" type="submit">Logout</button>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- Change Password Modal-->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="Change Password Modal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Your Password</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="passwordForm" action="<?php echo $serverHome; ?>/Admin/changePassword.php" method="post" class="text-center">
                <div class="modal-body">
                    <label for="pswd"> New Password &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <input type="password" name="pswd" id="pswd" required>
                    <br><br>
                    <label for="repassword"> Retype Password &nbsp;</label>
                    <input type="password" name="repassword" id="repassword" required>
                    <h6 id="passwordError">
                    </h6>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a href="javascript:checkPassword()" class="btn btn-primary">Change Password</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function checkPassword(){
    var p = document.getElementById("pswd").value;
    var rp = document.getElementById("repassword").value;
    var pmsg = document.getElementById("passwordError");

    if(p == ""){
        pmsg.style.visibility = "visible";
        pmsg.innerHTML = " <i class='fas fa-bug'></i> Password cannot be empty ";
    }else{
        if(p == rp){
            document.getElementById("passwordForm").submit();
        }else{
            pmsg.style.visibility = "visible";
            pmsg.innerHTML = " <i class='fas fa-bug'></i> Passwords does not match ";
        }
    }
}
</script>