<?php
    /**
     * Webpage to add new color set
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require '../findUrl.php';
    require 'header.php';
    require 'colorFilePath.php';
    ?>
    <title>Administrator - Colors</title>
    <link href="theme.css" rel="stylesheet">
    <script src="script.js"></script>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Dashboard';
        require 'sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require 'nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h3> Insert New Color set </h3>
                    <h6>
                        Hit Tab from the first field to cycle through the 
                        colors and see the change
                    </h6>
                    <!-- Content Row -->
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                        <form method="POST" 
                        action="addColorLogic.php" 
                        enctype="multipart/form-data">
                            <?php
                            require "{$globalColorPHPFilePath}";
                            for ($i=0; $i<count($colorTypes); $i++) {
                                echo '<div class="form-group">
                                <label for="'.$colorTypes[$i].'" class="col-sm-2 col-form-label">
                                '.$colorTypes[$i].'
                                </label>
                                <div>
                                <input type="text" name="'.$colorTypes[$i].'" 
                                onfocus="setAccessibilityChartStyle('.$colorTypes[$i].')"
                                onchange="setAccessibilityChartStyle('.$colorTypes[$i].')"
                                value = "'.$sampleColorValues[$i].'"
                                id="'.$colorTypes[$i].'"
                                placeholder="Enter color code or rgba()" required></div>
                            </div>';
                            }
                            ?>

                            <button class="btn btn-success" type="submit">
                            Add color
                            </button>
                        </form>
                        </div>

                        <div class="col p-2" id="entireBG">
                            <h3> Accessibility Chart </h3>
                            <h5> Make sure the colors make the content visible </h5>
                            <hr>

                            <div class="container p-2" 
                            id="shadowMessage" style="background:#fff">
                                <h6> 
                                The shadow color is used in 
                                <ul>
                                    <li> Shadows</li>
                                    <li> Announcements</li>
                                    <li> Gallery background </li>
                                    <li> FileUpload template page background </li>
                                    <li> Teachers cards in department </li>
                                    <li> Contact us page background </li>
                                </ul>
                                </h6>
                            </div>
                            <hr>

                            <h4 id="primaryAccent"> 
                                Accent Color is the primary color.
                                Its used in all the major headings,etc
                            </h4>
                            <hr>

                            <h6 id="newsEvents">
                                News Events BG is used as the background
                                of the news and events cards and in the navbar gradient
                            </h6>
                            <hr>

                            <h6 id="overlayColor">
                                The Overlay color is a complimentary color 
                                that goes along with the primary color. This 
                                can be the same as accent color with the opacity turned down
                                <span style="color:#fff">
                                    The text which is in the accent color should
                                    be barely visible to get the desired effect.
                                </span>
                            </h6>
                            <hr>

                            <h5 id="navbarGrad"> Navbar Gradient </h5>
                            <hr>

                            <h6 id="footerColor" style="color:#fff">
                                The footer color is the color of the footer.
                                The footer has all of its text content white
                            </h6>
                            <hr>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; RIT 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

</body>

</html>