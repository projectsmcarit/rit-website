<?php
    /**
     * Logic for deleting record from table
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables
     */

require 'tableDetail.php';
require '../../connection.php';
require '../../findUrl.php';
require '../../retrieveData.php';
if (isset($_POST['tableKey'], $_POST['recordId'])) {
    $tableKey = $_POST['tableKey'];
    $selectedTable = $tableDetails[$tableKey];
    $recordId = $_POST['recordId'];
} else {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
    exit;
}

// Executing query
    $currentRecord = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." WHERE id=".$recordId, $con);
    $sql = 'DELETE FROM '.$selectedTable["tableName"].' WHERE id='.$recordId;
    mysqli_query($con, $sql);
if (mysqli_affected_rows($con) > 0) {
    
    // Updating priorities
    foreach ($selectedTable["fields"] as $key => $value) {
        if ($value["type"] === "PRIORITY") {
            $query = "UPDATE ".$selectedTable["tableName"]." SET priority = priority-1 WHERE priority > ".$currentRecord[0]["priority"];
            if (!mysqli_query($con, $query)) {
                $errFlag = 1;
            }
            break;
        }
    }

    // Redirecting if update worked
    if ($errFlag !== 1) {
        $serverHome = findHost();
        $pageId = $selectedTable["page"];
        header('Location: '.$serverHome.'/Admin/tables/?page='.$pageId.'&table='.$tableKey);
        exit;
    }
} else {
    $errFlag = 1;
}

if ($errFlag == 1) {?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deleting Record</title>
</head>

<body id="bodyId">
    <div id="loaderId" class="loader">
        <h1>The record is being deleted. You will be redirected</h1>
    </div>
    <h1>Error</h1>
    <p> <?php echo mysqli_error($con); ?> </p>
    <p> <?php print_r(error_get_last()); ?> </p>
    <a 
    href="<?php echo findHost().'/Admin/tables/?page='.$pageId.'&table='.$tableKey; ?>">
    Go Back</a>

</body>

</html>

    <?php
}
?>