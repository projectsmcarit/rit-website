<?php
/**
 * Utility to show input field during insert
 *
 * This utility uses the type field in tableDetails array
 * to determine the appropriate input field
 *
 * PHP version 5.4.3
 *
 * @category Utility
 * @package  Admin-Tables
 * @author   Original Author <hoseakalayil@gmail.com>
 * @license  https://opensource.org/licenses No License
 * @version  SVN: $1.0$
 * @link     http://rit.ac.in/admin/tables
 */

/**
 * Function to find appropriate input field
 *
 * @param Array  $fieldDetails The fields value of the table
 * @param String $fieldValue   The default value to be assigned for the field
 * @param String $imgValue     Old image to be shown during insert
 * @param String $currTable    The table name
 *
 * @return String
 *
 * This function outputs the input field required
 * It uses the fields data in tableDetails in tableDetail file
 * This function will add default value if provided
 * This can be used in both insert and update
 */
function getInputField($fieldDetails, $fieldValue, $imgValue, $currTable)
{
    if ($fieldValue == null) {
        $fieldValue = '';
    }
    $inputType = '';
    $fieldId = $fieldDetails["name"];
    $fieldName = $fieldDetails["vName"];
    $fieldType = $fieldDetails["type"];
    $fieldReq = $fieldDetails["required"];
    $tooltip = '';
    if (array_key_exists('tooltip', $fieldDetails)) {
        $tooltip = '
        <a href="#" data-toggle="tooltip" 
        title="'.$fieldDetails["tooltip"].'">
        <i class="fas fa-info-circle"></i></a>';
    }

    include '../../connection.php';
    switch ($fieldType) {
    case 'BLOB':
        if ($fieldValue != '') {
            $fieldReq = false;
        }
        $inputType = '<input type="file" class="form-control" 
        id="'.$fieldId.'" 
        placeholder="'.$fieldName.'" 
        onchange="showPreview(\''.$fieldId.'\')"
        name = "'. $fieldId .'" '. $fieldReq .'>';
        break;

    case 'DATETIME':
        //If fieldValue exist...add T
        if ($fieldValue !== '') {
            $fieldValue = str_replace(" ", "T", $fieldValue);
            $fieldValues = explode("T", $fieldValue);
            $fieldDateValue = $fieldValues[0];
            $fieldTimeValue = $fieldValues[1];
            $min = '';
        } else if (array_key_exists('validation', $fieldDetails)) {
            $min = '';
        } else {

            date_default_timezone_set('Asia/Kolkata');
            $date = date('y-m-d h:i');
            $date = str_replace(" ", "T", $date);
            $fieldValue = $date;
            $fieldValues = explode("T", $date);
            $fieldDateValue = $fieldValues[0];
            $fieldTimeValue = $fieldValues[1];
            $min = 'min="20'.$fieldDateValue.'"';
        }
            $inputType = '<input type="date"
            onchange="populateDateTime(\''.$fieldId.'\',true)"
            id="'.$fieldId.'date" name = "'. $fieldId .'" '.$min.'
            value="'.$fieldDateValue.'" 
            '. $fieldReq .'>

            <input type="time"
            onchange="populateDateTime(\''.$fieldId.'\',false)"
            id="'.$fieldId.'time" name = "'. $fieldId .'"
            value="'.$fieldTimeValue.'" 
            '. $fieldReq .'>
            
            <input type="hidden"
            id="'.$fieldId.'" name = "'. $fieldId .'"
            value="'.$fieldValue.'" 
            '. $fieldReq .'>';
        break;
    
    case 'TEXT':
        $inputType = '
        <script>
            tinymce.init({
                selector: "#'. $fieldId .'",
                plugins: "help code hr link pagebreak preview wordcount",
                toolbar: "help | undo redo | hr pagebreak | bold italic underline | link | code preview wordcount"
            });
        </script>
        <textarea class="form-control" rows="5"
        id="'.$fieldId.'" 
        name = "'. $fieldId .'" '. $fieldReq .'>
        '.$fieldValue.'
        </textarea>';
        break;

    case 'LINK':
        $length = $fieldDetails["values"];
        $inputType = '<input type="url" class="form-control" 
        id="'.$fieldId.'" 
        name = "'. $fieldId .'"
        placeholder="'.$fieldName.'" 
        maxlength="'. $length .'" 
        value = "'.$fieldValue.'" '. $fieldReq .'>';
        break;

    case 'OPTION':
    case 'DEPARTMENT':
        //Specified Values
        if (is_array($fieldDetails["values"])) {
            $inputType = '<select class="form-control" name="'.$fieldId.'" 
            id="'.$fieldId.'" '.$fieldReq.'>';
            for ($i=0; $i < count($fieldDetails["values"]); $i++) { 
                $option = $fieldDetails["values"][$i];
                $selected = '';
                if ($fieldValue === $option) {
                    $selected = 'selected';
                }
                if ($i+1 === count($fieldDetails["values"]) && $fieldValue==='') {
                    $selected = 'selected';
                }
                $inputType .= '<option value="'. $option .'" '.$selected.'>'
                . $option .'</option>';
            }
            $inputType .= '</select>';
        } else {
            //From table
            $optSql = $fieldDetails["values"];
            $extremelySpecificAdmin = 0;
            if ($fieldType === "OPTION") {
                // Filtering by departments only for function detail
                if ($currTable === "ritwebsite_department_function_detail") {
                    if ($_SESSION["userType"] !== "admin") {
                        $optSql .= " WHERE link = '' AND department = '".$_SESSION["department"]."'";
                    } else {
                        $optSql = "SELECT id AS id,title_en AS name, department FROM ritwebsite_department_function_headings WHERE link = ''";
                        $extremelySpecificAdmin = 1;
                    }
                }
            }

            $adminOnSelectFilter = '';
            if ($fieldType === "DEPARTMENT") {
                // Setting filter for function type
                if ($currTable === "ritwebsite_department_function_detail") {
                    if ($_SESSION["userType"] === "admin") {
                        $adminOnSelectFilter = " onchange=filterFunctionType('".$fieldId."')";
                    }
                }
            }
            $data = retrieveData($optSql, $con);
            $inputType = '<select class="form-control" name="'.$fieldId.'" 
            id="'.$fieldId.'" '.$fieldReq.$adminOnSelectFilter.'>';
            for ($i=0; $i < count($data); $i++) {
                $option = $data[$i]["id"];
                $optionData = $data[$i]["name"];
                if ($extremelySpecificAdmin === 1) {
                    $optionData .= " - ".$data[$i]["department"];
                }
                $selected = '';
                if ($option == $fieldValue) {
                    $selected = 'selected';
                }
                if ($i+1 === count($data) && $fieldValue==='') {
                    $selected = 'selected';
                }
                $inputType .= '<option value="'. $option .'" '.$selected.'>'
                . $optionData .'</option>';
            }
            $inputType .= '</select>';
        }
        break;

    case 'PRIORITY':
        $data = retrieveData($fieldDetails["values"], $con);
        $selectHTML = '<select class="form-control" name="'.$fieldId.'" 
        id="'.$fieldId.'" '.$fieldReq.'>';
        $inputType = $selectHTML;
        $loopLimit = $fieldValue === '' ? $data[0]["n"]+2 : $data[0]["n"]+1;
        for ($i=1; $i < $loopLimit; $i++) {
            $selected = '';
            if ($i == $fieldValue) {
                $selected = 'selected';
            }
            if ($i+1 === $loopLimit && $fieldValue==='') {
                $selected = 'selected';
            }
            $inputType .= '<option value="'. $i .'" '.$selected.'>'
            . $i .'</option>';
        }

        // If there are no options
        if ($inputType === $selectHTML) {
            $inputType .= '<option value="1" selected> 1 </option>';
        }
        $inputType .= '</select>';
        break;
    
    case 'ENUM':
        if ($fieldValue == null) {
            return '<input type="hidden" id="'.$fieldId.'" name = "'. $fieldId .'">';
        } else {
            $fieldName = 'Previous Image';
            $inputType = '
            <input type="hidden" id="'.$fieldId.'" 
            name = "'. $fieldId .'" value="'. $fieldValue .'">
            <img src="data:'.$fieldValue.';base64,'
            .$imgValue.'" class="img-fluid img-thumbnail" alt="Thumbnail"
            width="250" height="250">';
        }
        break;

    case 'UPLOAD':
        if ($fieldValue != '') {
            $fieldReq = false;
        }
        $inputType = '<input type="file" class="form-control" 
        onchange="populateFileLink(\''.$fieldId.'\', \''.$fieldDetails["values"].'\',\''.findHost().'\')"
        id="'.$fieldId.'" 
        placeholder="'.$fieldName.'" 
        name = "'. $fieldId .'" '. $fieldReq .'>';
        break;

    case 'EMAIL':
        $inputType = '<input type="email" class="form-control" 
        id="'.$fieldId.'" 
        name = "'. $fieldId .'"
        placeholder="'.$fieldName.'" 
        value = "'.$fieldValue.'" '. $fieldReq .'>';
        break;
    
    default:
        $length = '';
        if (array_key_exists("values", $fieldDetails)) {
            $length = 'maxlength="'.$fieldDetails["values"].'"';
        }
        $inputValidation = array_key_exists("pattern", $fieldDetails) ? "pattern='".$fieldDetails["pattern"]."'" : "";
        $inputType = '<input type="text" class="form-control" 
        id="'.$fieldId.'" 
        name = "'. $fieldId .'"
        '.$inputValidation.'
        placeholder="'.$fieldName.'" 
        '.$length.'
        value = "'.htmlentities($fieldValue, ENT_QUOTES).'" '. $fieldReq .'>';
        break;
    }

    // Adding preivew for selected image
    $preview = '';
    if ($fieldType == "BLOB") {
        $preview = '
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">
            Selected Image
            </label>
            <div class="col-sm-10">
                <img id="'.$fieldId.'Preview" class="img-fluid img-thumbnail" alt="Thumbnail"
                width="250" height="250">
            </div>
        </div>
        ';
    }

    // Adding * in label to mark required fields
    $reqStar = "";
    if ($fieldReq !== '') {
        $reqStar = " *";
    }
    return '
    <div class="form-group row">
        <label for="'.$fieldId.'" class="col-sm-2 col-form-label">
        '.$fieldName.$reqStar.' '.$tooltip.'
        </label>
        <div class="col-sm-10">'. $inputType .'</div>
    </div>
    '.$preview;
}