<?php
/**
 * Utility to get value of field in the table
 *
 * This utility does the pre-processing of data
 * before showing it.
 *
 * PHP version 5.4.3
 *
 * @category Utility
 * @package  Admin-Tables
 * @author   Original Author <hoseakalayil@gmail.com>
 * @license  https://opensource.org/licenses No License
 * @version  SVN: $1.0$
 * @link     http://rit.ac.in/admin/tables
 */

/**
 * Function to get value of field
 *
 * @param Array $fieldDetails This is the fields value of the table being viewed
 * @param Array $rowDetails   This is the record retrieved from SELECT query
 *
 * @return String  Table field to be shown
 *
 * This function gets the value inserted in the field
 * This value is shown in the table while viewing in tables/index.php
 */
function getFieldValue($fieldDetails, $rowDetails)
{
    include '../../connection.php';
    $finalResult = '';
    switch ($fieldDetails["type"]) {
    case 'BLOB':
        $type = $rowDetails["type"];
        $image = 'data:'.$type.' ;base64,'.$rowDetails[$fieldDetails["name"]];
        $finalResult = '
        <td class="imgCell">
            <img src="'. $image .'" class="imgInCell" alt="Thumbnail">
        </td>';
        break;

    case 'OPTION':
        $fieldValue = $rowDetails[$fieldDetails["name"]];
        if (array_key_exists("optionTable", $fieldDetails)) {
            $optionTable = $fieldDetails["optionTable"];
            $optionName = $fieldDetails["optionName"];
            $optionId = $fieldDetails["optionId"];
            $q = "SELECT ".$optionName." AS name FROM ".$optionTable." WHERE ".$optionId." = ".$fieldValue;
            $fieldValue = retrieveData($q, $con)[0]["name"];
        }
        $finalResult = '
        <td>'. $fieldValue .'</td>';
        break;

    case 'UPLOAD':
        $finalResult = '';
        break;
    
    default:
        $fieldValue = $rowDetails[$fieldDetails["name"]];
        $finalResult = '
        <td class="tableCells">'. $fieldValue .'</td>';
        break;
    }
    
    return $finalResult;
}
