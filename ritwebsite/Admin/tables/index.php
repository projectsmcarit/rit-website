<?php
    /**
     * Webpage to show all the tables
     *
     * The webpage goes through 3 selection phase
     * Phase 1 Select table group
     * Phase 2 Select specific table from group
     * Phase 3 Perform CRUD operations on the selected table
     *
     * This page acts as the home page
     * for users other than admin
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables?page=PageName&table=tableName
     */
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require '../../findUrl.php';
    require '../header.php';
    ?>

    <title>Administrator - Tables</title>
    <link href="../theme.css" rel="stylesheet">
    <style>
        .confirm {
            display: none;
        }

        .table-card {
            transition: 500ms;
        }

        .table-card:hover {
            transform: scale(1.05, 1.05)
        }
    </style>
</head>

<?php
require '../../connection.php';
require '../../retrieveData.php';
require 'getFieldValue.php';
require 'tableDetail.php';
require 'instructions.php';
$serverHome = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'
? "https" : "http") . "://$_SERVER[HTTP_HOST]";
$current_page = $serverHome.$_SERVER["REQUEST_URI"];

$backLink = '';
$isInsertable = true;
if (isset($_GET['page'])) {
    $selectedPageKey = $_GET['page'];
    $selectedPage = $selectedPageKey.' page';
    $showBack = true;
    $showPages = false;
    
    if (isset($_GET['table'])) {
        $showTable = true;
        $selectedTableKey = $_GET['table'];
        $selectedTable = $tableDetails[$selectedTableKey];
        $status = $selectedPage.' / '.$selectedTable["vTableName"];
        $backLink = findHost()."/Admin/tables/index.php?page=".$selectedPageKey;
        if($_SESSION['userType'] !== 'admin'){
            include 'noInsert.php';
            if(in_array($selectedTable["tableName"],$noInsertTables)){
                $isInsertable = false;
            }
        }
    } else {
        $backLink = findHost()."/Admin/tables";
        $showTable = false;
        $status = $selectedPage.' / Select table';
    }
} else {
    $showBack = false;
    $showPages = true;
    $status = 'Select page';
}
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Tables';
        require '../sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require '../nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-start mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Tables</h1>

                    </div>

                    <div class="d-sm-flex mb-4 align-items-baseline">
                        <?php if ($showBack) { ?>
                        <a href="<?php echo $backLink; ?>" class="back-btn"> <i class="fas fa-arrow-circle-left"></i>
                        </a>
                        <?php } ?>
                        <p><?php echo $status; ?></p>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <?php
                        // ***********************
                        // ======= PHASE 1 =======
                        // ***********************
                        if ($showPages) {
                            $color = $colors[0];
                            for ($i=0; $i<count($pages); $i++) {
                                if ($_SESSION['userType'] === 'coordinator') {
                                    if (strpos($pages[$i]["vName"], "Department") === false) {
                                        continue;
                                    }
                                }
                                $link = $current_page.'?page='.$pages[$i]["name"];
                                $name = $pages[$i]["vName"];
                                $pageColor = $pages[$i]["color"];
                                if ($pageColor != $color) {
                                    $color = $pageColor;
                                } ?>

                        <div class="col-xl-3 col-md-6 mb-4 table-card">
                            <a href="<?php echo $link; ?>">
                                <div class="card shadow h-100 py-2"
                                    style="border-left: 0.4rem solid <?php echo $pageColor; ?>">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $name; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>


                                <?php
                            }
                        } else {

                            // ***********************
                            // ======= PHASE 3 =======
                            // ***********************
                            if ($showTable) {

                                //Displaying differently for specific tables
                                $extraField = '';
                                switch ($selectedTable["tableName"]) {
                                case 'ritwebsite_main_category':
                                    $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY priority ASC", $con);
                                    break;
                                
                                case 'ritwebsite_sub_category':
                                    $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY main_id", $con);
                                    break;
                            
                                case 'ritwebsite_department_details':
                                case 'ritwebsite_department_gallery':
                                case 'ritwebsite_department_news':
                                case 'ritwebsite_staff_details':
                                case 'ritwebsite_department_function_headings':
                                case 'ritwebsite_department_function_detail':
                                    if ($_SESSION['userType'] == 'admin') {
                                        $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY department", $con);
                                        $extraField = '<th> Department </th>';
                                    } else {
                                        $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." WHERE department = '".$_SESSION["department"]."'", $con);
                                    }
                                    break;      

                                case 'ritwebsite_course_detail':
                                    $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY graduate_type", $con);
                                    break;

                                default:
                                    $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY priority", $con);
                                    if (empty($selectedTableRows)) {
                                        $selectedTableRows = retrieveData("SELECT * FROM ".$selectedTable["tableName"]." ORDER BY id DESC", $con);
                                    }
                                    break;
                                } ?>

                        <?php if($isInsertable){ ?>        
                        <form action="insert.php" method="post" class="p-1">
                            <input type="hidden" name="tableKey" id="nameOfTable"
                                value="<?php echo $selectedTableKey; ?>">
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-plus"></i> Add new record
                            </button>
                        </form>
                        <?php } 
                        if (array_key_exists($selectedTable["tableName"], $instructions)) {
                            ?>
                        
                        <a href="<?php echo $instructions[$selectedTable["tableName"]];?>"
                        class="btn btn-primary my-2 w-25" target="_blank">
                                See Instructions <i class="fas fa-external-link-alt"></i>
                        </a>
                        
                        <?php
                        } ?>

                        <table class="table table-striped table-responsive" style="height: 400px;">
                        
                            <thead class="thead-dark">
                                <!-- Serial Number -->
                                <th>Id</th>

                                <!-- Table Fields -->
                                <?php
                                for ($i=0; $i<count($selectedTable["fields"]); $i++) {
                                    if ($selectedTable["fields"][$i]["type"] == "UPLOAD") {
                                        continue;
                                    }
                                    $headingName = $selectedTable["fields"][$i]["vName"]; ?>

                                <th><?php echo $headingName; ?></th>

                                    <?php
                                } ?>
                                
                                <!-- Extra Fields -->
                                <?php echo $extraField; ?>
                                <!-- Table Operations -->
                                <th>Actions</th>
                            </thead>

                            <tbody>
                                <?php
                                for ($i=0; $i<count($selectedTableRows); $i++) {?>
                                <tr>
                                    <!-- Serial Number -->
                                    <th><?php echo $i+1; ?></th>

                                    <!-- Table Field Value -->
                                    <?php
                                    for ($j=0; $j<count($selectedTable["fields"]); $j++) {
                                        $fieldValue = getFieldValue($selectedTable["fields"][$j], $selectedTableRows[$i]);
                                        echo $fieldValue;
                                    }
                                    if ($extraField !== '') {
                                        $extraDetail = '';
                                        switch ($selectedTable["tableName"]) {
                                        case 'ritwebsite_department_details':
                                        case 'ritwebsite_department_gallery':
                                        case 'ritwebsite_department_news':
                                        case 'ritwebsite_staff_details':
                                        case 'ritwebsite_department_function_headings':
                                            $extraDetail = '<td> '.$selectedTableRows[$i]["department"].' </td>';
                                        }
                                        echo $extraDetail;
                                    }
                                    ?>

                                    <!-- Table Operation -->
                                    <td>
                                        <form action="update.php" method="post" class="p-1">
                                            <input type="hidden" name="tableKey" id="keyOfTableToUpdate"
                                                value="<?php echo $selectedTableKey; ?>">
                                            <input type="hidden" name="recordId" id="idOfRecordToUpdate"
                                                value="<?php echo $selectedTableRows[$i]["id"]; ?>">
                                            <button class="btn btn-outline-primary btn-block" type="submit">
                                                <i class="fas fa-pen-square"></i> Update
                                            </button>
                                        </form>
                                        <form action="deleteLogic.php" method="post" class="p-1">
                                            <input type="hidden" name="tableKey" id="keyOfTableToDelete"
                                                value="<?php echo $selectedTableKey; ?>">
                                            <input type="hidden" name="recordId" id="idOfRecordToDelete"
                                                value="<?php echo $selectedTableRows[$i]["id"]; ?>">
                                            <a href="javascript:showConfirm('confirmBox<?php echo $i; ?>')"
                                                class="btn btn-outline-danger btn-block">
                                                <i class="fas fa-trash-alt"></i> Delete
                                            </a>

                                            <div id="confirmBox<?php echo $i; ?>" class="confirm">
                                                <p>
                                                    <i class="fas fa-exclamation-circle"></i>
                                                    Are you sure you want to delete this record?
                                                </p>
                                                <button class="btn btn-danger btn-block" type="submit">Yes</button>
                                                <a href="javascript:hideConfirm('confirmBox<?php echo $i; ?>')"
                                                    class="btn btn-outline-danger btn-block">
                                                    No
                                                </a>
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                                    <?php
                                } ?>
                            </tbody>
                        </table>
                                <?php
                            } else {

                                // ***********************
                                // ======= PHASE 2 =======
                                // ***********************
                                
                                for ($i=0; $i<count($tables[$selectedPageKey]); $i++) {
                                    $tableLink = $current_page.'&table='.$tables[$selectedPageKey][$i]["tableName"];
                                    $tableName = $tables[$selectedPageKey][$i]["vTableName"]; ?>

                        <div class="col-xl-3 col-md-6 mb-4 table-card">
                            <a href="<?php echo $tableLink; ?>">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <?php echo $tableName; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                                    <?php
                                }
                            }
                        } ?>


                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; RIT 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Custom scripts for all pages-->
    <script src="../script.js"></script>
    <script>
        function showConfirm(name) {
            document.getElementById(name).style.display = "block";
        }

        function hideConfirm(name) {
            document.getElementById(name).style.display = "none";
        }
    </script>

</body>

</html>