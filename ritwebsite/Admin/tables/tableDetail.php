<?php
    /**
     * Utility to store all details of tables
     *
     * The three variables in this file provide
     * data to the 3 PHASES in the index page
     * $pages for PHASE 1
     * $tables for PHASE 2
     * $tableDetails for PHASE 3
     * 
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables
     */

    //To distinguish between page sections
    $colors = [
        "#823199",
        "#4e73df",
        "#99154e",
        "#751252",
        "#824562",
        "#5812ad",
        "#326ade",
        "#74ecd6",
        "#5a8e4c",
        "#34ad4c",
        "#12abcd",
    ];

    /**
     * This array provides data for PHASE 1
     * 
     * @var Array
     * This array is used to display pages.
     *
     * Pages shows up first while viewing tables.
     *
     * Each page will have a corresponding entry in tables array.
     *
     * Each page contains its respective set of tables.
     */
    $pages = [
        //Categories Section
        ["name" => "Categories", "vName" => "Categories Section","color"=>$colors[0]],

        //Home Section
        ["name" => "Home", "vName" => "Home Page","color"=>$colors[1]],

        //Departments Section
        ["name" => "Department", "vName" => "Department Page","color"=>$colors[2]],
    
        //Facility Section
        ["name" => "Facility", "vName" => "Facility Page","color"=>$colors[3]],

        //Academic Section
        ["name" => "Academic", "vName" => "Academic Page","color"=>$colors[4]],

        //Activities
        ["name" => "Activities", "vName" => "Activities Page" ,"color"=>$colors[5]],

         //Activities
         ["name" => "Gallery", "vName" => "Gallery Page" ,"color"=>$colors[6]],

         //Quick link
         ["name" => "QuickLinks", "vName" => "Quick Links" ,"color"=>$colors[7]],

         //API Detail
         ["name" => "APIAuthorization", "vName" => "API Authorization" ,"color"=>$colors[8]],
          
         //office tables
         ["name" => "OfficeTables", "vName" => "Office Tables" ,"color"=>$colors[9]],

         //pensioners tables
         ["name" => "PensionersTables", "vName" => "Pensioners Tables" ,"color"=>$colors[10]],

        ];

    
    /**
     * This array provides data for PHASE 2
     * 
     * @var Array
     * This array is used to display tables inside a page.
     *
     * NOTE that the name field in pages 
     * is used as the key to identify selected page.
     *
     * Each entry contains the tableName field 
     * which will be used in tableDetail array.
     */
        $tables = [

        //Categories
        "Categories" => [
            ["tableName" => "ritwebsite_main_category", "vTableName" => "Main Category"],
            ["tableName" => "ritwebsite_sub_category", "vTableName" => "Sub Category"],
        ],

        //Home page
        "Home" => [
            ["tableName" => "quotes", "vTableName" => "Quotes"],
            ["tableName" => "imageSlider", "vTableName" => "Image Slider"],
            ["tableName" => "announcements", "vTableName" => "Announcements"],
            ["tableName" => "newsEvents", "vTableName" => "News"],
            ["tableName" => "events", "vTableName" => "Events"],
            ["tableName" => "ritwebsite_notices", "vTableName" => "Notices"],
            ["tableName" => "principalMsg", "vTableName" => "Message from Principal"],
            ["tableName" => "highlights", "vTableName" => "RIT Highlihts"],
        ],

        //Departments
        "Department" => [
            ["tableName" => "ritwebsite_department_details", "vTableName" => "Department Details"],
            ["tableName" => "ritwebsite_department_function_headings", "vTableName" => "Department Additional Detail Types"],
            ["tableName" => "ritwebsite_department_function_detail", "vTableName" => "Department Additional Detail Content"],
            ["tableName" => "ritwebsite_department_news", "vTableName" => "Department News"],
            ["tableName" => "ritwebsite_department_gallery", "vTableName" => "Department Gallery"],
            ["tableName" => "ritwebsite_staff_details", "vTableName" => "Technical Staff Details"],
        ],

        //Facility
        "Facility" => [
            ["tableName" => "ritwebsite_facility_detail", "vTableName" => "Facility Details"],
            ["tableName" => "ritwebsite_facility_auditorium", "vTableName" => "Facility Auditorium"],

        ],

        //Academic
        "Academic" => [
            ["tableName" => "ritwebsite_academic_detail", "vTableName" => "Academic Detail"],
            ["tableName" => "ritwebsite_course_detail", "vTableName" => "Course Detail"],
        ],

        //Activities
        "Activities"=>[
            ["tableName" => "ritwebsite_activity_details", "vTableName" => "Activities"],
        ],

        //Gallery
        "Gallery"=>[
            ["tableName" => "ritwebsite_gallery", "vTableName" => "Gallery"],
        ],

        //Quick links
        "QuickLinks"=>[
            ["tableName" => "ritwebsite_quick_link", "vTableName" => "QuickLinks"],
        ],
        
        //API Auth
        "APIAuthorization"=>[
            ["tableName" => "api_auth", "vTableName" => "API Auth Details"],
            ["tableName" => "api_dept", "vTableName" => "API Department Keys"],
        ],

        //office tables
        "OfficeTables"=>[
            ["tableName" => "ritwebsite_office_upload_types", "vTableName" => "Office File Upload Category"],
            ["tableName" => "ritwebsite_office_tables", "vTableName" => "Office File Uploads"],
        ],

        //pensioners tables
        "PensionersTables"=>[
            ["tableName" => "ritwebsite_pensioners_announcements", "vTableName" => "Pensioners Announcements"],
            ["tableName" => "ritwebsite_pensioners_details", "vTableName" => "Pensioners Details"],
        ],
        ];

    

        /**
     * This array provides data for PHASE 3
     * 
     * @var Array
     * This array is used to store the complete details of the tables
     * The tableName field in tables will have a corresponding entry here
     * This is used to locate the selected table
     *
     * The tableName field is the actual name of the table in database
     *
     * The fields field contains all fields inside the table that requires user input
     *
     * FIELDS
     *
     *  The name is name of the field in the database
     *
     *  The type is used in getInputField.php to return appropriate input field
     *
     *      The types are:
     *
     *          VARCHAR -> Simple text field,
     *
     *          BLOB -> Image select field,
     *
     *          ENUM -> Not shown to user. Auto populated from selected image,
     *
     *          TEXT -> Wysiwig editor,
     * 
     *          LINK -> A textbox with URL validation
     *          Cannot use local redirection if used
     * 
     *          PRIORITY -> A drop down to adjust priority,
     *          values subfield has the query that gives numer of records
     *
     *          OPTIONS -> A drop down to show elements in values,
     *          values subfield has the query that shows the options
     *          optionTable,optionName,optionId can be assigned if
     *          the value in the field needs to be represented differently, as in
     *          ritwebsite_sub_category
     * 
     *          DEPARTMENT -> Same as OPTIONS but with added control,
     *          values subfield has the query that shows the options
     *  
     *          UPLOAD -> This is not a field in the table.
     *          Used to give file upload field
     * 
     *          DATETIME -> Datetime input option
     *
     *  The values is used for specifying values which 
     *  are important to the corresponding fields
     *
     *  The vName is the label name for the input fields
     *
     *  The required is used to specify if a field is required or not
     *
     *  The tooltip is used to show message near the label
     */
        $tableDetails = [
        "ritwebsite_main_category"=>[
            "page"=>"Categories",
            "tableName"=>"ritwebsite_main_category",
            "vTableName"=>"Main Category",
            "fields"=>[
                ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_main_category","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                ["name"=>"name_en","type"=>"VARCHAR","values"=>50,"vName"=>"Category in English","required"=>"required"],
                ["name"=>"name_mal","type"=>"VARCHAR","values"=>50,"vName"=>"Category in Malayalam","required"=>"required"],
                ["name"=>"main_link","type"=>"VARCHAR","values"=>500,"vName"=>"Link","required"=>"","tooltip"=>"Leave # if it has sub category"],
            ],
        ],

        "ritwebsite_sub_category"=>[
            "page"=>"Categories",
            "tableName"=>"ritwebsite_sub_category",
            "vTableName"=>"Sub Category",
            "fields"=>[
                ["name"=>"main_id","type"=>"OPTION",
                 "values"=>"SELECT id AS id,name_en AS name FROM ritwebsite_main_category",
                 "optionTable"=>"ritwebsite_main_category", "optionName"=>"name_en", "optionId"=>"id",
                 "vName"=>"Main Category","required"=>"required", "tooltip"=>"Set the parent of this category"],
                ["name"=>"name_en","type"=>"VARCHAR","values"=>200,"vName"=>"Sub Category in English","required"=>"required"],
                ["name"=>"name_mal","type"=>"VARCHAR","values"=>500,"vName"=>"Sub Category in Malayalam","required"=>"required"],
                ["name"=>"sub_link","type"=>"VARCHAR","values"=>500,"vName"=>"Link","tooltip"=>"While adding departments sub category name should be given in the get parameters" ,"required"=>"required"],
            ],
        ],

        // Home Page Tables
            "quotes" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_quotes",
                "vTableName"=>"Quotes",
                "fields"=>[
                    ["name"=>"quote_en","type"=>"VARCHAR","values"=>1000,"vName"=>"Quote in English","required"=>"required"],
                ],
            ],

            "imageSlider" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_image_slider",
                "vTableName"=>"Image Slider",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_home_page_image_slider","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"title_en","type"=>"VARCHAR","values"=>200,"vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","values"=>400,"vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"caption_en","type"=>"VARCHAR","values"=>300,"vName"=>"Caption in English","required"=>""],
                    ["name"=>"caption_mal","type"=>"VARCHAR","values"=>800,"vName"=>"Caption in Malayalam","required"=>""],
                ],
            ],

            "announcements" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_announcement",
                "vTableName"=>"Announcements",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_home_page_announcement WHERE expiry > NOW()","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"announcement_en","type"=>"VARCHAR","values"=>1000,"vName"=>"Announcement in English","required"=>"required"],
                    ["name"=>"announcement_mal","type"=>"VARCHAR","values"=>1000,"vName"=>"Announcement in Malayalam","required"=>"required"],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the announcement to."],
                    ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>""],
                    ["name"=>"expiry","type"=>"DATETIME","values"=>500,"vName"=>"Expiry","required"=>"required","tooltip"=>"Date at which announcement should expire"],
                ],
            ],

            "newsEvents" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_news",
                "vTableName"=>"News & Events",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_home_page_news WHERE expiry > NOW()","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>""],
                    ["name"=>"heading_en","type"=>"VARCHAR","values"=>500,"vName"=>"Heading in English","required"=>"required"],
                    ["name"=>"heading_mal","type"=>"VARCHAR","values"=>600,"vName"=>"Heading in Malayalam","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","values"=>600,"vName"=>"Body in English","required"=>""],
                    ["name"=>"body_mal","type"=>"TEXT","values"=>600,"vName"=>"Body in Malayalam","required"=>""],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the news to."],
                    ["name"=>"link","type"=>"LINK","values"=>200,"vName"=>"Link","required"=>"required","tooltip"=>"Leave # if there is no hyperlink"],
                    ["name"=>"expiry","type"=>"DATETIME","values"=>500,"vName"=>"Expiry","required"=>"required","tooltip"=>"Date at which news should expire"],
                ],
            ],

            "events" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_events",
                "vTableName"=>"Events",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_home_page_events WHERE event_end > NOW()","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"event_start","type"=>"DATETIME","values"=>500,"vName"=>"Event start time","required"=>"required"],
                    ["name"=>"event_end","type"=>"DATETIME","values"=>500,"vName"=>"Event end time","required"=>"required"],
                    ["name"=>"heading_en","type"=>"VARCHAR","values"=>500,"vName"=>"Heading in English","required"=>"required"],
                    ["name"=>"heading_mal","type"=>"VARCHAR","values"=>600,"vName"=>"Heading in Malayalam","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","values"=>400,"vName"=>"Body in English","required"=>""],
                    ["name"=>"body_mal","type"=>"TEXT","values"=>500,"vName"=>"Body in Malayalam","required"=>""],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the event to."],
                    ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>"required","tooltip"=>"Leave # if there is no hyperlink"],
                ],
            ],

            "ritwebsite_notices"=>[
                "page"=>"Home",
                "tableName"=>"ritwebsite_notices",
                "vTableName"=>"Notices",
                "fields"=>[
                        ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_notices WHERE expireAt > NOW()","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                        ["name"=>"heading_en","type"=>"VARCHAR","values"=>500,"vName"=>"Heading in English","required"=>"required"],
                        ["name"=>"heading_mal","type"=>"VARCHAR","values"=>600,"vName"=>"Heading in Malayalam","required"=>"required"],
                        ["name"=>"body_en","type"=>"TEXT","values"=>400,"vName"=>"Body in English","required"=>"required"],
                        ["name"=>"body_mal","type"=>"TEXT","values"=>500,"vName"=>"Body in Malayalam","required"=>"required"],
                        ["name"=>"link","type"=>"VARCHAR","values"=>500,"vName"=>"Link","required"=>"","tooltip"=>"Leave # if there is no hyperlink"],
                        ["name"=>"createdAt","type"=>"DATETIME","values"=>500,"vName"=>" Created Date","required"=>"required","tooltip"=>"Date at which notice is created"],
                        ["name"=>"expireAt","type"=>"DATETIME","values"=>500,"vName"=>" Expire Date","required"=>"required","tooltip"=>"Date at which notice should expire"],
                        ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the quick link to."],
                        ["name"=>"type","type"=>"OPTION", "values"=>['Staffs','Students'], "vName"=>"To Whom","required"=>"required"],
                    ],
            ],

            "principalMsg" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_principal_message",
                "vTableName"=>"Message from principal",
                "fields"=>[
                    ["name"=>"principal_name_en","type"=>"VARCHAR","values"=>100,"vName"=>"Name of Principal in English","required"=>"required"],
                    ["name"=>"principal_name_mal","type"=>"VARCHAR","values"=>100,"vName"=>"Name of Principal in Malayalam","required"=>"required"],
                    ["name"=>"message_en","type"=>"TEXT","values"=>0,"vName"=>"Message in English","required"=>"required"],
                    ["name"=>"message_mal","type"=>"TEXT","values"=>0,"vName"=>"Message in Malayalam","required"=>"required"],
                    ["name"=>"vision_en","type"=>"TEXT","values"=>0,"vName"=>"Vision Message in English","required"=>"required"],
                    ["name"=>"vision_mal","type"=>"TEXT","values"=>0,"vName"=>"Vision Message in Malayalam","required"=>"required"],
                    ["name"=>"mission_en","type"=>"TEXT","values"=>0,"vName"=>"Mission Message in English","required"=>"required"],
                    ["name"=>"mission_mal","type"=>"TEXT","values"=>0,"vName"=>"Mission Message in Malayalam","required"=>"required"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                ],
            ],


            "highlights" => [
                "page"=>"Home",
                "tableName"=>"ritwebsite_home_page_highlights",
                "vTableName"=>"RIT Highlights",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_home_page_highlights","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"title_en","type"=>"VARCHAR","values"=>200,"vName"=>"Title in English","required"=>""],
                    ["name"=>"title_mal","type"=>"VARCHAR","values"=>300,"vName"=>"Title in Malayalam","required"=>""],
                ],
            ],
        // End Of Home Page Tables

        // Departments page Tables

            "ritwebsite_department_details" =>[
                "page"=>"Department",
                "tableName"=>"ritwebsite_department_details",
                "vTableName"=>"Department Details",
                "fields"=> [
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required","tooltip"=>"Image shown in the top"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    
                    ["name"=>"about_us_en","type"=>"TEXT","values"=>0,"vName"=>"About Us in English","required"=>"required","tooltip"=>"This is the first paragraph in about us section"],
                    ["name"=>"about_us_mal","type"=>"TEXT","values"=>0,"vName"=>"About Us in Malayalam","required"=>"required"],
                    
                    ["name"=>"about_us_sub_1_en","type"=>"TEXT","values"=>0,"vName"=>"Vision in English","required"=>"required","tooltip"=>"This is the first sub-paragraph in about us section"],
                    ["name"=>"about_us_sub_1_mal","type"=>"TEXT","values"=>0,"vName"=>"Vision in Malayalam","required"=>"required"],
                    
                    ["name"=>"about_us_sub_2_en","type"=>"TEXT","values"=>0,"vName"=>"Mission in English","required"=>"required","tooltip"=>"This is the second sub-paragraph in about us section"],
                    ["name"=>"about_us_sub_2_mal","type"=>"TEXT","values"=>0,"vName"=>"Mission in Malayalam","required"=>"required"],
                    
                    ["name"=>"hod_image","type"=>"BLOB","vName"=>"Image of Director/Hod","required"=>"required","tooltip"=>"Image of director/hod"],
                    ["name"=>"hod_type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],

                    ["name"=>"main_content_en","type"=>"TEXT","values"=>0,"vName"=>"Message from Director/HOD in English","required"=>"required","tooltip"=>"This is the message from director/HOD"],
                    ["name"=>"main_content_mal","type"=>"TEXT","values"=>0,"vName"=>"Message from Director/HOD in Malayalam","required"=>"required"],
                ],
            ],

            "ritwebsite_department_news"=>[
                "page"=>"Department",
                "tableName"=>"ritwebsite_department_news",
                "vTableName"=>"Department News",
                "fields"=>[
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>""],
                    ["name"=>"heading_en","type"=>"VARCHAR","values"=>500,"vName"=>"Heading in English","required"=>"required"],
                    ["name"=>"heading_mal","type"=>"VARCHAR","values"=>600,"vName"=>"Heading in Malayalam","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","values"=>400,"vName"=>"Body in English","required"=>""],
                    ["name"=>"body_mal","type"=>"TEXT","values"=>500,"vName"=>"Body in Malayalam","required"=>""],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "You can upload a file to redirect the link to."],
                    ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>"required","tooltip"=>"Link will be auto filled if file is selected"],
                    ["name"=>"expiry","type"=>"DATETIME","values"=>500,"vName"=>"Expiry","required"=>"required","tooltip"=>"Date at which news should expire"],
                ]
            ],

            "ritwebsite_department_gallery"=>[
                "page"=>"Department",
                "tableName"=>"ritwebsite_department_gallery",
                "vTableName"=>"Department Gallery",
                "fields"=>[
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                ]
            ],

            "ritwebsite_staff_details" => [
                "page"=>"Department",
                "tableName"=>"ritwebsite_staff_details",
                "vTableName"=>"Technical Staff Details",
                "fields"=>[
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>""],
                    ["name"=>"name","type"=>"VARCHAR","values"=>200,"vName"=>"Name","required"=>"required"],
                    ["name"=>"qualification","type"=>"VARCHAR","values"=>200,"vName"=>"Qualification","required"=>"","tooltip"=>"Please use the format \n GraduateType@@DegreeName@@UniversityName@@PassoutYear"],
                    ["name"=>"designation","type"=>"VARCHAR","values"=>200,"vName"=>"Designation","required"=>"required"],
                    ["name"=>"experience","type"=>"VARCHAR","vName"=>"Experience","required"=>""],
                    ["name"=>"phoneno","type"=>"VARCHAR","values"=>10,"vName"=>"Phone Number","required"=>"required","pattern"=>"[0-9]{10}"],
                    ["name"=>"emailid","type"=>"EMAIL","values"=>200,"vName"=>"Email Id","required"=>"required"],
                    ["name"=>"penno","type"=>"VARCHAR","values" => 30, "vName"=>"PEN No","required"=>"","pattern"=>"[0-9]{1,10}"],
                    ["name"=>"technical_skills","type"=>"VARCHAR","vName"=>"Technical Skills","required"=>""],
                ],
            ],

            "ritwebsite_department_function_headings"=>[
                "page"=>"Department",
                "tableName"=>"ritwebsite_department_function_headings",
                "vTableName"=>"Department Additional Detail Types",
                "fields"=>[
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments') AND name_en != 'Administration'","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"icon","type"=>"ICON","values"=>200,"vName"=>"Icon","required"=>"required"],
                    ["name"=>"title_en","type"=>"VARCHAR","values"=>200,"vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","values"=>200,"vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"subHeading_en","type"=>"VARCHAR","values"=>500,"vName"=>"Sub Heading in English","required"=>"required"],
                    ["name"=>"subHeading_mal","type"=>"VARCHAR","values"=>500,"vName"=>"Sub Heading in Malayalam","required"=>"required"],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, You can upload a file to redirect the link to."],
                    ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>"","tooltip"=>"If you are not using the file upload then leave this blank. Redirect will be made automatically"],
                ]
            ],

            "ritwebsite_department_function_detail"=>[
                "page"=>"Department",
                "tableName"=>"ritwebsite_department_function_detail",
                "vTableName"=>"Department Additional Detail Content",
                "fields"=>[
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments') AND name_en != 'Administration'","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"","tooltip"=>"You can add either a image or text or both"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>""],
                    ["name"=>"body_en","type"=>"TEXT","vName"=>"Body in English","required"=>""],
                    ["name"=>"body_mal","type"=>"TEXT","vName"=>"Body in Malayalam","required"=>""],
                    ["name"=>"function_type","type"=>"OPTION",
                     "values"=>"SELECT id AS id,title_en AS name FROM ritwebsite_department_function_headings",
                     "optionTable"=>"ritwebsite_department_function_headings", "optionName"=>"title_en", "optionId"=>"id",
                     "vName"=>"Additional Content Type","required"=>"required", "tooltip"=>"Set the type of the content you are adding"],
                      ]
                    ],      

        // End of Department tables

        // Facility  page Tables

            "ritwebsite_facility_detail"=>[
                "page"=>"Facility",
                "tableName"=>"ritwebsite_facility_detail",
                "vTableName"=>"Facility Details",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_facility_detail","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"title_en","type"=>"VARCHAR", "values"=>200, "vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR", "values"=>400,"vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","vName"=>"Body in English","required"=>"required"],
                    ["name"=>"body_mal","type"=>"TEXT","vName"=>"Body in Malayalam","required"=>"required"],
                ],
            ],

            "ritwebsite_facility_auditorium"=>[
                "page"=>"Facility ",
                "tableName"=>"ritwebsite_facility_auditorium",
                "vTableName"=>"Facility Auditorium",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_facility_auditorium","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                ],
            ],

        // End of  Facility page Tables

        // Academic page tables
            "ritwebsite_academic_detail"=>[
                "page"=>"Academic",
                "tableName"=>"ritwebsite_academic_detail",
                "vTableName"=>"Academic Detail",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_academic_detail","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"title_en","type"=>"VARCHAR","vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","vName"=>"Body in English","required"=>"required"],
                    ["name"=>"body_mal","type"=>"TEXT","vName"=>"Body in Malayalam","required"=>"required"],
                ],
            ],

            "ritwebsite_course_detail"=>[
                "page"=>"Academic",
                "tableName"=>"ritwebsite_course_detail",
                "vTableName"=>"Course Detail",
                "fields"=>[
                    ["name"=>"graduate_type","type"=>"OPTION",
                     "values"=>"SELECT title_en AS id,title_en AS name FROM ritwebsite_academic_detail",
                     "vName"=>"Graduate Type","required"=>"required", "tooltip"=>"Set the type of course"],
                    ["name"=>"title_en","type"=>"VARCHAR","vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"body_en","type"=>"TEXT","vName"=>"Body in English","required"=>"required"],
                    ["name"=>"body_mal","type"=>"TEXT","vName"=>"Body in Malayalam","required"=>"required"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>""],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"intake","type"=>"VARCHAR","vName"=>"Intake","required"=>"required"],
                    ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments') AND name_en != 'Administration'","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                ],
            ],
        // End of academic pages

        // Start of Activity
            "ritwebsite_activity_details"=>[
                "page"=>"Activity",
                "tableName"=>"ritwebsite_activity_details",
                "vTableName"=>"Activities",
                "fields"=>[
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_activity_details","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required","tooltip"=>"Image shown in the top"],
                    ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                    ["name"=>"title_en","type"=>"VARCHAR","values"=>50,"vName"=>"Heading in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","values"=>100,"vName"=>"Heading in Malayalam","required"=>"required"],
                    ["name"=>"body1_en","type"=>"TEXT","vName"=>"Text Aside Picture in English","required"=>""],
                    ["name"=>"body1_mal","type"=>"TEXT","vName"=>"Text Aside Picture in Malayalam","required"=>""],
                    ["name"=>"body2_en","type"=>"TEXT","vName"=>"Text Below Picture in English","required"=>""],
                    ["name"=>"body2_mal","type"=>"TEXT","vName"=>"Text Below Picture in Malayalam","required"=>""],
                ],
            ],
        // End of activity

        // Start of Gallery
                "ritwebsite_gallery" => [
                    "page" => "Gallery",
                    "tableName" => "ritwebsite_gallery",
                    "vTableName" => "Gallery",
                    "fields" => [
                        ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_gallery","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                        ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required","tooltip"=>"Image shown in the top"],
                        ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                        ["name"=>"title_en","type"=>"VARCHAR","values"=>200,"vName"=>"Title in English","required"=>""],
                        ["name"=>"title_mal","type"=>"VARCHAR","values"=>300,"vName"=>"Title in Malayalam","required"=>""],
                        ["name"=>"caption_en","type"=>"VARCHAR","values"=>300,"vName"=>"Caption in English","required"=>""],
                        ["name"=>"caption_mal","type"=>"VARCHAR","values"=>400,"vName"=>"Caption in Malayalam","required"=>""],
                        ["name"=>"category","type"=>"VARCHAR","values"=>100,"vName"=>"Category","required"=>"required"],
                    ],
                ],
        // End of Gallery


        // Start of quick links
                    "ritwebsite_quick_link"=>[
                        "page"=>"QuickLinks",
                        "tableName"=>"ritwebsite_quick_link",
                        "vTableName"=>"Quick Links",
                        "fields"=>[
                            ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_quick_link","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                            ["name"=>"name_en","type"=>"VARCHAR","values"=>50,"vName"=>"Category in English","required"=>"required"],
                            ["name"=>"name_mal","type"=>"VARCHAR","values"=>200,"vName"=>"Category in Malayalam","required"=>"required"],
                            ["name"=>"file","type"=>"UPLOAD","values"=>"main_link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the quick link to."],
                            ["name"=>"main_link","type"=>"VARCHAR","values"=>500,"vName"=>"Link","required"=>"","tooltip"=>"The link to file will be auto generated, on file select. If you are using a link make sure no file is selected since it has more precedence"],
                        ],
                    ],
        // End of Quick Links

        // API AUTH
                    "api_auth"=>[
                        "page"=>"APIAuthorization",
                        "tableName"=>"ritwebsite_api_details",
                        "vTableName"=>"Api Auth",
                        "fields"=>[
                            ["name"=>"apiKey","type"=>"VARCHAR","values"=>200,"vName"=>"Api Key","required"=>"required"],
                            ["name"=>"username","type"=>"VARCHAR","values"=>200,"vName"=>"User name","required"=>"required"],
                            ["name"=>"password","type"=>"VARCHAR","values"=>200,"vName"=>"Password","required"=>"required"],
                        ],
                    ],
                    "api_dept"=>[
                        "page"=>"APIAuthorization",
                        "tableName"=>"ritwebsite_api_departments",
                        "vTableName"=>"Api Department Values",
                        "fields"=>[
                            ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                            ["name"=>"apiValue","type"=>"VARCHAR","values"=>200,"vName"=>"Corresponding API key","required"=>"required","tooltip"=>"This is case sensitive. It should be as in the API"],
                        ],
                    ],
        // End of API AUTH

        // office tables
        "ritwebsite_office_upload_types"=>[
            "page"=>"OfficeTables",
            "tableName"=>"ritwebsite_office_upload_types",
            "vTableName"=>"Office File Upload Categories",
            "fields"=>[                    
                    ["name"=>"name","type"=>"VARCHAR","values"=>200,"vName"=>"Name of the category in Malayalam","required"=>"required"],
                    ["name"=>"name_mal","type"=>"VARCHAR","values"=>200,"vName"=>"Name of the category","required"=>"required"],
                ],
            ],

        "ritwebsite_office_tables"=>[
            "page"=>"OfficeTables",
            "tableName"=>"ritwebsite_office_tables",
            "vTableName"=>"Office File Uploads",
            "fields"=>[                    
                    ["name"=>"uploadType","type"=>"OPTION",
                    "values"=>"SELECT name AS id,name AS name FROM ritwebsite_office_upload_types",
                    "vName"=>"Upload Category","required"=>"required", "tooltip"=>"Set the category this file should be uploaded to"],
                    ["name"=>"title_en","type"=>"VARCHAR","values"=>200,"vName"=>"Title in English","required"=>"required"],
                    ["name"=>"title_mal","type"=>"VARCHAR","values"=>300,"vName"=>"Title in Malayalam","required"=>"required"],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the link to."],
                    ["name"=>"link","type"=>"LINK","values"=>100,"vName"=>"Link","required"=>"required","tooltip"=>"The link is auto generated if you upload file"],
                ],
            ],
        //pensioners announcements
        "ritwebsite_pensioners_announcements"=>[
            "page"=>"PensionersTables",
            "tableName"=>"ritwebsite_pensioners_announcements",
            "vTableName"=>"Pensioners Announcements",
            "fields"=>[  
                    ["name"=>"priority","type"=>"PRIORITY","values"=>"SELECT count(id) AS n FROM ritwebsite_pensioners_announcements WHERE expiry > NOW()","vName"=>"Priority","required"=>"required", "tooltip"=>"Set the priority of this entry"],
                    ["name"=>"announcement_en","type"=>"VARCHAR","values"=>1000,"vName"=>"Announcement in English","required"=>"required"],
                    ["name"=>"announcement_mal","type"=>"VARCHAR","values"=>1000,"vName"=>"Announcement in Malayalam","required"=>"required"],
                    ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the announcement to."],
                    ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>""],
                    ["name"=>"expiry","type"=>"DATETIME","values"=>500,"vName"=>"Expiry","required"=>"required","tooltip"=>"Date at which announcement should expire"],
                ],
            ],

        "ritwebsite_pensioners_details"=>[
            "page"=>"PensionersTables",
            "tableName"=>"ritwebsite_pensioners_details",
            "vTableName"=>"Pensioners Details",
            "fields"=>[ 
                 
                ["name"=>"image","type"=>"BLOB","vName"=>"Image","required"=>"required"],
                ["name"=>"type","type"=>"ENUM","values"=>['jpeg','jpg','png'], "vName"=>"Image Type","required"=>"required"],
                ["name"=>"name","type"=>"VARCHAR","values"=>200,"vName"=>"Name","required"=>"required"],
                ["name"=>"department","type"=>"DEPARTMENT","values"=>"SELECT name_en AS id,name_en AS name FROM ritwebsite_sub_category WHERE main_id = (SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')","vName"=>"Department","required"=>"required", "tooltip"=>"Set the department to add this data to"],
                ["name"=>"designation","type"=>"VARCHAR","values"=>200,"vName"=>"Designation","required"=>"required"],
                ["name"=>"phoneno","type"=>"VARCHAR","values"=>10,"vName"=>"Phone Number","required"=>"","pattern"=>"[0-9]{10}"],
                ["name"=>"emailid","type"=>"EMAIL","values"=>200,"vName"=>"Email Id","required"=>""],
                ["name"=>"contact_detail","type"=>"TEXT", "vName"=>"Contact Details","required"=>"required"],
                ["name"=>"retire_date","validation"=>"yes","type"=>"DATETIME","vName"=>"Retirement Date","required"=>"required"],
                ["name"=>"file","type"=>"UPLOAD","values"=>"link","vName"=>"File","required"=>"", "tooltip" => "Optionally, you can upload a file to redirect the details to."],
                ["name"=>"link","type"=>"LINK","values"=>500,"vName"=>"Link","required"=>""],
                ],
            ],
        ];