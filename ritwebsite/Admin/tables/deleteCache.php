<?php
  /**
   * Utility to delete cache files
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Nav
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */

   // For changes in category and quick link remove all cache
   $cacheResetFiles = ["categories","quickLinks"];
   $nameNeedle = "../../cache/cached_$cacheName*.html";
   if (in_array($cacheName, $cacheResetFiles)) {
    $nameNeedle = "../../cache/*.html";
   }

   foreach (glob($nameNeedle) as $filename) {
     unlink($filename);
   }
?>