<?php
    /**
     * Utitlity to restrict insert
     *
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables?page=PageName&table=tableName
     */
    $noInsertTables = [
        'ritwebsite_home_page_principal_message',
        'ritwebsite_department_details'];
?>