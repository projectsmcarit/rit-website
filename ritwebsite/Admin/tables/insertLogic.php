<?php
    /**
     * Logic to insert record
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables
     */
session_start();
$_SESSION['tableKey'] = $_POST['tableKey'];

//Checking if POST was made
require 'tableDetail.php';
if (isset($_POST['tableKey'])) {
    $tableKey = $_POST['tableKey'];
    $selectedTable = $tableDetails[$tableKey];
} else {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
    exit;
}


//Setting fields and values for INSERT
$fields = '(';
$params = '(';

require '../../connection.php';
require '../../retrieveData.php';
for ($i=0; $i<count($selectedTable["fields"]); $i++) {
    $currentField = $selectedTable["fields"][$i];

    if ($currentField["type"] == "BLOB") {
        //If file was not uploaded
        if ($_FILES[$currentField["name"]]['name'] == null) {
            //Redirect back if file is required
            if ($currentField["required"] == "required") {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1');
                exit;
            } else {
                //Continue adding other fields if file is not required
                continue;
            }
        } else {
            //If file was uploaded
            $allowed = ['jpeg','jpg','png','gif','bmp'];
            $extension = strtolower(pathinfo($_FILES[$currentField["name"]]['name'], PATHINFO_EXTENSION));
            //Check if file is allowed
            if (in_array($extension, $allowed)) {
                if ($_FILES['image']['error'] !== 0) {
                    header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1.5');
                    exit;
                }
                $img = base64_encode(file_get_contents($_FILES[$currentField["name"]]['tmp_name']));
                $type = getimagesize($_FILES[$currentField["name"]]['tmp_name'])["mime"];
                $params .= '"'.$img.'",';
                $fields .= $currentField["name"].",";
            } else {
                //Unsupported file
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e2');
                exit;
            }
        }
    } elseif ($currentField["type"] == "ENUM") {
        //If file was uploaded type will be set
        if ($type != null) {
            $params .= '"'.$type.'",';
            $type = null;
            $fields .= $currentField["name"].",";
        }
    } elseif ($currentField["type"] == "UPLOAD") {
        //If file was not uploaded
        if ($_FILES[$currentField["name"]]['name'] == null) {
            //Redirect back if file is required
            if ($currentField["required"] == "required") {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1');
                exit;
            } else {
                //Continue adding other fields if file is not required
                continue;
            }
        } else {
            //If file was uploaded
            $autoGenPath = $_POST[$currentField["values"]];
            $pathFrags = explode("/", $autoGenPath);
            $fileName = end($pathFrags);
            $targetFile = getcwd().DIRECTORY_SEPARATOR."uploads/".$fileName;
            if (move_uploaded_file($_FILES[$currentField["name"]]['tmp_name'], $targetFile)) {
                continue;
            } else {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e3');
                exit;
            }
        }
    } elseif ($currentField["type"] == "PRIORITY") {
        $inputPriority = $_POST["priority"];
        $totalNum = retrieveData("SELECT count(id) FROM ".$selectedTable["tableName"], $con);
        //New category added to end of list
        if ($totalNum[0]["count(id)"] < $inputPriority) {
            $params .= $inputPriority.',';
            $fields .= "priority,";
        } else {
            //Update priority of other fields
            $query = "UPDATE ".$selectedTable["tableName"]." SET priority = priority+1 WHERE priority >= ".$inputPriority;
            if (mysqli_query($con, $query)) {
                $params .= $inputPriority.',';
                $fields .= "priority,";
            } else {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e4');
                exit;
            }
        }
    } elseif ($currentField["type"] == "DEPARTMENT") {
        if ($_SESSION["userType"] === "admin") {
            $fields .= "department,";
            $params .= '"'.$_POST["department"].'",';
        } else {
            $fields .= "department,";
            $params .= '"'.$_SESSION["department"].'",';
        }
    } else {
        $data = addslashes($_POST[$currentField["name"]]);
        $params .= '"'.$data.'",';
        $fields .= $currentField["name"].",";
    }
}
//Finished Setting fields and values

//Trimming trailing commas
$fields = rtrim($fields, ",").')';
$params = rtrim($params, ",").')';

// Executing query
require '../../findUrl.php';
$sql = 'INSERT INTO '.$selectedTable["tableName"].' '.$fields.' VALUES '.$params;
if (mysqli_query($con, $sql)) {
    $serverHome = findHost();
    $pageId = $selectedTable["page"];
    header('Location: '.$serverHome.'/Admin/tables/?page='.$pageId.'&table='.$tableKey);
    exit;
} else {
    $errFlag = 1;
}

if ($errFlag == 1) {?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inserting New Record</title>
</head>

<body id="bodyId">
    <h1 class="d-block">Error</h1><br>
    <p class="d-block"> <?php echo mysqli_error($con); ?> </p><br>
    <p class="d-block"> <?php echo error_get_last(); ?> </p>
    <a href="<?php echo findHost().'/Admin/tables/?page='.$pageId.'&table='.$tableKey; ?>" rel="noopener noreferrer">Go Back</a>
</body>

</html>
    <?php
}
?>