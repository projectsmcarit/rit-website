<?php
    /**
     * Utitlity to show instructions
     *
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables?page=PageName&table=tableName
     */

     $instructions = [
         "ritwebsite_department_function_headings" => "instructions/dept_additional_detail_type.pdf",
         "ritwebsite_department_function_detail" => "instructions/dept_additional_detail_content.pdf"
     ];
?>