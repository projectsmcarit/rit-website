<?php
    /**
     * Webpage to insert records
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables/insert
     */
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
        require '../../findUrl.php';
        require '../header.php';
        require 'getInputField.php';
        require 'tableDetail.php';

    // Getting error message back from INSERT
    $errMsg = '';
    if (isset($_GET['err'])) {
        $err = $_GET['err'];
        switch ($err) {
        case 'e1':
            $specificErrorMessage = 'You have to upload image';
            break;
        
        case 'e1.5':
            $specificErrorMessage = 'File upload limit exceeded';
            break;

        case 'e2':
            $specificErrorMessage = 'Unsupported file type';
            break;
        
        case 'e3':
            $specificErrorMessage = 'Could not upload file to server';
            break;

        case 'e4':
            $specificErrorMessage = 'Error while changing priority';
            break;

        default:
            $specificErrorMessage = 'Error while inserting';
        }
        $errMsg = '
        <div class="d-sm-flex mb-4 align-items-baseline" style="color:red">
            <p><i class="fas fa-bug"></i>'.$specificErrorMessage.'</p>
        </div>
        ';
    }

    if (isset($_POST['tableKey'])) {
        $tableKey = $_POST['tableKey'];
    } else {
        $tableKey = $_SESSION['tableKey'];
    }
    $backLink = findHost()."/Admin/tables/index.php?page=".$tableDetails[$tableKey]["page"]."&table=".$tableKey;
    //Getting table details
    $selectedTable = $tableDetails[$tableKey];

    require '../../connection.php';
    require '../../retrieveData.php';
    
    // Checking to display NOTE
    $note = '';
    $vimpnote = '';
    $noteHeading = '<h4 class="text-primary"><i class="far fa-clipboard"></i> Note</h4> <h5>';
    $blobNote = "<br>Max Upload Size is 2MB <br>
    Supported file types are JPEG, JPG, PNG, GIF, BMP <br>";
    $dateNote = "<br>For datetime<br>
    Use YYYY-MM-DD HH:MM:SS format if datetime picker is not supported<br>";
    $iconNote = "<br>For Icon<br>
    Copy unicode for an icon from <a target='_blank' href='https://fontawesome.com/'>fontawesome</a><br>";
    $priorityNote = "<br>For Priority<br>
    Priority of expired records are not considered<br>";

    //Notes for fields
    foreach ($selectedTable["fields"] as $key => $value) {
        switch ($value["type"]) {
        case 'BLOB':
            $note .= $noteHeading.$blobNote;
            $noteHeading = '';
            $blobNote = '';
            break;
        case 'DATETIME':
            $note .= $noteHeading.$dateNote;
            $noteHeading = '';
            $dateNote = '';
            break;
        case 'ICON':
            $note .= $noteHeading.$iconNote;
            $noteHeading = '';
            $iconNote = '';
            break;
        case 'PRIORITY':
            $note .= $noteHeading.$priorityNote;
            $noteHeading = '';
            $priorityNote = '';
            break;
        }
    }

    //Notes for tables
    switch ($selectedTable["tableName"]) {
    case 'ritwebsite_main_category':
    case 'ritwebsite_sub_category':
    case 'ritwebsite_quick_link':
        $vimpnote = "To give local redirect in links. Two options are available<br>
        <br> <u><strong> Option 1:- </strong> Redirect to a department page.</u><br>
        For this make sure the link is <q>/departments?dept=DEPARTMENT_NAME</q>
        <br> The DEPARTMENT_NAME should not have any special charachters 
        and should be the same as the category name in english<br>

        <br> <u><strong> Option 2:- </strong> Redirect to a fileUpload page.</u><br>
        For this make sure the link is <q>/fileUploads?fileUploadType=UPLOAD_CATEGORY</q>
        <br> The UPLOAD_CATEGORY should only be any one of the english titles given in 
        <a target='_blank' href='".findHost()."/Admin/tables?page=OfficeTables&table=ritwebsite_office_upload_types'>
        Office File Upload Caegory</a>";
        break;
    case 'ritwebsite_home_page_principal_message':
        $note .= "<br>Only the first record is used to show Principal Message<br>";
        break;
    case 'ritwebsite_staff_details':
        $note .= "<br>While adding staff details, <br>
        Seperate multiple records using <strong>##</strong><br>";
        break;
    }
    
    $note .= $noteHeading."
            <br>For Malayalam content<br>
            Use <a target='_blank' href='https://translate.google.co.in/'> Google translate </a> 
            and copy paste accordingily<br>
            <br> Required fields are marked with * <br><br></h5>";

    $cacheName = strtolower($selectedTable["page"]);
    require 'deleteCache.php';
    ?>
    <title>Administrator - Tables</title>
    <link href="../theme.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Tables';
        require '../sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require '../nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-start mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Insert New Record</h1>
                    </div>

                    <div class="d-sm-flex mb-4 align-items-baseline">
                        <a href="<?php echo $backLink; ?>" class="back-btn"> <i class="fas fa-arrow-circle-left"></i>
                        </a>
                        <p><?php echo $selectedTable["vTableName"]; ?></p>
                    </div>

                    <!-- Showing error message -->
                    <?php echo $errMsg; ?>

                    <!-- Shwoing VIMP Note -->
                    <?php
                    if ($vimpnote !== '') { ?>
                    <div class="container vimpNote p-4 mb-2">
                        <h5 style="color:red">Very Important</h5>
                        <?php echo $vimpnote; ?>
                    </div>
                    <?php
                    }
                    ?>

                    <!-- Showing note -->
                    <?php echo $note; ?>

                    <!-- Content Row -->
                    <div class="row">
                        <form method="POST" action="insertLogic.php" enctype="multipart/form-data">

                            <input type="hidden" name="tableKey" id="tableKey" value="<?php echo $tableKey; ?>">

                            <?php
                            for ($i=0; $i<count($selectedTable["fields"]); $i++) {
                                if ($selectedTable["fields"][$i]["type"] == "DEPARTMENT"
                                    && $_SESSION["userType"] !== "admin"
                                ) {
                                    continue;
                                }
                                $input = getInputField($selectedTable["fields"][$i], null, null, $selectedTable["tableName"]);
                                echo $input;
                            }
                            ?>

                            <button class="btn btn-success" type="submit">Add Record</button>
                            <button class="btn btn-danger" type="reset">Clear All</button>
                        </form>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; RIT 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Custom scripts for all pages-->
    <script src="../script.js"></script>
    <script>
        // For file upload
        function populateFileLink(fileId, urlId, hostUrl) {
            var file = document.getElementById(fileId).files[0];
            var fileName = file.name;
            if (file.size > 2000000) {
                alert('File size exceeded 2MB. The operation will fail');
            } else {
                document.getElementById(urlId).value = hostUrl + "/Admin/tables/uploads/" + Date.now() + fileName;
            }
        }

        // For image preview
        function showPreview(id) {
            var file = new FileReader();
            var f = document.getElementById(id).files[0];
            file.readAsDataURL(f);
            if (f.size > 2000000) {
                alert('File size exceeded 2MB. The operation will fail')
            } else {
                file.onload = function (event) {
                    document.getElementById(id + "Preview").src = event.target.result;
                };
            }
        }

        // For Datetime
        function populateDateTime(fieldId, isDate) {
            var datetimeField = document.getElementById(fieldId);
            var data;

            // Getting current date and time
            data = datetimeField.value.split("T");

            var type = isDate ? "date" : "time";
            var element = document.getElementById(fieldId + type);
            if (isDate) {
                datetimeField.value = element.value + "T" + data[1];
            } else {
                datetimeField.value = data[0] + "T" + element.value;
            }
            console.log(datetimeField.value);
        }

        // For Filtering type by deprtmnet
        function filterFunctionType(id) {
            var dept = document.getElementById(id).value;
            // Hardcoding id of function type since this is too specific
            var functionType = document.getElementById("function_type");
            var flag = true;
            for (let index = 0; index < functionType.options.length; index++) {
                const element = functionType.options[index];

                //Hiding others
                if (!element.innerText.includes(dept)) {
                    element.style.display = "none";
                } else {
                    if (flag) {
                        functionType.options.selectedIndex = index;
                        flag = false;
                    }
                    element.style.display = "block";
                }
            }
            if (flag) {
                functionType.options.selectedIndex = -1;
            }
        }
    </script>

</body>

</html>