<?php
    /**
     * Logic to update record
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin-Tables
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/tables
     */
session_start();
$_SESSION['tableKey'] = $_POST['tableKey'];
$_SESSION['recordId'] = $_POST['recordId'];

//Checking if POST was made
require '../../findUrl.php';
require 'tableDetail.php';
require '../../connection.php';
require '../../retrieveData.php';
if (isset($_POST['tableKey'], $_POST['recordId'])) {
    $tableKey = $_POST['tableKey'];
    $selectedTable = $tableDetails[$tableKey];
    $recordId = $_POST['recordId'];
} else {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
    exit;
}

//Adding common fields
$finalResult = '';
for ($i=0; $i<count($selectedTable["fields"]); $i++) {
    $currentField = $selectedTable["fields"][$i];

    $tempResult = '';
    if ($currentField["type"] == 'BLOB') {
        if ($_FILES[$currentField["name"]]['name'] == null) {
            continue;
        } else {
            $allowed = ['jpeg','jpg','png','gif','bmp'];
            $extension = pathinfo($_FILES[$currentField["name"]]['name'], PATHINFO_EXTENSION);
            if (in_array($extension, $allowed)) {
                if ($_FILES['image']['error'] !== 0) {
                    header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1.5');
                    exit;
                }
                $img = base64_encode(file_get_contents($_FILES[$currentField["name"]]['tmp_name']));
                $type = getimagesize($_FILES[$currentField["name"]]['tmp_name'])["mime"];
                $tempResult = $currentField["name"].' = "'.$img.'"';
            } else {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1');
                exit;
            }
        }
    } elseif ($currentField["type"] == 'ENUM') {
        if (!isset($type)) {
            continue;
        }
        $tempResult = $currentField["name"].' = "'.$type.'"';
        $type = null;
    } elseif ($currentField["type"] == "UPLOAD") {
        //If file was not uploaded
        if ($_FILES[$currentField["name"]]['name'] == null) {
            //Redirect back if file is required
            if ($currentField["required"] == "required") {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e1');
                exit;
            } else {
                //Continue adding other fields if file is not required
                continue;
            }
        } else {
            //If file was uploaded
            $autoGenPath = $_POST[$currentField["values"]];
            $pathFrags = explode("/", $autoGenPath);
            $fileName = end($pathFrags);
            $targetFile = getcwd().DIRECTORY_SEPARATOR."uploads/".$fileName;
            if (move_uploaded_file($_FILES[$currentField["name"]]['tmp_name'], $targetFile)) {
                continue;
            } else {
                echo "Could not upload file";
                $errFlag = 1;
                break;
            }
        }
    } elseif ($currentField["type"] == "PRIORITY") {
        $inputPriority = $_POST["priority"];
        $currentPriority = retrieveData("SELECT priority FROM ".$selectedTable["tableName"]." WHERE id=".$recordId, $con)[0]["priority"];
        // No change in priority
        if ($currentPriority === $inputPriority) {
            $params .= $inputPriority.',';
            $fields .= $currentField["name"].",";
        } else {
            // Update priority of other fields
            if ($inputPriority > $currentPriority) {
                $currentPriority += 1;
                $query = "UPDATE ".$selectedTable["tableName"]." SET priority = priority-1 WHERE priority BETWEEN ".$currentPriority." AND ".$inputPriority;
            } else {
                $currentPriority -= 1;
                $query = "UPDATE ".$selectedTable["tableName"]." SET priority = priority+1 WHERE priority BETWEEN ".$inputPriority." AND ".$currentPriority;
            }
            if (mysqli_query($con, $query)) {
                $params .= $inputPriority.',';
                $fields .= $currentField["name"].",";
            } else {
                header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e4');
                exit;
            }
        }
        $tempResult = $currentField["name"].' = '.$inputPriority;
    } elseif ($currentField["type"] == "DEPARTMENT") {
        if ($_SESSION["userType"] === "admin") {
            $data = $_POST["department"];
        } else {
            $data = $_SESSION["department"];
        }
        $tempResult = $currentField["name"].' = "'.$data.'"';
    } else {
        $data = addslashes($_POST[$currentField["name"]]);
        $tempResult = $currentField["name"].' = "'.$data.'"';
    }

    $finalResult .= $tempResult.',';
}
//End of common fields

$finalResult = rtrim($finalResult, ',');

// Executing query
if ($errFlag !== 1) {
    $sql = 'UPDATE '.$selectedTable["tableName"].' SET '.$finalResult.' WHERE id='.$recordId;
    mysqli_query($con, $sql);
    $pageId = $selectedTable["page"];
    if (mysqli_affected_rows($con) > 0) {
        $serverHome = findHost();
        header('Location: '.$serverHome.'/Admin/tables/?page='.$pageId.'&table='.$tableKey);
        exit;
    } else {
        if (mysqli_error($con) == null) {
            header('Location: '.$_SERVER["HTTP_REFERER"].'?err=e2');
            exit;
        }
        $errFlag = 1;
    }
}

if ($errFlag == 1) {?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Updating Record</title>
</head>

<body id="bodyId">
    <h1>Error</h1>
    <p> <?php echo mysqli_error($con); ?> </p>
    <p> <?php print_r(error_get_last()); ?> </p>
    <a href="<?php echo findHost().'/Admin/tables/?page='.$pageId.'&table='.$tableKey; ?>">Go Back</a>
</body>

</html>
    <?php
}
?>