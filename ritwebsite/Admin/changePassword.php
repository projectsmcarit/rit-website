<?php
    /**
     * Logic to change password of user
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | Change Password</title>
</head>
<body>

<?php
if (isset($_POST)) {
    include '../connection.php';
    $newPass = $_POST['pswd'];
    $sql = "UPDATE ritwebsite_logintb SET password='".$newPass."' WHERE id=".$_SESSION['id'];
    mysqli_query($con, $sql);

    if (mysqli_affected_rows($con) > 0) {
        $msgHead = "Success";
        $msgPass = "Successfully changed your password";
    } else {
        if (mysqli_error($con) === null) {
            $msgHead = "Error";
            $msgPass = "This is already your current password";
        }
    }
} else {
    $msgHead = "Error";
    $msgPass = "There was some error while updating password";
}
?>

    <h1> <?php echo $msgHead ?> </h1>
    <h2> <?php echo $msgPass; ?> </h2>
    <h5> <?php echo mysqli_error($con); ?> </h5>
    <h5> <?php print_r(error_get_last()); ?> </h5>
    <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        Go Back
    </a>

</body>
</html>