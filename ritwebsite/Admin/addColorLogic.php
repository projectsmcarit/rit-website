<?php
    /**
     * Logic to add new color set
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */

session_start();
require '../findUrl.php';
require 'colorFilePath.php';

$path = $globalColorJSFilePath;
$file = fopen($path, 'r');
$exisitingData = fread($file, filesize($path));
fclose($file);

require "{$globalColorPHPFilePath}";
$newColor = "\n\n\t{";
for ($i=0; $i<count($colorTypes); $i++) {
    $newColor .= "\n\t\t'".$cssVariableName[$i]."': '".$_POST[$colorTypes[$i]]."',";
}
$newColor .= "\n\t}\n];";
$data = substr($exisitingData, 0, strripos($exisitingData, "}"))."},".$newColor;

$file = fopen($path, 'w');
fwrite($file, $data);
fclose($file);

header("Location: ".findHost()."/Admin");
?>