<?php
    /**
     * Homepage of admin
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
        require '../findUrl.php';
        require 'header.php';
        require 'colorFilePath.php';

        //Getting users
        require '../connection.php';
        require '../retrieveData.php';
        $usr = retrieveData("SELECT count(id) FROM ritwebsite_logintb", $con);
        $imgs= retrieveData("SELECT count(id) FROM ritwebsite_gallery", $con);
        $events=retrieveData("SELECT count(id) FROM ritwebsite_home_page_events", $con);

        //Getting colors
        $colorFile = $globalColorJSFilePath;
        $file = fopen($colorFile, 'r');
        $colorSets = fread($file, filesize($colorFile));
        $colorSets = explode("[", $colorSets)[1];
        $colorSets = str_replace("];", "", $colorSets);
        $colorSets = explode("}", $colorSets);
        fclose($file);
    ?>
    <title>Administrator - Dashboard</title>
    <link href="theme.css" rel="stylesheet">
    <style>
        .confirm {
            display: none;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Dashboard';
        require 'sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require 'nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-start mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">
                        <!-- Total Users -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold 
                                            text-success text-uppercase mb-1">
                                                Total users</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <?php echo $usr[0]["count(id)"] - 1; ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <!-- Total images in gallery -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold 
                                            text-success text-uppercase mb-1">
                                                Total images in gallery</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <?php echo $imgs[0]["count(id)"] ; ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-image"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <!-- Total events -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold 
                                            text-success text-uppercase mb-1">
                                                Total events</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <?php echo $events[0]["count(id)"] ; ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar-day"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Color Sets</h1>
                            <form action="addColor.php" method="post">
                                <button type="submit" class="btn btn-primary">
                                    Add New Color Set</button>
                            </form>
                        </div>

                        <table class="table table-responsive">
                            <thead class="thead-dark">
                                <?php
                                require "{$globalColorPHPFilePath}";
                                for ($i=0; $i<count($colorTypes); $i++) {
                                    echo '<th>'.$colorTypes[$i].'</th>';
                                }
                                ?>
                                <th>Actions</th>
                            </thead>

                            <tbody>
                                <?php
                                
                                for ($i=0; $i<count($colorSets)-1; $i++) { ?>
                                <tr>
                                    <?php
                                    $set = explode(":", $colorSets[$i]);
                                    for ($j=0; $j<count($set); $j++) {
                                        $value = explode(",", $set[$j]);
                                        
                                        if (count($value) <= 2) {
                                            if (strpos($value[0], "--") !== false || $value[0]=="") {
                                                continue;
                                            } else {
                                                $col = str_replace("'", "", $value[0]);
                                                echo '<td style="color:#000">'
                                                .$col.
                                                '<div class="colorBall" 
                                                style="height:10px;width:50px;
                                                background-color:'.$col.'">
                                                </div>
                                                </td>';
                                            }
                                        } else {
                                            $col = $value[0]
                                            .",".$value[1]
                                            .",".$value[2]
                                            .",".$value[3];
                                            
                                            $col = str_replace("'", "", $col);
                                            echo '<td style="color:#000">'
                                            .$col.'<div class="colorBall" 
                                            style="height:10px;width:50px;
                                            background-color:'.$col.'">
                                            </div>
                                            </td>';
                                        }
                                    }
                                    ?>

                                    <td>
                                        <?php
                                        if (count($colorSets) === 2) {?>
                                        Cannot delete <br>
                                        Need to have atleast one color set
                                            <?php
                                        } else {
                                            ?>
                                        <form action="deleteColor.php" method="post">
                                            <input type="hidden" name="colorIndex" 
                                            value="<?php echo $i; ?>">
                                            <a href="javascript:showConfirm('confirmBox<?php echo $i; ?>')"
                                                class="btn btn-outline-danger btn-block">
                                                <i class="fas fa-trash-alt"></i> Delete
                                            </a>

                                            <div id="confirmBox<?php echo $i; ?>" class="confirm">
                                                <p> 
                                                    <i class="fas fa-exclamation-circle"></i>
                                                    Are you sure you want to delete this record?
                                                </p>
                                                <button class="btn btn-danger btn-block" type="submit">Yes</button>
                                                <a href="javascript:hideConfirm('confirmBox<?php echo $i; ?>')"
                                                    class="btn btn-outline-danger btn-block">
                                                    No
                                                </a>
                                        </form>
                                    </td>
                                        <?php }
                                        ?>
                                </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; RIT 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Custom scripts for all pages-->
        <script src="script.js"></script>
        <script>
            function showConfirm(name) {
                document.getElementById(name).style.display = "block";
            }

            function hideConfirm(name) {
                document.getElementById(name).style.display = "none";
            }
        </script>

</body>

</html>