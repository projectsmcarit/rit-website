<?php
    /**
     * Webpage to update user
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin-Users
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/users
     */
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require '../../findUrl.php';
    require '../header.php';
    require '../tables/tableDetail.php';
    require '../../connection.php';
    require '../../retrieveData.php';

    if (isset($_POST['id'])) {
        $selectedId = $_POST['id'];
    } else {
        $selectedId = $_SESSION['userIdForUpdate'];
    }

    $selectedUserData = retrieveData("SELECT * FROM ritwebsite_logintb WHERE id=".$selectedId, $con);

    $errMsg = '';
    if (isset($_GET['err'])) {
        if ($_GET['err'] === "e1") {
            $message = 'The fields did not have any changes';
        } else {
            $message = "Error while updating data";
        }
        $errMsg = '
                <div class="d-sm-flex mb-4 align-items-baseline" style="color:red">
                    <h5><i class="fas fa-bug"></i>'.$message.'</h5>
                </div>';
    }


    //Settting User Types
    require 'typesOfUsers.php';
    $showType = '';
    $options = '';
    for ($i=0; $i<count($typeOfUsers); $i++) {
        if ($typeOfUsers[$i] == $selectedUserData[0]["userType"]) {
            $sel = "selected";
        } else {
            $sel = '';
        }
        $options .= '<option value="'.$typeOfUsers[$i].'" '.$sel.' >'. $typeOfUsers[$i] .'</option>';
    }
    $showType = '<div class="form-group row">
        <label for="userType" class="col-sm-2 col-form-label"> Type Of User </label>
            <div class="col-sm-10">
                <select class="form-control" name="userType" id="userType" required>
                    '.$options.'
                </select>
            </div>
    </div>';
    
    

    //Setting Departments
    $showMain = '';
    $categories = retrieveData(
        "SELECT * FROM ritwebsite_sub_category 
        WHERE main_id = (
            SELECT id FROM ritwebsite_main_category 
            WHERE name_en= 'Departments')",
        $con
    );
    $options = '';
    for ($i=0; $i<count($categories); $i++) {
        if ($categories[$i]["name_en"] == $selectedUserData[0]["department"]) {
            $sel = "selected";
        } else {
            $sel = '';
        }
        $options .= '<option value="'.$categories[$i]["name_en"].'" '.$sel.'>'. $categories[$i]["name_en"] .'</option>';
    }
    $showMain = '<div class="form-group row">
        <label for="dept" class="col-sm-2 col-form-label"> Department Of User </label>
            <div class="col-sm-10">
                <select class="form-control" name="department" id="department" required>
                    '.$options.'
                </select>
            </div>
    </div>';
    
    ?>
    <title>Administrator - Dashboard</title>
    <link href="../theme.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">

    <script>
        function validateForm() {
            // Collect form data in JavaScript variables
            var pw1 = document.getElementById("password").value;
            var pw2 = document.getElementById("Confirm Password").value;

            var submitBtn = document.getElementById("addUserBtn");
            var errMsg = document.getElementById("pswdMsg");

            if (pw2 == "") {
                errMsg.innerHTML = "Confirm the password please!";
                submitBtn.disabled = true;
            } else if (pw1 != pw2) {
                errMsg.innerHTML = "Passwords does not match";
                submitBtn.disabled = true;
            } else {
                errMsg.innerHTML = "";
                submitBtn.disabled = false;
            }
        }
    </script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Users';
        require '../sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require '../nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-start mb-4">
                        <h1 class="h3 mb-0 text-gray-800">
                            <a href="<?php echo findHost().'/Admin/users' ?>" class="back-btn"> <i
                                    class="fas fa-arrow-circle-left"></i>
                            </a>
                            Update User
                        </h1>
                    </div>
                
                    <?php echo $errMsg;?>

                    <form action="updateUserLogic.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $selectedId; ?>">

                        <!-- Basic User details -->
                        <hr>
                        <div class="d-sm-flex align-items-center justify-content-start mb-4">
                            <h5 class="h5 mb-0 text-gray-800">Basic details</h5>
                        </div>

                        <!-- Name -->
                        <div class="form-group row">
                            <label for="username" class="col-sm-2 col-form-label">
                                Name of user
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="username" name="username"
                                    placeholder="Username" maxlength="50"
                                    value="<?php echo $selectedUserData[0]["username"]; ?>" required>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">
                                Password for user
                            </label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="password" name="password"
                                    placeholder="Password" maxlength="50"
                                    value="<?php echo $selectedUserData[0]["password"]; ?>" required>
                            </div>
                        </div>

                        <!-- Cofirm Password -->
                        <h6 id="pswdMsg" ></h6>
                        <div class="form-group row">
                            <label for="Confirm Password" class="col-sm-2 col-form-label">
                                Confirm Password for user
                            </label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="Confirm Password"
                                    name="Confirm Password" placeholder="Confirm Password" maxlength="50"
                                    onkeyup="validateForm()" required>
                            </div>
                        </div>

                        <?php
                        echo $showType;
                        echo $showMain;
                        ?>

                        <button type="submit" id="addUserBtn"
                         class="btn btn-success" disabled>Update user</button>
                    </form>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; RIT 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Custom scripts for all pages-->
        <script src="group.js"></script>
        <script src="../script.js"></script>
        <script>
            function showConfirm() {
                document.getElementById("confirmBox").style.display = "block";
            }

            function hideConfirm() {
                document.getElementById("confirmBox").style.display = "none";
            }
        </script>

</body>

</html>