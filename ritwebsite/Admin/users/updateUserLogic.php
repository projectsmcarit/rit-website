<?php
    /**
     * Logic to update user
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin-Users
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/users
     */
session_start();
require '../../connection.php';
require '../../findUrl.php';
if (isset($_POST)) {
    $_SESSION['userIdForUpdate'] = $_POST['id'];
    $finalQuery = '';
    foreach ($_POST as $key => $value) {
        switch ($key) {
        case 'id':
            $recordId = $value;
            break;

        case 'username':
            $finalQuery .= 'username = "'.$value.'" ,';
            break;

        case 'password':
            $finalQuery .= 'password = "'.$value.'" ,';
            break;
        
        case 'userType':
            $finalQuery .= 'userType = "'.$value.'" ,';
            break;
        
        case 'department':
            $finalQuery .= 'department = "'.$value.'" ,';
            break;
        }
    }
    $finalQuery = substr($finalQuery, 0, -1);
    $query = 'UPDATE ritwebsite_logintb SET '.$finalQuery.' WHERE id='.$recordId;
    mysqli_query($con, $query);
    if (mysqli_affected_rows($con) > 0) {
        header('Location: '.findHost().'/Admin/users/');
        exit;
    } else {
        if (mysqli_error($con) == null) {
            header('Location: '.findHost().'/Admin/users/updateUser.php?err=e1');
            exit;
        }
        $errFlag = 1;
    }
} else {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
    exit;
}

if ($errFlag == 1) {?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Updating user</title>    
</head>

<body id="bodyId">

        <h1>Error</h1>
        <p> <?php echo mysqli_error($con); ?> </p>
        <p> <?php print_r(error_get_last()); ?> </p>
        <a href="<?php echo findHost().'/Admin/users';?>" >
            Go Back
        </a>


</body>

</html>
    <?php
}
?>