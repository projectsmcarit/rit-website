<?php
    /**
     * Logic to add user
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin-Users
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/users
     */

if (isset($_POST)) {
    foreach ($_POST as $key => $value) {
        switch ($key) {
        case 'username':
            $name = $value;
            break;
        case 'password':
            $pass = $value;
            break;
        case 'userType':
            $userType = $value;
            break;
        case 'dept':
            $dept = $value;
            break;
        }
    }

    include '../../connection.php';
    include '../../findUrl.php';
    $query = 'INSERT INTO ritwebsite_logintb (username,password,userType,department)
     VALUES ("'.$name.'" , "'.$pass.'" , "'.$userType.'" , "'.$dept.'")';
    if (mysqli_query($con, $query)) {
        $serverHome = findHost();
        header('Location: '.$serverHome.'/Admin/users/');
        exit;
    } else {
        $errFlag = 1;
    }
} else {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
    exit;
}

if ($errFlag == 1) {?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adding new user</title>
</head>

<body id="bodyId">

    <h1>Error</h1>
    <p> <?php echo mysqli_error($con); ?> </p>
    <p> <?php echo error_get_last(); ?> </p>
    <a 
     href="<?php
        echo findHost().'/Admin/users'; 
        ?>">
        Go Back 
    </a>

</body>

</html>

    <?php
}
?>