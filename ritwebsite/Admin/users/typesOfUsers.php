<?php
    /**
     * Utility to classify users
     * 
     * To add a new type of user
     * We can add it here and it will show up
     * However to fine tune permission of the user
     * We will have to go to the index page of tables
     *
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Admin-Users
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/users
     */
$typeOfUsers = [
    "coordinator",
    "representative"
];
?>