<?php
    /**
     * Webpage for CRUD Users
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Admin-Users
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin/users
     */
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require '../../findUrl.php';
    require '../header.php';
    require '../../connection.php';
    require '../../retrieveData.php';
    require '../tables/tableDetail.php';
    $myId = $_SESSION["id"];

    $users = retrieveData("SELECT * FROM ritwebsite_logintb", $con);
    $usrCount = count($users);
    ?>
    <title>Administrator - Dashboard</title>
    <link href="../theme.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        $activeSideBar = 'Users';
        require '../sideBar.php';
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php require '../nav.php'; ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Users</h1>
                        <form action="addUser.php" method="POST">
                            <button type="submit" 
                            class="btn btn-primary">
                                Add New User <i class="fas fa-user-plus"></i>
                            </button>
                        </form>
                    </div>

                    <!-- Content Row -->
                    <div class="col">

                        <?php
                        for ($i=0; $i<$usrCount; $i++) {
                            if ($users[$i]["userType"] == 'admin') {
                                continue;
                            }
                            $id = $users[$i]["id"];
                            $name = $users[$i]["username"];
                            $profile = ucwords($name[0]);
                            $department = '';
                            if ($users[$i]["department"] != null) {
                                $department = '<h6>Department:- '. $users[$i]["department"].'</h6>';
                            } ?>

                        <div class="row profile-card m-4">
                            <div class="col">
                                <div class="row">

                                    <!-- User Name -->
                                    <div class="col-md-8 col-sm-4 p-2 d-flex align-items-end">
                                        <div class="rounded-circle 
                                        test d-flex align-items-center 
                                        justify-content-center avatarImg me-1">
                                            <?php echo $profile; ?>
                                        </div>
                                        <h4><?php echo $name; ?></h4>
                                    </div>

                                    <!-- Update Delete -->
                                    <div class="col-md-4 col-sm-8 p-2 d-flex align-self-end">

                                        <form action="updateUser.php" method="post" class="p-1">
                                            <input type="hidden" name="id" id="id"
                                             value="<?php echo $id; ?>">
                                            <button class="btn btn-success btn-block" type="submit">
                                                <i class="fas fa-pen-square"></i> Update
                                            </button>
                                        </form>

                                        <form action="deleteLogic.php" method="post" class="p-1">
                                            <input type="hidden" name="id" id="id"
                                             value="<?php echo $id; ?>">
                                            <a href="javascript:showConfirm('confirmBox<?php echo $id; ?>')"
                                                class="btn btn-danger btn-block">
                                                <i class="fas fa-trash-alt"></i> Delete
                                            </a>

                                            <div id="confirmBox<?php echo $id; ?>" class="confirmBox">
                                                <p>
                                                    <i class="fas fa-exclamation-circle"></i>
                                                    Are you sure you want to delete this record?
                                                </p>
                                                <button class="btn btn-danger btn-block" 
                                                type="submit">
                                                    Yes
                                                </button>
                                                <a href="javascript:hideConfirm('confirmBox<?php echo $id; ?>')"
                                                    class="btn btn-info btn-block">
                                                    No
                                                </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <!-- Permissions -->
                                <div class="p-2">
                                    <?php echo $department; ?>
                                </div>
                            </div>
                        </div>
                        <hr>

                            <?php
                        }
                        ?>



                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; RIT 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Custom scripts for all pages-->
    <script src="../script.js"></script>
    <script>
        function showConfirm(name) {
            document.getElementById(name).style.display = "block";
        }

        function hideConfirm(name) {
            document.getElementById(name).style.display = "none";
        }
    </script>

</body>

</html>