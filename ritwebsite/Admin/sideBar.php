<?php
    /**
     * Utility to show sidebar
     *
     * PHP version 5.4.3
     *
     * @category Sidebar
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */

if ($_SESSION["userType"] == "admin") {?>

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary 
    sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a target="_blank" rel="noopener noreferrer" class="sidebar-brand d-flex 
        align-items-center justify-content-center" 
        href="<?php echo findHost(); ?>">
            <div class="sidebar-brand-text mx-3">
                RIT
            </div>
        </a>

        <?php if ($activeSideBar == "Dashboard") {
            $active = 'active';
        } else {
            $active = '';
        } ?>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item <?php echo $active; ?>">
            <a class="nav-link" href="<?php echo findHost(); ?>/Admin/index.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <?php if ($activeSideBar == "Users") {
            $active = 'active';
        } else {
            $active = '';
        } ?>
        <!-- Nav Item - Users -->
        <li class="nav-item <?php echo $active; ?>">
            <a class="nav-link" 
            href="<?php echo findHost(); ?>/Admin/users/index.php">
                <i class="fas fa-users"></i>
                <span>Users</span></a>
        </li>

        <?php if ($activeSideBar == "Tables") {
            $active = 'active';
        } else {
            $active = '';
        } ?>
        <!-- Nav Item - Tables -->
        <li class="nav-item <?php echo $active; ?>">
            <a class="nav-link" 
            href="<?php echo findHost(); ?>/Admin/tables/index.php">
                <i class="fas fa-table"></i>
                <span>Tables</span></a>
        </li>

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->
    <?php 
} ?>