<?php
    /**
     * Header used in all Admin pages
     *
     * PHP version 5.4.3
     * 
     * This file is dependent on findUrl.php
     * Make sure to import that file first
     *
     * @category Header
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
$serverHome = findHost();
if (!isset($_SESSION['username'], $_SESSION['id'], $_SESSION['userType'])) {
    echo "
        <h1>Error</h1>
        Un-Authorized access. Please Login to continue
        <a href='".$serverHome."/login.php'> Login </a>";
    exit;
}
if ($_SESSION["userType"] !== "admin") {
    // Checking if URL has tables
    $uriParts = explode("/", $_SERVER["REQUEST_URI"]);
    $flag = 0;
    foreach ($uriParts as $key => $value) {
        if ($value === "tables") {
            $flag = 1;
            break;
        }
    }
    if ($flag === 0) {
        echo "
        <h1>Error</h1>
        You dont have access to this page.
        <a href='javascript:window.history.back()'> Go Back </a>";
        exit;
    }
}
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $serverHome; ?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $serverHome; ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $serverHome; ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo $serverHome; ?>/images/favicon/site.webmanifest">

<!-- jQuery -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
    integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>

<!-- Bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
</script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css"
    integrity="sha384-wESLQ85D6gbsF459vf1CiZ2+rr+CsxRY0RpiF1tLlQpDnAgg6rwdsUF1+Ics2bni" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/c076c3c3aa.js" crossorigin="anonymous"></script>

<!-- Custom font -->
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Tiny MCE WYSIWIG -->
<script src="https://cdn.tiny.cloud/1/8jniwtmoax4sp4zt41p4x3my83me5yw9qslp0cqyz7waksmj/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>