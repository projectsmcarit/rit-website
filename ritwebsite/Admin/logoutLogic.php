<?php
    /**
     * Logic to logout user
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
session_start();
session_destroy();
require '../findUrl.php';
$serverHome = findHost();
header('Location: '.$serverHome.'/login.php');
?>