<?php
    /**
     * Utility to store path info of color files
     * 
     * This helps to change path of the color files
     * in admin panel from a single point
     *
     * PHP version 5.4.3
     *
     * @category Logic
     * @package  Admin
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/admin
     */
    $globalColorJSFilePath = "../theme/colors.js";
    $globalColorPHPFilePath = "../theme/colors.php";
?>