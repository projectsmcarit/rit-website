<?php
    /**
     * Webpage showing course details
     * 
     * This webpage shows data for UG And PG
     * We use the getParameter "?type" to know which
     * type was triggered.
     *
     * PHP version 5.4.3
     *
     * @category Webpage
     * @package  Academics
     * @author   Original Author <ashlymohan174@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/Academics?type=GraduateType
     */
?>
<!DOCTYPE html>
<html>

<head>
    <?php
        require '../findUrl.php';
        require '../header.php';
        require '../locale.php';
        require '../connection.php';
        require '../retrieveData.php';
    if (isset($_GET["type"])) {
        $gradType = $_GET["type"];
    } else {
        $gradType = "Under Graduate Programmes";
    }

    // cached_academic_Under Graduate Programmes_en.html
    $cachefile = "../cache/cached_academic_".$gradType.$lang.".html";
    require '../cache/retrieveCache.php';

    $Academic = retrieveData(
        'SELECT * FROM ritwebsite_academic_detail 
        WHERE title_en = "'.$gradType.'" ORDER BY priority', $con
    );
    $Course = retrieveData(
        'SELECT * FROM  ritwebsite_course_detail 
        WHERE graduate_type = "'.$gradType.'"', $con
    );
    ?>

    <title>Rajiv Gandhi Institute Of Technology | Courses</title>
    <link href="style.css" rel="stylesheet">
</head>

<body onload="setTheme()">

    <body>

        <?php
            $activePageName = "Academic";
            require '../nav.php';
            $title = $Academic[0]["title".$lang];
            $body = $Academic[0]["body".$lang];
            $type = $Academic[0]["type"];
            $image = $Academic[0]["image"];
            $AcademicImage = 'data:' .$type.' ;base64,'.$image;
            
        ?>

        <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex flex-column align-items-center
         justify-content-center text-center"
            style="background: url('<?php echo $AcademicImage; ?>');">

            <h1><?php echo $title; ?></h1>
            <a href="#heading" class="btn-get-started scrollto">
                <i class="fas fa-angle-double-down"></i>
            </a>

        </section>

        <!-- ======= Services Section ======= -->
        <section id="heading" class="services section-bg">
            <div class="container">

                <div class="section-title">
                    <h2><?php echo $languages[$lang]["CoursesOffered"] ?></h2>
                    <p><?php echo $body; ?></p>
                </div>

            </div>

            </div>
        </section><!-- End Services Section -->

        <main class="container">

            <?php
            for ($i=0;$i<count($Course);$i++) {
                $courseLinkId = $Course[$i]["title_en"];
                $courseTitle = $Course[$i]["title".$lang];
                $courseImageType = $Course[$i]["type"];
                $courseImage = 'data:' .$courseImageType.
                ' ;base64,'.$Course[$i]["image"];
                $courseBody = $Course[$i]["body".$lang];
                $courseIntake = $Course[$i]["intake"];
                $courseLink =  $Course[$i]["department"];
                ?>

            <article id="<?php echo $courseLinkId; ?>" class="postcard red">

                <a class="postcard__img_link" href="#">
                    <img class="postcard__img" 
                    src="<?php echo $courseImage; ?>" alt="Image Title" />
                </a>

                <div class="postcard__text">
                    <h1 class="postcard__title blue">
                    <?php echo $courseTitle; ?></a></h1>
                    <div class="postcard__bar"></div>

                    <div class="postcard__preview-txt">
                    <?php echo $courseBody; ?> </div>
                    <ul class="postcard__tagbox">
                        <li class="tag__item">
                            <i class="fas fa-tag mr-2"></i>
                            <?php echo $courseIntake; ?> Intakes
                        </li>
                        <li class="tag__item">
                            <a href="<?php echo findhost()."/departments?lang=".$lang."&dept=".$courseLink; ?>" 
                            target="_blank" rel="noopener noreferrer">
                                <i class="fas fa-external-link-alt"></i>
                                Goto Department
                            </a>
                        </li>
                    </ul>
                </div>
            </article>

                <?php
            } ?>

        </main>

        <?php 
            require '../footer.php';
            require '../cache/createCache.php';
        ?>

    </body>

</html>
