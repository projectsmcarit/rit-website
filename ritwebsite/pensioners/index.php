<?php
  /**
   * Webpage showing pensioners details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Facilities
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Facilities
   */
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  
     Require '../findUrl.php';
     require '../header.php';
     require '../connection.php';
     require '../retrieveData.php';
     require '../locale.php';
    $pensionDetails = retrieveData("SELECT * FROM ritwebsite_pensioners_details", $con);
    ?>
  <title>Rajiv Gandhi Institute of Technology | PENSIONERS</title>
  <link href="style.css" rel="stylesheet">

</head>

<body onload="setTheme()">

  <?php
    $activePageName = "Pensioners";
    require '../nav.php';
    ?>

  <div class="container py-5">
    <div class="row py-5">
      <div class="col-lg-10 mx-auto">
        <div class="card rounded shadow border-0">
          <div class="card-body p-5 bg-white rounded">

            <!-- <div class="col-md-4 offset-md-4 mt-5  pt-3" >
               <div class="input-group mb-3"  >
                 <input type="text" id="myInput"  class="form-control" placeholder="Search for names..">
                 <div class="input-group-append" style="float:right;">
                 <span class="input-group-text" ><i class="fa fa-search" ></i></span>
                 </div>
               </div> 
            </div> -->

            <div class="table-responsive">
              <table id="example" style="width:100%" class="table table-striped table-bordered">
                <thead>
                  <tr class="header">
                    <th><?php echo $languages[$lang]["Sl.No"]; ?></th>
                    <th><?php echo $languages[$lang]["Image"]; ?></th>
                    <th><?php echo $languages[$lang]["name"]; ?></th>
                    <th><?php echo $languages[$lang]["Department"]; ?></th>
                    <th><?php echo $languages[$lang]["Designation"]; ?></th>
                    <th><?php echo $languages[$lang]["email"]; ?></th>
                    <th><?php echo $languages[$lang]["phone"]; ?></th>
                    <th><?php echo $languages[$lang]["retiredate"]; ?></th>
                    <th><?php echo $languages[$lang]["contactdetails"]; ?></th>
                    <th><?php echo $languages[$lang]["citation"]; ?></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    $sno=1;
                    for ($i=0;$i<count($pensionDetails);$i++) {
                        $pensionName = $pensionDetails[$i]["name"];
                        $pensionDepartment = $pensionDetails[$i]["department"];
                        $pensionDesignation = $pensionDetails[$i]["designation"];
                        $pensionemail = $pensionDetails[$i]["emailid"];
                        $pensionPhone = $pensionDetails[$i]["phoneno"];
                        $pensionRetire = $pensionDetails[$i]["retire_date"];
                        $pensionImageType = $pensionDetails[$i]["type"];
                        $pensionImage = 'data:' .$pensionImageType.' ;base64,'.$pensionDetails[$i]["image"];
                        $pensionContact = $pensionDetails[$i]["contact_detail"];
                        $plink=$pensionDetails[$i]["link"];
                        ?>
                  <tr>
                    <td><?php echo $sno++; ?></td>
                    <td><img src="<?php echo $pensionImage; ?>" width="100" height="100"></td>

                    <td><?php echo $pensionName; ?></td>
                    <td><?php echo $pensionDepartment; ?></td>

                    <td><?php echo $pensionDesignation; ?></td>
                    <td><?php echo $pensionemail; ?></td>
                    <td><?php echo $pensionPhone; ?></td>
                    <td><?php echo $pensionRetire; ?></td>
                    <td><?php echo $pensionContact; ?></td>
                    <td>
                                    <a href='<?php echo $plink; ?>'>
                                        <?php echo $languages[$lang]["show"] ?>
                                    </a>
                                </td>
                  </tr>
                        <?php
                    }
                            
                    ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  

  <?php require '../footer.php'; ?>
  
</body>

</html>