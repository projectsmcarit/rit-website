<?php
  /**
   * Utility to display Navigation bar
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Nav
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?><?php
$serverHome = findHost();
$actual_link = findActualLink();

if (isset($_GET["lang"])) {
    $lang = $_GET["lang"];
} else {
    $lang = "_en";
}

if ($lang == "_en") {
    $engButton = "active-btn";
    $malButton = "";
} else {
    $engButton = "";
    $malButton = " active-btn";
}

$checkGet = strpos($_SERVER["REQUEST_URI"], '&');
$checkGet2 = strpos($_SERVER["REQUEST_URI"], '?');
$getParameters = '';
if ($checkGet !== false) {
    $getParameters = substr($_SERVER["REQUEST_URI"], $checkGet);
} else if ($checkGet2 !== false) {
    if (!strpos($_SERVER["REQUEST_URI"], "?lang")) {
        $getParameters = "&".substr($_SERVER["REQUEST_URI"], $checkGet2+1);
    }
} else {
    $getParameters = '';
}

$categoryDetails = retrieveData(
    '
        SELECT  
        main.id,
        main.name_en AS main_name_en, 
        main.name_mal AS main_name_mal,
        main.main_link,
        sub.name_en AS sub_name_en, 
        sub.name_mal AS sub_name_mal,
        sub.sub_link
        FROM ritwebsite_main_category AS main LEFT JOIN ritwebsite_sub_category AS sub
        ON main.id = sub.main_id
        ORDER BY main.priority ASC , sub.name_en ASC', $con
);
$categoryCount = count($categoryDetails);

$i = 0;
$body = '';
while ($i < $categoryCount) {
    if ($activePageName == $categoryDetails[$i]["main_name_en"]) {
        $status = 'active';
    } else {
        $status = '';
    }
    
    if ($categoryDetails[$i]["sub_name_en"] != null) {
        $currentId = $categoryDetails[$i]["id"];
        //If there are sub cat then start group and loop through

        $body .= '
        <li class="nav-item dropdown '.$status.'">
        <a class="nav-link dropdown-toggle" href="#" id="'. $categoryDetails[$i]["main_name_en"] .'" role="button"
        data-bs-toggle="dropdown" aria-expanded="false">'
        .$categoryDetails[$i]["main_name".$lang].
        '</a>
        <ul class="dropdown-menu" aria-labelledby="'.$categoryDetails[$i]["main_name_en"].'">';
        
        while ($i < $categoryCount) {
            if ($categoryDetails[$i]["id"] == $currentId) {
                //Add them to group
                $link = $categoryDetails[$i]["sub_link"];

                if (strpos($link, "http") !== false) {
                    $finalLink = $link;
                } else {
                    if (strpos($link, "?")) {
                        $links = explode("?", $link);
                        $finalLink = $serverHome."/".$links[0]."?lang=".$lang."&".$links[1];
                    } else {
                        if (strpos($link, "#")) {
                            $links = explode("#", $link);
                            $finalLink = $serverHome."/".$links[0]."?lang=".$lang."#".$links[1];
                        } else {
                            $finalLink = $serverHome."/".$link."?lang=".$lang;
                        }
                    }
                }

                $body .= '
                <li><a class="dropdown-item" 
                href="'.$finalLink.'">'
                .$categoryDetails[$i]["sub_name".$lang].'</a>
                </li>';
            } else {
                //Break
                $body .= '</ul> </li>';
                break;
            }
            $i++;
        }

    } else {
        //No subcat so add and continue
        $link = $categoryDetails[$i]["main_link"];
        if (strpos($link, "http") !== false) {
            $finalLink = $link;
        } else {
            if (strpos($link, "?")) {
                $links = explode("?", $link);
                $finalLink = $serverHome."/".$links[0]."?lang=".$lang."&".$links[1];
            } else {
                
                if (strpos($link, "#")) {
                    $links = explode("#", $link);
                    $finalLink = $serverHome."/".$links[0]."?lang=".$lang."#".$links[1];
                } else {
                    $finalLink = $serverHome."/".$link."?lang=".$lang;
                }
            }
        }
        $body .= '
            <li class="nav-item '.$status.'">
            <a href="'.$finalLink.'" class="nav-link">'
            .$categoryDetails[$i]["main_name".$lang].
            '</a></li>';
        $i++;
    }
}
?>

<div class="row p-1 m-0 justify-content-end" style="background-color: var(--accent); color: #fff;">
    <div class="col-12 justify-content-end text-end">
        <a target="_blank" class="topBtn" href="http://rit.ac.in/RITTOUR/vtour.html">Take A Tour </a>
        |
        <a target="_blank" class="topBtn" href="http://rit.ac.in/ritsoft/ritsoftv2/login.php#data"> RITSoft </a>
        |
        <a target="_blank" class="topBtn" style="text-align:left;" href="<?php echo $serverHome; ?>/login.php?lang=<?php echo $lang; ?>" > Login </a>
        |
        <a href="<?php echo $actual_link; ?>?lang=_en<?php echo $getParameters ?>" type="submit" name="english"
            class="topBtn <?php echo $engButton; ?>"> English </a>
        |
        <a href="<?php echo $actual_link; ?>?lang=_mal<?php echo $getParameters ?>" type="submit" name="malayalam"
            class="topBtn <?php echo $malButton; ?>"> മലയാളം </a>
    </div>
</div>

<!-- Logo -->
<div class="row bg-light p-0 m-0 justify-content-center">
    <div class="col-12 d-flex justify-content-center">
        <a class="navbar-brand" href="<?php echo $serverHome; ?>">
            <img class="img-fluid" style="max-height:20vh" src="<?php echo $serverHome; ?>/images/LOGO.png" alt="Logo">
        </a>
    <br>
    </div>
</div>


<!-- Announcements -->
<?php
if ($activePageName === 'Home' || $activePageName === 'Pensioners' ) {
    $announcements = retrieveData('SELECT * FROM ritwebsite_home_page_announcement WHERE expiry > NOW() ORDER BY priority', $con);
    $annTableName = 'ritwebsite_home_page_announcement';
    if ($activePageName === 'Pensioners') {
        $announcements = retrieveData("SELECT * FROM ritwebsite_pensioners_announcements ORDER BY priority", $con);
        $annTableName = 'ritwebsite_pensioners_announcements';
    }?>
<div class="row m-0 p-0 bg-light announcements">
    <div class="col-1 p-1 align-self-center bg-light">

        <div class="text-end me-2">
            <h4><i class="fas fa-bullhorn"></i></h4>
        </div>
        <!-- <div class="announcement-heading d-sm-none d-md-block">
            <h5 style="text-align:left; background-color:#fff; color:var(--accent);">
                <?php // echo $languages[$lang]["Announcements"] ?>
            </h5>
        </div> -->
        

    </div>
    <div class="col-10 p-0 m-0">
        <div class="marquee">
            <div class="marquee__content marqBar" id="marqAnim">
                <ul class="list-inline">
                    <li class="p-1 mx-1">
                        <a class="announcement-link" href="#" style="color:transparent;font-size:2vw">
                            Welcome to Rajiv Gandhi Institute Of Technology
                        </a>
                    </li>
                    <?php
                    for ($i=0; $i<count($announcements); $i++) {
                        $lastId = '';
                        if ($i+1 === count($announcements)) {
                            $lastId = 'id="lastAnnouncement"';
                        }
                        $announcement = $announcements[$i]["announcement".$lang];
                        $announcementLink = $announcements[$i]["link"];
                        $annLinkPos = strpos($announcementLink, $pathToUploads);
                        if ($annLinkPos !== false) {
                            $annLinkPart = substr($announcementLink, $annLinkPos);
                            $announcementLink = findHost().$annLinkPart;
                        } ?>

                    <li <?php echo $lastId; ?>class="announcement-item p-1 mx-1">
                        <a class="announcement-link" href="<?php echo $announcementLink; ?>" target="_blank">
                            <?php echo $announcement; ?>
                        </a>
                    </li>

                        <?php
                    } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-1">
        <form id="allAnnouncement" action="viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
            style="display:contents">
            <input type="hidden" name="vName" value="AllAnnouncements">
            <input type="hidden" name="table" value="ritwebsite_home_page_announcement">
            <a href="javascript:document.getElementById('allAnnouncement').submit()" class="view-more" id="viewMore">
                <i class="fas fa-angle-double-right"></i>
            </a>
        </form>
    </div>
</div>
<script>
    setAnimation();
    async function setAnimation() {
        // Get total width of all announcements
        var totalWidth = 0;
        var allAnn = await document.getElementsByClassName("announcement-item");
        for (let index = 0; index < allAnn.length; index++) {
            const element = allAnn[index];
            element.getBoundingClientRect();
            totalWidth += element.getBoundingClientRect().width;
        }

        // Set width and animation duration according to width
        var marq = document.getElementById("marqAnim");
        var timeDivider = 10;
        if (/Android|webOS|iPhone|iPad|Mac|Macintosh|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator
            .userAgent)) {
            timeDivider = 30;
            console.log("Mobile Device");
        }
        marq.style.width = totalWidth + "%";
        marq.style.animationDuration = totalWidth / timeDivider + "s";
        console.log(marq.style.width);
        console.log(marq.style.animationDuration);
    }

    // Running every 2 seconds to check if last element is shown
    setInterval(() => {
        var pos = document.getElementById("lastAnnouncement").getBoundingClientRect();
        var endPos = 0 - pos.width;
        if (pos.x < endPos) {
            var element = document.getElementById("marqAnim");
            element.classList.remove("marquee__content");

            // trigger a DOM reflow 
            void element.offsetWidth;

            element.classList.add("marquee__content");
        }
    }, 2000);
</script>
    <?php
} ?>


<!-- Nav Bar -->

<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light navbar-light" id="navbar">
    <div class="container-fluid p-0">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav m-auto">
                <?php echo $body; ?>
            </ul>
        </div>
    </div>

</nav>