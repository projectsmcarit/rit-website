<?php
  /**
   * Webpage showing login page
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Login
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/login
   */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
        require 'findUrl.php';
        require 'header.php';
        require 'locale.php';
        require 'connection.php';
        require 'retrieveData.php';
        $showError = 'hide';
    if (isset($_GET["err"])) {
            $errorBody = $languages[$lang][$_GET["err"]];
            $showError = 'show';
    }
    ?>
    <title> Rajiv Gandhi Institite of Techonology | Login</title>
    <style>
        .login-button {
            border-radius: 20px;
            background-color: var(--background);
            color: var(--text);
            border: 2px solid var(--accent);
            transition: 500ms;
        }

        .login-button:hover {background-color: var(--accent);}

        .errorMessage {color: red;}

        .show {display: block;}

        .hide {display: none;}
    </style>
</head>

<body onload="setTheme()">
    <?php 
        $activePageName = '';
        require 'nav.php';
    ?>

    <section clas="section-style">
        <div class="row justify-content-center m-4">
            <div class="col-md-4 col-10 p-4">
                <h2><?php echo $languages[$lang]["Login"] ?></h2>
                <form method="POST" action="authenticate.php">
                    <div class="form-group mb-3">
                        <input id="inputEmail" type="name" placeholder="<?php echo $languages[$lang]["Username"] ?>"
                            required="" autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4"
                            name="username">
                    </div>
                    <div class="form-group mb-3">
                        <input id="inputPassword" type="password"
                            placeholder="<?php echo $languages[$lang]["Password"] ?>" required=""
                            class="form-control rounded-pill <border-5></border-5> shadow-sm px-4 text-primary" name="password">
                    </div>
                    <div class="row text-center errorMessage <?php echo $showError ?>">
                        <p><i class="fas fa-bug"></i> <?php echo $errorBody; ?></p>
                    </div>
                    <input type="hidden" name="lang" id="lang" value="<?php echo $lang; ?>">
                    <div class="row justify-content-center p-5">
                        <button class="login-button p-2" type="submit">
                            <?php echo $languages[$lang]["Login"] ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <?php require 'footer.php'; ?>
</body>

</html>