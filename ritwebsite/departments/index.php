<?php
  /**
   * Webpage showing department details
   *
   * We use the get parameter "?dept" to find the
   * requested department. The fetchapi gets triggered
   * here and sends request to get details of teachers in
   * the current department.
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Department
   * @author   Original Author <ashlymohan174@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/departments?dept=DepartmentName
   */
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
       require '../findUrl.php';
       require '../header.php';
       require '../locale.php';
       require '../connection.php';
       require '../retrieveData.php';
       require '../api/fetchapi.php';
   
    if (isset($_GET['dept'])) {
        $selectedDeptKey = $_GET['dept'];
    } else {
        $selectedDeptKey = retrieveData("SELECT * FROM ritwebsite_sub_category WHERE main_id=(SELECT id FROM ritwebsite_main_category WHERE name_en='Departments')", $con)[0]["name_en"];
    }

    //  Fetching from API
    $apiAvoidDepartments = ['Administration'];
    if (in_array($selectedDeptKey, $apiAvoidDepartments)) {
        $apiResponse = [["DONT SHOW"]];
    } else {
        $apiDepartment = retrieveData("SELECT apiValue FROM ritwebsite_api_departments WHERE department='".$selectedDeptKey."'", $con)[0]["apiValue"];
        $apiAuth = retrieveData("SELECT * FROM ritwebsite_api_details", $con);
        $apiResponse = fetchDepartmentDetails($apiDepartment, $apiAuth[0]['apiKey'], $apiAuth[0]['username'], $apiAuth[0]['password']);
        
        // Saving API to cache
        $apiImageCachePath = "cache/imageData.txt";
        $apiDataCachePath = "cache/data.txt";

        $imageFile = fopen($apiImageCachePath, 'w');
        $dataFile = fopen($apiDataCachePath, 'w');
        
        ftruncate($imageFile, 0);
        ftruncate($dataFile, 0);

        fwrite($imageFile, urlencode(serialize($apiResponse[0])));
        fwrite($dataFile, urlencode(serialize($apiResponse[1])));

        fclose($imageFile);
        fclose($dataFile);
    }

       //  Starting fetch process
       $selectedDeptDetails = retrieveData('SELECT * FROM ritwebsite_department_details WHERE department = "'.$selectedDeptKey.'"', $con);
       $selectedDeptLinks = retrieveData('SELECT * FROM ritwebsite_department_function_headings WHERE department = "'.$selectedDeptKey.'"', $con);
       $selectedDeptNews = retrieveData('SELECT * FROM ritwebsite_department_news WHERE department = "'.$selectedDeptKey.'" AND expiry > NOW() ORDER BY id DESC', $con);
       $newsLength = count($selectedDeptNews);
       $selectedDeptGallery = retrieveData('SELECT * FROM ritwebsite_department_gallery WHERE department = "'.$selectedDeptKey.'"', $con);
       $galleryLength = count($selectedDeptGallery);
       $category = retrieveData('SELECT name_en,name_mal FROM ritwebsite_sub_category WHERE name_en="'.$selectedDeptKey.'"', $con);
       $selectedDeptName = $category[0]["name".$lang];
       $staffs=retrieveData('SELECT * FROM ritwebsite_staff_details WHERE department="'.$selectedDeptKey.'"', $con);

       $selectedDeptTitle = $category[0]["name_en"];
       $selectedDeptBanner = $category[0]["name".$lang];
    ?>
  <title>Rajiv Gandhi Institite Of Techonology | Departments | <?php echo $selectedDeptTitle; ?></title>

  <link rel="stylesheet" href="style.css">
</head>

<body onload="setTheme()">

  <?php
    $activePageName = "Departments";
    require '../nav.php';
    
    // Cover Image
    $coverImage = $selectedDeptDetails[0]["image"];
    $coverImageType = $selectedDeptDetails[0]["type"];
    $coverImg = "data:".$coverImageType.";base64,".$coverImage;

    // About Us
    $aboutUs = $selectedDeptDetails[0]["about_us".$lang];
    $aboutUsSub1 = $selectedDeptDetails[0]["about_us_sub_1".$lang];
    $aboutUsSub2 = $selectedDeptDetails[0]["about_us_sub_2".$lang];

    // Image of director
    $hodImage = $selectedDeptDetails[0]["hod_image"];
    $hodType = $selectedDeptDetails[0]["hod_type"];
    $hodImg = "data:".$hodType.";base64,".$hodImage;

    // Main Content
    $mainContent = $selectedDeptDetails[0]["main_content".$lang];
    ?>
  <!-- ======= Hero Section ======= -->
  <section id="hero" style="background: url('<?php echo $coverImg; ?>') top center;background-size: cover;">
    <div class="hero-container ">
      <h1><?php echo $selectedDeptBanner; ?></h1>
    </div>
  </section>

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about my-4">
      <div class="container p-2">
        <div class="row">

          <div class="col-12">
            <?php echo $aboutUs; ?>
          </div>

          <div class="col-sm-6 row-md">
            <?php echo $aboutUsSub1; ?>
          </div>

          <div class="col-sm-6 row-md">
            <?php echo $aboutUsSub2; ?>
          </div>

        </div>
      </div>
    </section>

    <!-- Special Features -->
    <?php
    if ($selectedDeptKey !== "Administration" && count($selectedDeptLinks) > 0) {
        ?>
    <section class="features">
      <div class="container">
        <div class="row justify-content-center p-4">

        <?php
        for ($i=0; $i < count($selectedDeptLinks); $i++) {
            $linkId = $selectedDeptLinks[$i]["id"];
            $linkIcon = $selectedDeptLinks[$i]["icon"];
            $linkTitle = $selectedDeptLinks[$i]["title".$lang];
            $linkSubTitle = $selectedDeptLinks[$i]["subHeading".$lang];
            $linkRedirect = $selectedDeptLinks[$i]["link"];
            if ($linkRedirect === "#" || $linkRedirect === "") {
                $linkRedirect = 'functions?lang='.$lang.'&dept='.$selectedDeptKey.'&type='.$linkId;
            } else {
                $linkPos = strpos($linkRedirect, "/Admin");
                $subLink = substr($linkRedirect, $linkPos);
                $linkRedirect = findHost().$subLink;
            } ?>

          <div class="col-sm-8 col-md-4 p-2 ">
            <div class="p-1 text-center featureCard">
              <a href="<?php echo $linkRedirect; ?>" target="_blank">
                <h1> <i class="fas"> &#x<?php echo $linkIcon; ?> </i> </h1>
                <h4> <?php echo $linkTitle; ?> </h4>
                <hr class="featureRule">
                <p> <?php echo $linkSubTitle; ?> </p>
              </a>
            </div>
          </div>

            <?php
        } ?>

        </div>
      </div>
    </section>

        <?php
    }
    ?>

    <!-- News And Events -->
    <?php
    if ($newsLength != 0) {
        ?>
    <section>
      <div class="container p-2">
      <div id="multi-item-example" class="news-events carousel slide carousel-multi-item">

        <!--Heading-->
        <div class="row">
          <div class="col-8">
            <h1 class="title">
              <?php echo $languages[$lang]["News"];
                if ($newsLength !== 0) {
                    ?>
              <form id="allNews" action="../viewAll/index.php?lang=<?php echo $lang; ?>" method="post"
                style="display:contents">
                <input type="hidden" name="vName" value="AllNews">
                <input type="hidden" name="table" value="ritwebsite_department_news">
                <input type="hidden" name="dept" value="<?php echo $selectedDeptKey; ?>">
                <a href="javascript:document.getElementById('allNews').submit()" class="view-more" id="viewMore">
                  <i class="fas fa-angle-double-right"></i>
                </a>
              </form>
                    <?php
                } ?>
            </h1>
          </div>

          <div class="controls-top col-4 align-self-center text-end">
            <button class="circle-btn" type="button" data-bs-target="#multi-item-example" data-bs-slide="prev">
              <i class="fas fa-chevron-left"></i></button>
            <button class="circle-btn" type="button" data-bs-target="#multi-item-example" data-bs-slide="next">
              <i class="fas fa-chevron-right"></i></button>
          </div>

        </div>

        <!--Slides-->
        <div class="carousel-inner" role="listbox">

          <?php
            $emptyNews = '';
            $newsIndex = 0;
            $totalLength = $newsLength;
            while ($totalLength != 0) {
                $newsActive = '';
                if ($newsIndex == 0) {
                    $newsActive = 'active';
                } ?>

          <div class="carousel-item <?php echo $newsActive; ?>">

                <?php
                $newsRepeat = 4;
                if ($totalLength < 4) {
                    $newsRepeat = $totalLength;
                }

                for ($j=0; $j<$newsRepeat; $j++) {
                    $emptyNews = 'Not Empty';
                    $newsType = $selectedDeptNews[$newsIndex]["type"];
                    if ($newsType == null) {
                        $newsImage = '../images/news-holder.png';
                    } else {
                        $newsBlob = $selectedDeptNews[$newsIndex]["image"];
                        $newsImage = 'data:' .$newsType.' ;base64,'.$newsBlob;
                    }
                    $newsHeading = $selectedDeptNews[$newsIndex]["heading".$lang];
                    $newsBody = $selectedDeptNews[$newsIndex]["body".$lang];
                    $newsLink = $selectedDeptNews[$newsIndex]["link"];
                    $newsLinkPos = strpos($newsLink, $pathToUploads);
                    if ($newsLinkPos !== false) {
                        $newsLinkPart = substr($newsLink, $newsLinkPos);
                        $newsLink = findHost().$newsLinkPart;
                    } ?>

            <div class="col-md-3 p-1" style="float:left">
              <div class="card mb-2 news-card">
                <img class="card-img-top" src="<?php echo $newsImage; ?>" alt="Card image cap" style="height:200px;">
                <div class="card-body scroll-bar">
                  <a target="_blank" href="<?php echo $newsLink; ?>">
                    <h4 class="card-title"><?php echo $newsHeading; ?></h4>
                  </a>
                  <p class="card-text">
                    <?php echo $newsBody; ?>
                  </p>
                </div>
              </div>
            </div>

                        <?php
                         $newsIndex += 1;
                } ?>

          </div>

                <?php
                        $totalLength -=  $newsRepeat;
            }
            if ($emptyNews === '') {
                echo '<h5 class="text-center">No Recent Updates</h5>';
            } ?>

        </div>
        <!--/.Slides-->

      </div>
      </div>
    </section>
        <?php
    } ?>

    <!-- ======= HOD  Section ======= -->
    <section id="desk" class="desk">
      <div class="container p-2 hod-container">

        <div class="col-md row justify-content-evenly">

          <div class="col-sm-12 col-md-6 justify-content-center text-center order-1">
            <img src="<?php echo $hodImg; ?>" class="img-fluid rounded-circle hod-img" alt="Hod">
          </div>

          <div class="col-sm-12 col-md-6 row order-1 order-md-0">
            <?php echo $mainContent; ?>
          </div>

        </div>

      </div>
    </section>

    <!-- ======= Our Team Section ======= -->
    <section id="team" class="team" style="margin-bottom:0">

      <div class="container p-2">

        <div class="section-title">
          <?php 
            if ($selectedDeptKey == "Administration") { ?>
            <h1>
            <a href="javascript:toggleStaffs()">
              <i id="staffToggleIcon" class="fas fa-chevron-down"></i>
            </a>
            Administrative Staffs
          </h1>
            <?php } else { ?>
            <h1>
            <a href="javascript:toggleStaffs()">
              <i id="staffToggleIcon" class="fas fa-chevron-down"></i>
            </a>
            Our Team
          </h1>
                <?php
            } 
            ?>
         
        </div>

        <div id="allStaff">
        <div class="row justify-content-center">

          <!-- ********************** -->
          <!-- Staff Details from API -->
          <!-- ********************** -->        
            <!-- Show loading -->
            <?php if ($apiResponse == null) { ?>
            <div id="progressBar" class="loader"></div>

            <!-- Show error -->
            <?php } elseif ($apiResponse == [['ERROR']]) {?>
            <h5 class="text-left">
              <i class="fas fa-exclamation-triangle" style="color:#ff0033"></i>
              Oops!!! Unable to get details from RitSoft </h5>

            <?php } elseif ($apiResponse == [["DONT SHOW"]]) {
                // Dont show anything
            } else {
                // Show teachers
                for ($i=0; $i < count($apiResponse[1]); $i++) {
                    $teacherImageId = $apiResponse[1][$i]['fid'];
                    $img = $apiResponse[0][$teacherImageId];
                    if ($img === '') {
                          $teacherImage =  '../images/defaultProfile.png';
                          $postImgDetail = "../".$teacherImage;
                    } else {
                        $teacherImage = "data:image/jpeg;base64,".$img;
                        $postImgDetail = $teacherImage;
                    }
                    $teacherName = $apiResponse[1][$i]['name'];
                    $teacherPosition = $apiResponse[1][$i]['designation'];
                    $teacherMob = $apiResponse[1][$i]['phoneno'];
                    $teacherEmail = $apiResponse[1][$i]['emailid']; ?>

            <div class="col-md-3 row p-4">
              <form action="Profile/index.php" method="POST" id="gotoDetails">
                <input type="hidden" name="retrieveType" value="API">
                <input type="hidden" name="index" value="<?php echo $i; ?>">
                <button type="submit" class="col-12 teacherCard">
                  <div class="col justify-content-center text-center teacher">

                    <div class="row p-2">
                      <img src="<?php echo $teacherImage; ?>" alt="Teacher Image" 
                      class="img-fluid rounded-circle img-height">
                    </div>

                    <div class="row">
                      <h5> <?php echo $teacherName; ?> </h5>
                    </div>

                    <div class="row teacherDetail">
                      <h6> <?php echo $teacherPosition; ?> </h6>
                    </div>

                    <div class="row teacherDetail">
                      <p> <?php echo $teacherEmail; ?> </p>
                    </div>

                  </div>
                </button>
              </form>
            </div>

                    <?php
                }
            }
            ?>

          <!-- ********************************* -->
          <!-- Staff Details from Local Database -->
          <!-- ********************************* -->

          <br>
          <?php 
            if (count($staffs) !== 0 ) { 
                $headingKeyForStaff = "TechnicalStaff";
                if ($selectedDeptKey === "Administration") {
                    $headingKeyForStaff = "AdminStaff";
                }
                ?>
          <h3 class="techStaffTitle">
             <?php echo $languages[$lang][$headingKeyForStaff] ?> 
          </h3>
          <hr>
            <?php } ?>

          <?php
            for ($i=0;$i<count($staffs);$i++) {
                $staffId = $staffs[$i]["id"];
                $staffName = $staffs[$i]["name"];
                $staffImageType = $staffs[$i]["type"];
                $staffImage = $staffImageType == '' ? '../images/defaultProfile.png' : 'data:' .$staffImageType.' ;base64,'.$staffs[$i]["image"];
                $staffPostImageDetail = $staffImage;
                if ($staffImage === '../images/defaultProfile.png') {
                    $staffPostImageDetail = "../".$staffImage;
                }
                $staffDesignation = $staffs[$i]["designation"];
                $staffMobNo = $staffs[$i]["phoneno"];
                $staffEmailId = $staffs[$i]["emailid"]; ?>
          <div class="col-md-3 row p-4">
            <form action="Profile/index.php" method="POST" id="gotoDetails">
              <input type="hidden" name="retrieveType" value="LOCAL">
              <input type="hidden" name="index" value="<?php echo $staffId; ?>">

              <button type="submit" class="col-12 teacherCard">
                <div class="col justify-content-center text-center teacher">

                  <div class="row p-2">
                    <img src="<?php echo $staffImage; ?>" alt="Teacher Image"
                    class="img-fluid rounded-circle img-height">
                  </div>

                  <div class="row">
                    <h5> <?php echo $staffName; ?> </h5>
                  </div>

                  <div class="row teacherDetail">
                    <h6> <?php echo $staffDesignation; ?> </h6>
                  </div>

                  <div class="row teacherDetail">
                    <p> <?php echo $staffEmailId; ?> </p>
                  </div>

                </div>

            </form>
          </div>
                <?php
            }?>
        </div>
        </div>

      </div>
    </section>

    <!-- ======= Our Gallery Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container p-2">

        <div class="section-title">
          <h1><?php echo $languages[$lang]['Gallery'] ?></h1>
        </div>

        <div class="row portfolio-container justify-content-center">

          <?php
            for ($i=0; $i<$galleryLength; $i++) {
                $image = $selectedDeptGallery[$i]["image"];
                $type = $selectedDeptGallery[$i]["type"];
                $galleryImg = "data:".$type.";base64,".$image; ?>
          <div class="col-md-3 row gallery-img" style="height:250px">
            <img class="img-fluid rounded" style="height:250px;padding:10px" src="<?php echo $galleryImg; ?>"
              alt="image">
          </div>

                <?php
            } ?>

        </div>

      </div>
    </section>

  </main>

  <?php require '../footer.php'; ?>

  <script>
  function toggleStaffs(){
    var allStaff = document.getElementById("allStaff");
    var icon = document.getElementById("staffToggleIcon");
    const turnOn = allStaff.style.display == "none";
    if (turnOn) {
      allStaff.style.display = "block";
      icon.style.transform = "rotate(0deg)";
      setTimeout(() => {
        allStaff.style.transform = "translate(50%,50%) scale(1) translate(-50%,-50%)";
      }, 500);
    } else {
      allStaff.style.transform = "translate(-50%,-50%) scale(0)";
      icon.style.transform = "rotate(180deg)";
      setTimeout(() => {
        allStaff.style.display = "none";
      }, 700);
    }
  }
  </script>
</body>

</html>