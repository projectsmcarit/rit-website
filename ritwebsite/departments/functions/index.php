<?php
  /**
   * Webpage showing facility details
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Facilities
   * @author   Original Author <ashlymohan174@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/Facilities
   */
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  
     require '../../findUrl.php';
     require '../../header.php';
     require '../../connection.php';
     require '../../retrieveData.php';
     require '../../locale.php';
    if (!isset($_GET["dept"], $_GET["type"])) {
        header("Location: ../index.php");
    }
    $currentDepartment = $_GET["dept"];
    $currentType = $_GET["type"];
    $heading = retrieveData("SELECT * FROM ritwebsite_department_function_headings WHERE id=".$currentType, $con)[0];
    $detail = retrieveData("SELECT * FROM ritwebsite_department_function_detail WHERE function_type=".$currentType." AND department='".$currentDepartment."' ", $con);
    // For image slider
    $imageActive = 'data-bs-slide-to="0" class="active"
    aria-current="true" aria-label="Slide 1"';
    $imgActive = 'active';
    $indicatorIndex = 1;
    $indicatorButtons = '';
    $imageSliderImages = '';

    // For Text Only
    $textOnly = '';

    // For Img And Text
    $imgAndText = '';
    foreach ($detail as $key => $value) {
        if ($value["body_en"] === '') {
              $imgType = $value["type"];
              $imgData = $value["image"];
              $img = 'data:'.$imgType.';base64,'.$imgData;

              $indicatorButtons .= '
              <button type="button" data-bs-target="#carouselExampleIndicators" 
              '.$imageActive.'></button>';
              $imageSliderImages .= '
              <div class="carousel-item '.$imgActive.'">
                <img src="'.$img.'" class="d-block w-100 funcImg" alt="Image">
              </div>';

              $imageActive = 'data-bs-slide-to="'.$indicatorIndex.'"
              aria-label="Slide '.$indicatorIndex++.'"';
              $imgActive = '';
        } elseif ($value["image"] === '') {

              $textOnly .= '
              <div class="row my-4">
                <div class="col-12">
                  '.$value["body".$lang].'
                </div>
              </div>';

        } else {

              $imgType = $value["type"];
              $imgData = $value["image"];
              $img = 'data:'.$imgType.';base64,'.$imgData;

              $imgAndText .= '
              <div class="row my-4">

                <div class="col-sm-12 col-md-6">
                  <img class="img-fluid funcDetailImg"
                   src="'.$img.'" alt="Image">
                </div>
                <div class="col-sm-12 col-md-6">
                  '.$value["body".$lang].'
                </div>
      
              </div>';
              
        }
    }
    ?>
  <title>Rajiv Gandhi Institute of Technology | Facilities</title>

  <link href="style.css" rel="stylesheet">

</head>

<body onload="setTheme()">

  <?php
    $activePageName = "";
    require '../../nav.php';
    ?>

  <main>
    <!-- For records with image only -->
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <?php echo $indicatorButtons; ?>
      </div>
      <div class="carousel-inner">
        <?php echo $imageSliderImages; ?>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>

    <!-- Function Heading -->
    <div class="container p-2">
      <div class="row">
        <div class="col-12">
          <h1>
            <i class="fas"> &#x<?php echo $heading["icon"]; ?> </i>
            <?php echo $heading["title".$lang]; ?>
          </h1>
          <h4> <?php echo $heading["subHeading".$lang] ?> </h4>
        </div>
      </div>
    </div>

    <!-- Function Content -->
    <div class="container p-2">
      <?php
        echo $textOnly;
        echo $imgAndText;
        ?>
    </div>
  </main>

  <?php require '../../footer.php'; ?>

</body>

</html>