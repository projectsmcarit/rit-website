<?php
  /**
   * Webpage showing profile details
   *
   * This webpage is triggered from departments page
   * The details of the selected teacher will be sent
   * by POST, thereby eliminating need to call API again
   *
   * PHP version 5.4.3
   *
   * @category Webpage
   * @package  Profile
   * @author   Original Author <ashlymohan174@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/departments/Profile
   */
require "../../findUrl.php";
$recordId = $_POST["index"];
if ($_POST["retrieveType"] === "API") {
    $apiImageCachePath = "../cache/imageData.txt";
    $apiDataCachePath = "../cache/data.txt";
    if (file_exists($apiDataCachePath)) {

        $imageFile = fopen($apiImageCachePath, 'r');
        $dataFile = fopen($apiDataCachePath, 'r');

        $imageFileContent = fread($imageFile, filesize($apiImageCachePath));
        $dataFileContent = fread($dataFile, filesize($apiDataCachePath));

        fclose($imageFile);
        fclose($dataFile);

        $teacherDetail = unserialize(urldecode($dataFileContent))[$recordId];
        $imageId = $teacherDetail["fid"];
        $teacherBlob = unserialize(urldecode($imageFileContent))[$imageId];
        $teacherImage = "data:image/jpeg;base64,".$teacherBlob;
    } else {
        header("Location: ".findHost()."/departments");
    }
} else {
    include '../../retrieveData.php';
    include '../../connection.php';
    $staffDetails = retrieveData("SELECT * FROM ritwebsite_staff_details WHERE id=".$recordId, $con);
    $teacherDetail = $staffDetails[0];
    $teacherImage = "data:".$staffDetails[0]["type"].";base64,".$staffDetails[0]["image"];
}

// Default image
$emptyImageConfig = [
    '',
    'data:image/jpeg;base64,',
    'data:;base64,'
];
if (in_array($teacherImage, $emptyImageConfig)) {
    $teacherImage = "../../images/defaultProfile.png";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rajiv Gandhi Institute Of Technology | Profile</title>
    <link rel="stylesheet" href="style.css">

    <?php require '../../header.php'; ?>
</head>

<body onload="setTheme()">
    <div class="container">

        <div class="row justify-content-center">

            <!-- Back Button -->
            <div class="col-12">
                <a href="javascript:history.back()" class="back-btn">
                    <i class="fas fa-arrow-circle-left"></i>
                </a>
            </div>

            <!-- Logo -->
            <div class="col-12 text-center mb-4">
                <img src="../../images/LOGO.png" alt="RIT Logo" class="img-fluid max">
            </div>

            <hr>

            <!-- Image -->
            <div class="col-sm-12 col-md-6 text-center ">
                <img src="<?php echo $teacherImage ?>" alt="Image" class="img-fluid max">
            </div>

            <!-- Name and other details -->
            <div class="col-sm-12 col-md-6">
                <div class="container">
                    <div class="row">
                        <h1 class="col-12 "> <?php echo $teacherDetail["name"]; ?> </h1>
                        <h2 style="font-family: 'Poppins', sans-serif"
                        class="col-12 "> <?php echo $teacherDetail["designation"]; ?> </h2>
                        <h3 class="col-12 "> <?php echo $teacherDetail["emailid"]; ?> </h3>
                        <h4 class="col-12 "> <?php echo $teacherDetail["phoneno"]; ?> </h4>

                        <!-- Showing scholar link, if exist -->
                        <?php 
                        if (array_key_exists("publication_scholar_link", $teacherDetail)) {
                            $scholarLink = $teacherDetail["publication_scholar_link"];
                            if ($scholarLink !== "" && $scholarLink !== null) {
                                ?>
                        <h4 class="col-12 ">
                            <a href="<?php echo $teacherDetail["publication_scholar_link"]; ?>"
                             target="_blank" rel="noopener noreferrer">
                             <i class="fas fa-external-link-alt"></i> Scholar Profile</a>
                        </h4>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>

            <hr class="mt-2">

            <!-- Basic Details -->
            <h3>Basic Details</h3>

            <table class="table table-striped table-responsive">
                <?php
                $groupItems ='';
                $seperators = ["##"];
                $tableViewKeys = ["qualification","patent"];
                $tableViewHeadings = [
                    "qualification" => [' Qualification ',' Specialization ',' University ', ' Year '],
                    "patent" => [' Patent No. ',' Issued By ',' Validity ',' Details ']
                ];
                foreach ($teacherDetail as $key => $value) {
                    $flag = 0;
                    switch ($key) {
                    // Items not to be shown
                    case 'name':
                    case 'fid':
                    case 'designation':
                    case 'emailid':
                    case 'phoneno':
                    case 'major_publications':
                    case 'publication_scholar_link':
                    case 'image':
                    case 'type':
                    case 'id':
                    case 'priority':
                    case 'teachexp_as_on':
                        $flag = 1;
                        break;
                    }

                    // Skipping if not needed
                    if ($flag === 1) {
                        continue;
                    } else {
                        //Skipping if no content
                        if ($value === ''
                            || $value === '0' 
                            || $value === null
                        ) {
                            continue;
                        }
                            
                        // Formatting title
                        switch ($key) {
                        case 'research_and_develop':
                            $title = "Research And Development";
                            break;

                        case 'no_papers_journal':
                            $title = "No: of publications";
                            break;
                            
                        case 'no_papers_conference':
                            $title = "No: of conference papers";
                            break;
                            
                        default:
                            $titles = explode("_", $key);
                            $title = '';
                            for ($i=0; $i < count($titles); $i++) {
                                $title .= ucfirst($titles[$i]) . " ";
                            }
                            break;
                        }
                        
                        $content = '';
                        if (in_array($key, $tableViewKeys)) {

                            // CASE A -> Table View
                            $groupDisplayType = "table";
                            $groupDisplayClass = ' class="table table-striped table-responsive"';

                            // Setting table headings
                            $tableHeadings = $tableViewHeadings[$key];  
                            $theadContent = '<thead class="table-dark"><tr>';
                            foreach ($tableHeadings as $headKey => $headingValue) {
                                $theadContent .= "<th> ".$headingValue." </th>";
                            }
                            $theadContent .= "</tr></thead>";

                            // Adding table content
                            $outerBody = explode("##", $value);
                            for ($i=0; $i < count($outerBody); $i++) { 
                                $body = explode("@@", $outerBody[$i]);
                                $qualText = '<tr>';
                                for ($j=0; $j < count($body); $j++) {
                                    $qualText .= "<td> ".$body[$j]." </td>";
                                }
                                $content .= $qualText."</tr>";
                            }
                            $content = $theadContent.$content;

                        } else {

                            // CASE B -> List View
                            $groupDisplayType = "ul";
                            $groupDisplayClass = "";

                            for ($j=0; $j < count($seperators); $j++) {
                                if (strpos($value, $seperators[$j]) !== false) {
                                    $body =explode($seperators[$j], $value);
                                    for ($j=0; $j < count($body); $j++) {
                                        $content .= '<li>
                                            '.$body[$j].'
                                            </li>';
                                    }
                                    break;
                                }
                            }

                        }
                                

                        if ($content === '') {
                            $content = $value;
                        } else {
                            $groupItems .= '
                                    <div class="col-12"> <h5> '.$title.' </h5> </div>
                                    
                                    <div class="table-responsive">
                                        <'.$groupDisplayType.$groupDisplayClass.'>
                                        '.$content.' 
                                        </'.$groupDisplayType.'>
                                        </div>
                                    
                                    <hr class="my-2">';
                            continue;
                        } ?>

                <tr>
                    <th style="text-align:left;"> <?php echo $title; ?> </th>
                    <td> <?php echo $content; ?> </td>
                </tr>

                        <?php
                    }
                }
                ?>
            </table>
            <!-- End of Basic Details -->


            <!-- Grouped Details -->
            <hr class="mt-2">
            <?php echo $groupItems; ?>
            <!-- End of Grouped Details -->

            <!-- Major Publications -->
            <?php
            if ($teacherDetail["major_publications"] !== ''
                && $teacherDetail["major_publications"] !== null
            ) {?>
            <h3> Major Publications </h3>
            <div class="col-12 justify-content-center">
                <?php
                    $pubs = explode('##', $teacherDetail["major_publications"]);
                    $pubIndex = 1;
                foreach ($pubs as $key => $value) { ?>
                <div class="col-12 m-2 p-4 pubs">
                    <?php 
                    
                    echo $value; ?>
                </div>
                <?php }
                ?>
            </div>
                <?php
            }
            ?>
        <!-- End of Major Publications -->
        </div>
    </div>
</body>

</html>