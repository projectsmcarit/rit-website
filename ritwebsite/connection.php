<?php
  /**
   * Utility to connect with databases
   *
   * PHP version 5.4.3
   *
   * @category Utility
   * @package  Database
   * @author   Original Author <hoseakalayil@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?>
<?php 
$host = "127.0.0.1";
$dbname = "ritwebdb";
$username = "ritwebadmin";
$password = "ritwebadminpass";

// $host = "127.0.0.1";
// $dbname = "test";
// $username = "root";
// $password = "";

$con=mysqli_connect($host, $username, $password) or die("server not found");
mysqli_select_db($con, $dbname)or die("database not found");



?>