<?php
    /**
     * Utility to get the host url
     *
     * In the development enviornment the host was
     * localhost:8088/ritwebsite
     * Since we are using this function to get the host
     * Any changes to the URL can be made here and it will
     * be reflected in all its usages
     * PHP version 5.4.3
     *
     * @category Utility
     * @package  Ritwebsite
     * @author   Original Author <hoseakalayil@gmail.com>
     * @license  https://opensource.org/licenses No License
     * @version  SVN: $1.0$
     * @link     http://rit.ac.in/
     */

/**
 * Function to find Host name
 *
 * @return String
 */
function findHost()
{
    $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $path = explode("/ritwebsite/", $_SERVER['REQUEST_URI'])[0];
    return rtrim($host.$path, '/').'/ritwebsite';
}

/**
 * Function to find complete link
 *
 * @return String
 */
function findActualLink()
{
    $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $path = explode("?", $_SERVER['REQUEST_URI'])[0];
    return $host.$path;
}//end findActualLink()
?>
