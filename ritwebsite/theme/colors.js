const colorThemes = [
    {
        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#e3c2c2',
        '--accent': '#ff0000',
        '--newsevent': '#F6B8B8',
        '--blur': 'rgba(255, 99, 71, 0.5)',
        '--footerColor': '#3c0d02',
    },

    {
        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#E3C3DC',
        '--accent': '#EE0565',
        '--newsevent': '#FABDEA',
        '--blur': 'rgba(186, 2, 33, 0.5)',
        '--footerColor': '#620221',
    },

    {
        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#C2C2E3',
        '--accent': '#0000ff',
        '--newsevent': '#08c0f7',
        '--blur': 'rgba(99, 71, 255, 0.5)',
        '--footerColor': '#0d023c',
    },

    {

        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#E3C8F9',
        '--accent': '#770F8C',
        '--newsevent': '#E7E1F7',
        '--blur': 'rgba(167, 14, 139, 0.5)',
        '--footerColor': '#2E0636',
    },

    {
        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#C2E3C2',
        '--accent': '#00ff00',
        '--newsevent': '#09f673',
        '--blur': 'rgba(99, 255, 71, 0.5)',
        '--footerColor': '#0d3c02',
    },

    {
        '--background': '#ffffff',
        '--text': '#00003D',
        '--inactive': '#FBE3C6',
        '--accent': '#efb810',
        '--newsevent': '#fff2ad',
        '--blur': 'rgba(255,118,19,0.5)',
        '--footerColor': '#ad5a13',
    },
];