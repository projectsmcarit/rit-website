<?php
  /**
   * Utility to show color set
   * 
   * The files hold data needed for displaying
   * and inserting color sets in the admin panel
   * Any color types added in colors.js should 
   * be added here as well
   *
   * PHP version 5.4.3
   *
   * @category Utility
   * @package  Home
   * @author   Original Author <shibythomas1998@gmail.com>
   * @license  https://opensource.org/licenses No License
   * @version  SVN: $1.0$
   * @link     http://rit.ac.in/
   */
?>
<?php
/**
 * Used to show dsplay names while viewing in Admin panel
 */
$colorTypes = [
    "Background",
    "Text",
    "Shadow_color",
    "Accent",
    "News_Events_BG",
    "Overlay_color",
    "Footer_color",
];

/**
 * Used to insert color set in the js file on color set insert
 */
$cssVariableName = [
    "--background",
    "--text",
    "--inactive",
    "--accent",
    "--newsevent",
    "--blur",
    "--footerColor",
];

// Sample Color set
$sampleColorValues = [
    "#ffffff",
    "#000000",
    "#E3C3DC",
    "#EE0565",
    "#FABDEA",
    "rgba(186, 2, 33, 0.5)",
    "#620221"
]
?>