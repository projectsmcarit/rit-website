CREATE DATABASE IF NOT EXISTS ritwebdb;
GRANT ALL PRIVILEGES ON ritwebdb.* TO 'ritwebadmin'@'localhost' IDENTIFIED BY 'ritwebadminpass';
FLUSH PRIVILEGES;

