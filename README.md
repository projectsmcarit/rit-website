# Rit Website #
## Directory Structure ##

<details>
<summary>
For Major Functionalities
</summary>

```
.
├── \Admin
│   ├── \users # Accessible only to ADMIN
│   │   ├── index.php           
│   │   ├── style.css           -> UI page to show all users
│   │   ├
│   │   ├── addUser.php         -> UI page for showing fields to add user
│   │   ├── addUserLogic.php    -> SQL logic to add new user
│   │   ├
│   │   ├── updateUser.php      -> UI page for showing fields to update user
│   │   ├── updateUserLogic.php -> SQL logic for updating user
│   │   ├
│   │   ├── deleteLogic.php     -> SQL logic for deleting a user
│   │   ├
│   │   ├── typeOfUsers.php     -> Holds all type of users tha can be added
│   │   │
│   │   .
│   ├── \tables # Accessible to ADMIN, COORDINATOR
│   │   ├── /uploads            -> Folder that stores all file uploads
│   │   ├
│   │   ├── tableDetail.php     -> The crucial file that holds data about all the tables
│   │   │                          this file ensures proper working of
│   │   │                          all table actions
│   │   ├
│   │   ├── index.php           -> UI page that shows all the tables accessible in 3
|   |   |                          phases
│   │   ├── getFieldValue.php   -> Function that returns value for a field in the
│   │   │                          selected table
│   │   ├
│   │   ├── insert.php          -> UI page for showing fields to add record
│   │   ├── insertLogic.php     -> SQL logic to add new record
│   │   ├
│   │   ├── update.php          -> UI page for showing fields to update record
│   │   ├── updateLogic.php     -> SQL logic for updating record
│   │   ├
│   │   ├── getInputField.php   -> Function that returns the correct input field for a
│   │   │                          table field. Used in insert & update
│   │   ├
│   │   ├── deleteLogic.php     -> SQL logic for deleting a record
│   │   │
│   │   .
│   ├── header.php              -> Files included in all admin pages. These files 
│   ├── sideBar.php                includes the js and css necessary. It also checks for
│   ├── nav.php                    session validity
│   ├
│   ├── index.php               -> UI files that build the admin dashboard
│   ├── script.js
│   ├── theme.css
│   ├
│   ├── addColor.php            -> UI page to add new color set
│   ├── addColorLogic.php       -> File Manipulation code to add the new color set
│   ├
│   ├── deleteColor.php         -> File Manipulation code to delete existing color set
│   ├
│   ├── changePassword.php      -> SQL logic to update current users password
│   ├
│   ├── logoutLogic.php         -> Destroys current session and redirects to login page
|   |
|   .
|
├── \theme
│   ├── colors.js               -> Actual JS file that contains all color sets
│   ├── colors.php              -> PHP file that keeps track off the number of colors
|   |                              in a color set and their names for file manipulation
|   |                              in admin side.
|   .
├── \api
│   ├── fetchapi.php            -> Function that uses curl to fetch staff details
|   |                              from Rit Soft API
|   .
.
```

</details>

<details>
<summary>
For UI side
</summary>

```
.
├── \Academics
│   ├── index.php       -> UI page thats used to show UG PG courses offered
│   ├── style.css          Uses ?type to distinguish between UG PG
|   .
├── \Activities
│   ├── index.php
│   ├── style.css       -> UI page that shows all activities
|   .
├── \contact us
│   ├── index.php       -> UI page that shows all email and phone numbers
│   ├── style.css
|   .
├── \departments
│   ├── \functions
|   |   ├── index.php   -> UI page to show details of a selected functionality
|   |   ├── style.css      selected from department page
|   |   .
│   ├── \Profile
|   |   ├── index.php   -> UI page to show details of selected staff.
|   |   ├── style.css      This works on API and local DB
|   |   .
│   ├── index.php       -> UI page to show details of a department 
│   ├── style.css          Uses ?dept to distinguish between departments
|   .
├── \Facility
│   ├── index.php
│   ├── style.css       -> UI page to show facilities
|   .
├── \fileUploads
│   ├── index.php       -> A UI template to show all file uploads
│   ├── style.css          It uses ?fileUploadType to distinguish
|   .
├── \gallery
│   ├── index.php
│   ├── style.css       -> UI page to show all uploaded images
|   .
├── \viewAll
│   ├── index.php       -> A UI template to show all news, events, etc
│   ├── style.css          Including expired ones
|   .
├── header.php          -> Crucial Files included in all user side files
├── nav.php                This include header, navbar and footer
├── footer.php
├── theme.css
|
├── index.php
├── style.css           -> The home page that the user sees
|
├── login.php           -> The UI page to get username & password
├── authenticate.php    -> The SQL logic that carries out login process
|
├── connection.php      -> Initiates connection with the database
├── retrieveData.php    -> A dedicated function that takes SQL query string as argument
|                          and returns a array of records
|
├── findUrl.php         -> Dedicated functions to retrieve root of system
├── locale.php          -> Array that holds all static values for localiization
.
```

</details>
<br>

## Database Rough book ##
[Google Doc](https://docs.google.com/document/d/1kM3-mdtZ_EiSYh9dXxvKc75tQRH7xObuFrSKqbpaVx8/edit?usp=sharing)


## Database Backups ##
There are 2 backups
* ritwebdb.sql:- This is used to restore database when the container restarts

* ritwebsite/adminer.sql:- This is used to restore database on test server using adminer. Since max upload in adminer is 2MB

### Contributors ###
Academic project by 
* [Ashly Mathew](mailto:ashlymohan174@gmail.com)
* [Hosea Varghese Kalayil](https://hvkalayil.github.io/)
* [Shiby Thomas](mailto:shibythomas1998@gmail.com)

Under the guidance of
* [Jane George](mailto:jangeok@gmail.com),
* [Shalu Murali](mailto:shalumurali18@rit.ac.in)
* [Sonupriya PS](mailto:sonupriyaps@gmail.com)
* [Tomsy Paul](mailto:tomsypaul@gmail.com)
