Take terminal and give the following
------------------------------------

cd rit-website

//Build the image named rit-website-image (windows users, no sudo; non debians, please su to root)
sudo docker build -t rit-website-image .

//Create the container with the name ritwebsite-container
sudo docker create --restart unless-stopped -it -p 8088:80 -v $(pwd)/ritwebsite/:/var/www/html/ritwebsite --name ritwebsite-container rit-website-image
//For Windows users
sudo docker create --restart unless-stopped -it -p 8088:80 -v %cd%/ritwebsite/:/var/www/html/ritwebsite --name ritwebsite-container rit-website-image
//A container with a volume is created. 

//start the container
sudo docker container start -i -a ritwebsite-container
(You may use any suitable port number, but use the same below also)

// To detach from container press Ctrl+p+q

Then open the browser and give 127.0.0.1:8088/ritwebsite

For subsequent executions, put your code inside the volume (i.e. rit-website/ritwebsite/ folder)  and refresh the browser.
If something not working, you can stop the container and start it again by any one of the following:

  1) sudo docker container stop ritwebsite-container
     sudo docker container start ritwebsite-container 
		OR
  2) sudo docker container restart ritwebsite-container
 


Database details
****************

root password - nil (blank)

username - ritwebadmin
password - ritwebadminpass
database - ritwebdb

You can access the database through 
mysql -uroot ritwebdb
or
mysql -uritwebadmin -pritwebadminpass ritwebdb

# Backup
docker exec ritwebsite-container /usr/bin/mysqldump -u root --password= ritwebdb>ritwebdb.sql

# Restore
type ritwebdb.sql | docker exec -i ritwebsite-container /usr/bin/mysql -u root --password= ritwebdb

please share the mistakes/vagueness you find in the whatsapp group..

Happy Coding...!!
